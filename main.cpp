#include <Game.h>
#include <Filesystem/Filesystem.h>
#include <Tasks/TaskManager.h>
#include <Tasks/Task.h>
#include <Objects/GameInterface.h>
#include <Editor/Editor.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Objects/Engine.h>
#include <Log/GameConsole.h>
#include <Log/Log.h>
#include <iostream>

class TestTask : public Tasks::Task
{
public:
  TestTask()
    :Task(nullptr)
  {
  }
  void virtualExecute()
  {
  }
};

int main(int argv, char* argc[])
{
  bool errorQuit = false;
  try
    {
      try
	{
	  assert(sizeof(POINTER_INT) == sizeof(void*));
	  InstantiationList::global()->run();
	  Filesystem::global()->getNode("dev")->attachChild(new Engine);
	  for (GameComponent* child = Filesystem::global()->getNode("dev")->getChildren(); child; child = child->getNext())
	    {
	      std::cout << "Initing " << child->getName() << child << std::endl;
	      SingletonInterface* singleton = static_cast<Singleton<Log>*>(child);
	      assert(dynamic_cast<SingletonInterface*>(singleton));
	      singleton->singletonInit();
	    }
	  { /// FIXME attach these in a pcp script
	    //Editor::Editor::registerActions();
	    //DefaultFactories::global()->addFactory<StaticSkin, StaticSkin::StaticData>();
	  }
	  //Filesystem::global()->getNode("/")->executeCommand("print");
	  g_Game.init();
	  g_Game.run();
	}
      catch (ConsoleInterface::Error& _error)
	{
	  errorQuit = true;
	  std::cerr << "Uncaught error: " << _error.getMessage() << std::endl;
	  _error.catchError();
	  throw -1;
	}
    }
  catch (float f)
    {
      std::cerr << "Unknown exception thrown, caught at main.cpp:60" << std::endl;
      throw -1;
    }
  
  /*Tasks::TaskManager* manager = new Tasks::TaskManager;
  manager->run();
  manager->addTaskToThisFrame(new TestTask());*/
  //    Filesystem::global()->shutdown();
  std::cout << "Program closed successfully, except for known bugs" << std::endl;
  throw 0;
}

