#include "WaterMeshSensor.h"

WaterMeshSensor::WaterMeshSensor(BedBoundary* _boundary)
{
  boundary = _boundary;
}

WaterMeshSensor::~WaterMeshSensor()
{
}

void WaterMeshSensor::update()
{
}

void WaterMeshSensor::particleHit()
{
  assert(false);
}
