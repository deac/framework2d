#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <Entities/Entity.h>
#include <string>
class AbstractFactoryBase;

class Projectile : public Entity
{
    public:
        Projectile(AbstractFactoryBase* _explosion);
        virtual ~Projectile();
        void update();
        void hitWall(); /// Or something. It doesn't damage whatever it hit
        void hitAndDamage(Entity* target);
    protected:
    private:
        bool alive;
        AbstractFactoryBase* explosion;
};

#endif // PROJECTILE_H
