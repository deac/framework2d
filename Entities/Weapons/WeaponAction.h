#ifndef WEAPONACTION_H
#define WEAPONACTION_H

#include <GameObject.h>
class Vec2f;

class WeaponAction: public TypedGameComponent<WeaponAction>
{
    public:
        WeaponAction();
        virtual ~WeaponAction();
        static void registerActions(GameComponentType* _type);
        virtual void fire(const Vec2f& _source, const Vec2f& _position)=0;

        static std::string name()
        {
            return "WeaponAction";
        }
    protected:
    private:
	std::string virtualPrint();
};

#endif // WEAPONACTION_H
