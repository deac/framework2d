#pragma once

#include <Entities/Entity.h>
class BedBoundary;

class WaterMeshSensor: public Entity
{
public:
  WaterMeshSensor(BedBoundary* _boundary);
  ~WaterMeshSensor();
  void update();
  void particleHit();
private:
  BedBoundary* boundary;
};

