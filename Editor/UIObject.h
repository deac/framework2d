#pragma once

namespace CEGUI
{
  struct EventArgs;
  class Window;
}

namespace Editor
{
  class UIObject
  {
  public:
    UIObject(CEGUI::Window* _window);
    virtual ~UIObject();
  protected:
    CEGUI::Window* window;
  private:
    bool destroyed(const CEGUI::EventArgs& _args);
  };
}
