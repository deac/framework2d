#pragma once

#include <Editor/EditorMode.h>

namespace Editor
{
  class FilesystemEditor: public EditorMode
  {
  public:
    FilesystemEditor(Editor* _editor);
    ~FilesystemEditor();
    void update();
    void render();
  private:
  };
}
