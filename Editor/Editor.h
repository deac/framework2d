#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>
#include <Objects/GameInterfaces/UpdateFunctionInterface.h>
class TypeTable;
class PropertyBagInterface;
class InstanceParameters;
class PropertyBagWindow;

namespace Editor
{
  class EditorMode;
  class CommandParser;
  class DataManager;
  class StaticData;
  class Editor: public TypedGameComponent<Editor>, public ConsoleInterface, public UpdateFunctionInterface
  {
  public:
    Editor();
    ~Editor();
    void init();
    DataManager* getData(){return data;}
    void rebuild(InstanceParameters* _params);
    static void registerActions(GameComponentType* _type);
    static void staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table);
    static std::string name()
    {
      return "Editor";
    }
    static void typeInit(StaticData* _data, PropertyBagInterface* _params);
    void instanceInit(StaticData* _data, PropertyBagInterface* _params);
    void executeCommand(const std::string& _command);
    GameComponent* getComponent(){return this;}
  protected:
    void update();
    EditorMode* activeMode;
    CommandParser* parser;
    DataManager* data;
  };
}
