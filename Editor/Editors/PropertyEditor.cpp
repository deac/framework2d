#include <Editor/Editors/PropertyEditor.h>
#include <Log/Log.h>
#include <CEGUI/CEGUI.h>
#include <AbstractFactory/PropertyBagWindow.h>
#include <Editor/Editor.h>
#include <Filesystem/Filesystem.h>
using namespace CEGUI;

PropertyEditor::PropertyEditor(const std::string& _object, const std::string& _type, const std::string& _propertyName, Window* _editRoot, PropertyBagWindow* _window)
{
  object = _object;
  type = _type;
  propertyName = _propertyName;
  window = _window;

  static unsigned int iter = 0;
  rootWindow = WindowManager::getSingleton().loadWindowLayout("PropertyEditPopup.layout");
  CEGUI::System::getSingleton().getGUISheet()->addChildWindow(rootWindow);
  rootWindow->show();
  rootWindow->getChild("EditGroup")->addChildWindow(_editRoot);
  rootWindow->getChild("FinishButton")->subscribeEvent(PushButton::EventClicked, Event::Subscriber(&PropertyEditor::handleFinishButtonPressed, this));
}

PropertyEditor::~PropertyEditor()
{
  CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(rootWindow);
}

Window* PropertyEditor::getEditRoot()
{
  return rootWindow->getChild("EditGroup")->getChild(0);
}

bool PropertyEditor::handleFinishButtonPressed(const EventArgs& _event)
{
  Editor::Editor* editor = static_cast<Editor::Editor*>(Filesystem::singleton()->getNode("gameMode"));

  assert(object.substr(0,4) == "game");
  std::string objectName;
  if (object.size() > 4)
    objectName = "./" + object.substr(4);
  else
    objectName = ".";
  try
    {
      editor->executeCommand("set " + propertyName + " in " + objectName + " to " + type + " " + getValue() );
    }
  catch (ConsoleInterface::Error _e)
    {
      _e.catchError();
    }

  window->setProperty(type, propertyName, getValue());
  delete this;
}
