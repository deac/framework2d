#pragma once

#include <string>
class PropertyBagWindow;
namespace CEGUI
{
  class EventArgs;
  class Window;
}

class PropertyEditor
{
public:
  PropertyEditor(const std::string& _object, const std::string& _type, const std::string& _propertyName, CEGUI::Window* _editRoot, PropertyBagWindow* _window);
  virtual ~PropertyEditor();
protected:
  CEGUI::Window* getEditRoot();
private:
  CEGUI::Window* rootWindow;
  PropertyBagWindow* window;
  virtual std::string getValue()=0;
  std::string object, type, propertyName;
  bool handleFinishButtonPressed(const CEGUI::EventArgs& _event);
};
