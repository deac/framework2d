#include <Editor/Editors/DefaultEditor.h>
#include <CEGUI/CEGUI.h>
using namespace CEGUI;

DefaultEditor::  DefaultEditor(const std::string& _object, const std::string& _type, const std::string& _propertyName, const std::string& _currentValue, PropertyBagWindow* _window)
  :PropertyEditor(_object, _type, _propertyName, WindowManager::getSingleton().loadWindowLayout("DefaultPropertyEditor.layout"), _window)
{
  getEditRoot()->getChild(0)->setText(_currentValue);
}

DefaultEditor::~DefaultEditor()
{
}

std::string DefaultEditor::getValue()
{
  return getEditRoot()->getChild(0)->getText().c_str();
}
