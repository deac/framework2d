#pragma once

#include <Editor/Editors/PropertyEditor.h>

class DefaultEditor: public PropertyEditor
{
public:
  DefaultEditor(const std::string& _object, const std::string& _type, const std::string& _propertyName, const std::string& _currentValue, PropertyBagWindow* _window);
  ~DefaultEditor();
  std::string getValue();
private:
};
