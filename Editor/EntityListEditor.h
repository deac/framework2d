#pragma once

#include <Editor/EditorMode.h>
class EntityListData;
namespace CEGUI
{
  struct EventArgs;
  class PushButton;
  class Combobox;
  class GroupBox;
}

namespace Editor
{
  class EntityListEditor: public EditorMode
  {
  public:
    EntityListEditor(Editor* _editor, EntityListData* _data, CEGUI::Window* _root);
    ~EntityListEditor();
    void activate(bool _active);
    void render();
    void update();
  private:
    bool newButtonPressed(const CEGUI::EventArgs& _args);
    bool finishButtonPressed(const CEGUI::EventArgs& _args);
    bool cancelButtonPressed(const CEGUI::EventArgs& _args);
    EntityListData* data;
      CEGUI::GroupBox* listDisplay;
      CEGUI::PushButton* newButton;
      CEGUI::Combobox* comboBox;

      CEGUI::GroupBox* newEntityDisplay;

  };
}
