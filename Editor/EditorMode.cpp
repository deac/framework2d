#include "EditorMode.h"
#include <CEGUI/CEGUI.h>

namespace Editor
{
  EditorMode::EditorMode(Editor* _editor, Data* _data)
  {
    editor = _editor;
  }
  EditorMode::~EditorMode()
  {
  }
  
  void EditorMode::activate(bool _active)
  {
    virtualActivate(_active);
  }

}

