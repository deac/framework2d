#include <Editor/Data/DataManager.h>
#include <AbstractFactory/InstanceParameters.h>

namespace Editor
{
  DataManager::DataManager()
  {
    entities = new InstanceParameters(nullptr, "game");
    entities->setType("/dev/defaultFactories/Folder");
  }
  DataManager::~DataManager()
  {
  }

  InstanceParameters* DataManager::getObject(const std::string& _address)
  {
    InstanceParameters* ret = static_cast<InstanceParameters*>(entities->getNode(_address));
    assert(dynamic_cast<InstanceParameters*>(entities->getNode(_address)));
    return ret;
  }

  InstanceParameters* DataManager::getRoot()
  {
    return entities;
  }
}
