#pragma once

#include <Types/Tree.h>

namespace Editor
{
  class DataSubscriber;
  class Data
  {
  public:
    Data();
    ~Data();
    void changed();
  private:
    List<DataSubscriber> subscribers;
  };
}
