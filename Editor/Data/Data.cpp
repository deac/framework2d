#include <Editor/Data/Data.h>
#include <Editor/Data/DataSubscriber.h>

namespace Editor
{
  Data::Data()
  {
  }

  Data::~Data()
  {
    for (DataSubscriber* subscriber = subscribers.front(); subscriber; subscriber = subscriber->getNextNode())
      {
	subscriber->dataDestroyed();
      }
  }
  void Data::changed()
  {
    for (DataSubscriber* subscriber = subscribers.front(); subscriber; subscriber = subscriber->getNextNode())
      {
	subscriber->dataChanged();
      }
  }
}
