#pragma once

#include "Data.h"
#include <vector>
#include <map>

namespace Editor
{
  class DataListBase: public Data
  {
  public:
    virtual unsigned int getSize()=0;
    virtual std::string getName(unsigned int _index)=0;
  };
  template <typename Type>
  class DataList: public DataListBase
  {
  public:
    DataList();
    ~DataList();
    Type* getObject(unsigned int _index){return data[_index];}
    Type* getObjectByName(const std::string& _name);
    void addInOrder(Type* _object);
    unsigned int getSize(){return data.size();}
    std::string getName(unsigned int _index){return data[_index]->getName();}
  private:
    std::vector<Type*> data;
  };
}


namespace Editor
{
  template <typename Type>
  DataList<Type>::DataList()
  {
  }
  
  template <typename Type>
  DataList<Type>::~DataList()
  {
    for (unsigned int i = 0; i != data.size(); i++)
      {
	delete data[i];
      }
  }

  template <typename Type>
  Type* DataList<Type>::getObjectByName(const std::string& _name)
  {
    for (unsigned int i = 0; i != data.size(); i++)
      {
	if (data[i]->getName() == _name)
	  return data[i];
      }
    return nullptr;
  }

  template <typename Type>
  void DataList<Type>::addInOrder(Type* _type)
  {
    data.push_back(_type);
  }
}
