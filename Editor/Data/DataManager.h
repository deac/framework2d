#pragma once

#include <string>
class InstanceParameters;

namespace Editor
{
  class DataManager
  {
  public:
    DataManager();
    ~DataManager();
    /*DataList<EntityListData>* getEntityLists(){return &entityLists;}
    DataList<FactoryListData>* getFactoryLists(){return &factoryLists;}
    DataList<FactoryTypeData>* getFactoryTypes(){return &factoryTypes;}*/
    //DataList<EntityData>* getEntities(){return &entities;}
    InstanceParameters* getObject(const std::string& _address);
    InstanceParameters* getRoot();
  private:
    /*DataList<EntityListData> entityLists;
    DataList<FactoryListData> factoryLists;
    DataList<FactoryTypeData> factoryTypes;*/
    //DataList<EntityData> entities;
    InstanceParameters* entities;
  };
}
