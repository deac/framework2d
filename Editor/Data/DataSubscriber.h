#pragma once

#include <Types/Tree.h>

namespace Editor
{
  class DataSubscriber: public ListNode<DataSubscriber>
  {
  public:
    DataSubscriber(Data* _data);
    virtual ~DataSubscriber();
    virtual void dataChanged()=0;
    virtual void dataDestroyed()=0;
  private:
  };
}
