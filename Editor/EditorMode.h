#pragma once

#include <Editor/UIObject.h>

namespace Editor
{
  class Editor;
  class Data;
  class EditorMode
  {
  public:
    EditorMode(Editor* _editor, Data* _data);
    virtual ~EditorMode();
    void activate(bool _active);
    virtual void render()=0;
    virtual void update()=0;
  protected:
    virtual void virtualActivate(bool _active){}
    Editor* editor;
  private:
  };
}
