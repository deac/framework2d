#pragma once

#include <Editor/EditorMode.h>
class FactoryListData;

namespace Editor
{
  class FactoryEditor: public EditorMode
  {
  public:
    FactoryEditor(Editor* _editor, FactoryListData* _data);
    ~FactoryEditor();
    void activate(bool _active);
    void update();
    void render();
  private:
    FactoryListData* data;
  };
}
