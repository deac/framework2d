#include <Editor/FilesystemEditor.h>

namespace Editor
{
  FilesystemEditor::FilesystemEditor(Editor* _editor)
    :EditorMode(_editor, nullptr)
  {
  }
  FilesystemEditor::~FilesystemEditor()
  {
  }

  void FilesystemEditor::update()
  {
  }
  void FilesystemEditor::render()
  {
  }
}
