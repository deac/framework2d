#pragma once

#include <string>

namespace Editor
{
  class FactoryTypeData
  {
  public:
    FactoryTypeData();
    ~FactoryTypeData();
    const std::string& getProduct(){return product;}
    const std::string& getName(){return name;}
  private:
    std::string product, name;
  };
}
