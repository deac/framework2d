#include "Editor.h"
#include <Graphics/Camera/PhysicsCamera.h>
#include <Filesystem/Filesystem.h>
#include <GameModes/BubbleDrawEvent.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Graphics/GraphicsManager.h>
#include <Input/InputManager.h>
#include <AI/PlayerInputBrain.h>
#include <Level/Level.h>
#include <AbstractFactory/AbstractFactories.h>
#include <AI/AIManager.h>
#include <Graphics/Camera/FreeCamera.h>
#include <Physics/PhysicsManager.h>
#include <Physics/WaterPhysicsSystem/WaterPhysicsSystem.h>
#include <Editor/FactoryEditor.h>
#include <Editor/EntityListEditor.h>
#include <SDL/SDL.h>
#include <Editor/Parser/CommandParser.h>
#include <Editor/Data/DataManager.h>
#include <Editor/FilesystemEditor.h>
#include <AbstractFactory/InstanceParameters.h>
#include <AbstractFactory/PropertyBagWindow.h>

namespace Editor
{
  Editor::Editor()
    :TypedGameComponent("gameMode", Filesystem::global()),
     ConsoleInterface(false)
  {
    CEGUI::Window* root = CEGUI::System::getSingleton().getGUISheet();
    activeMode = nullptr;
    static_cast<Log*>(Filesystem::global()->getNode("dev/log"))->newConsole(this, "editor");
    //    activeMode = new EntityListEditor(this, new EntityListData("penis"), root);
    attachBiStream(stdin, stdout);
  }

  Editor::~Editor()
  {
    delete data;
    delete parser;
  }

  void Editor::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
  {
  }
  
  void Editor::typeInit(StaticData* _data, PropertyBagInterface* _params)
  {
  }
  void Editor::instanceInit(StaticData* _data, PropertyBagInterface* _params)
  {
    data = new DataManager;
    parser = new CommandParser(this, data);
    init();
  }

  InstanceParameters* g_Root = nullptr; // FIXME
  void Editor::init()
  {
    std::string command;
    command = "new /dev/defaultFactories/ComponentLoader named components in .";
    input(&command);
    command = "new /dev/defaultFactories/Folder named factories in .";
    input(&command);
    command = "new /dev/defaultFactories/Folder named entities in .";
    input(&command);
    command = "new components/StaticSkin named staticSkin in factories";
    input(&command);
    command = "new /dev/defaultFactories/GameObject named player in entities";
    input(&command);
    command = "new /dev/defaultFactories/StaticPosition named position in entities/player";
    input(&command);
    command = "new factories/staticSkin named skin in entities/player";
    input(&command);
    command = "new /dev/defaultFactories/CeguiPanelFactory named panel in factories";
    input(&command);
    command = "new factories/panel named controls in .";
    input(&command);
    command = "new /dev/defaultFactories/PlayerControllerFactory named playerController in factories";
    input(&command);
    command = "new factories/playerController named controller in entities/player";
    input(&command);
    command = "new components/Camera named camera in factories";
    input(&command);
    command = "new factories/camera named camera in .";
    input(&command);
    //command = "delete entities/player";
    //input(&command);
    command = "undo";
    //executeCommand(command);
    //executeCommand(command);
    CEGUI::System::getSingleton().getGUISheet()->removeChildWindow("Component");
    CEGUI::System::getSingleton().getGUISheet()->addChildWindow(g_Root->createWindow()->getWindow());

    
    CEGUI::Window* root = CEGUI::System::getSingleton().getGUISheet();
    activeMode = new FilesystemEditor(this);
  }
  struct StaticData
  {
    static std::string name()
    {
      return "Editor";
    }
    static void staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
    {
    }
  };

  void Editor::registerActions(GameComponentType* _type)
  {
    DefaultFactories::global()->addFactory<Editor, StaticData>();
    _type->exposeInterface<Editor, ConsoleInterface>();
    _type->exposeInterface<Editor, UpdateFunctionInterface>();
  }

  void Editor::update()
  {
    bool running = true;
    /// FIXME needs a timer
    //activeMode->update();
    running = g_InputManager.processInput();
    if (!running)
      getParent()->detachChild(this);
  }

  void Editor::executeCommand(const std::string& _command)
  {
    parser->executeCommand(&_command);
  }
  
  void Editor::rebuild(InstanceParameters* _parameters)
  {
    assert(_parameters->getName() == "game");
    assert(_parameters->getType() == "/dev/defaultFactories/Folder");
    try {
      delete Filesystem::global()->getNode("game");
    } catch (ConsoleInterface::Error& _error){_error.catchError();}
    GameComponent* root = Filesystem::global();
    GameComponent* game = _parameters->__build__(root);
    g_Root = _parameters;
    //Filesystem::global()->attachChild(game);
  }
}
