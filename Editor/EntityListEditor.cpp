#include <Editor/EntityListEditor.h>
#include <Level/EntityListData.h>
#include <Level/EntityData.h>
#include <CEGUI/CEGUI.h>

/*namespace Editor
{
  EntityListEditor::EntityListEditor(Editor* _editor, EntityListData* _data, CEGUI::Window* _root)
    :EditorMode(_editor, _data, CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/FrameWindow", _data->getName()))
  {
    data = _data;
    _root->addChildWindow(window);
    {
      listDisplay = static_cast<CEGUI::GroupBox*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/GroupBox", "ListDisplay"));
      window->addChildWindow(listDisplay);
      listDisplay->setArea({0,0},{0,0},{1,1},{1,0});
      comboBox = static_cast<CEGUI::Combobox*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Combobox", "Entities"));
      listDisplay->addChildWindow(comboBox);
      comboBox->setArea({0,0},{0,0},{1,1},{1,-30});
      for (unsigned int i = 0; i != data->entitiesSize(); i++)
	{
	  comboBox->addItem(new CEGUI::ListboxTextItem(data->getEntity(i)->getName()));
	}

      newButton = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "NewEntity"));
      newButton->setArea({0,0},{1,-30},{1,0},{1,0});
      listDisplay->addChildWindow(newButton);
      newButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(&EntityListEditor::newButtonPressed, this));
    }
    {
      newEntityDisplay = static_cast<CEGUI::GroupBox*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/GroupBox", "NewEntityDisplay"));
      window->addChildWindow(newEntityDisplay);
      newEntityDisplay->setArea({0,0},{0,0},{1,1},{1,0});
      newEntityDisplay->hide();
      
      CEGUI::PushButton* finish = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "Finish"));
      finish->setArea({0,0},{1,-30},{0.5f,0},{1,0});
      newEntityDisplay->addChildWindow(finish);
      finish->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(&EntityListEditor::finishButtonPressed, this));
      
      CEGUI::PushButton* cancel = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "Cancel"));
      cancel->setArea({0.5f,0},{1,-30},{1,0},{1,0});
      newEntityDisplay->addChildWindow(cancel);
      cancel->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(&EntityListEditor::cancelButtonPressed, this));

      CEGUI::MultiColumnList* properties = static_cast<CEGUI::MultiColumnList*>(CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/MultiColumnList", "Properties"));
      properties->setArea({0,0},{0,0},{1,1},{1,-30});
      properties->addColumn("Name", 0, {0.3f, 0});
      newEntityDisplay->addChildWindow(properties);
    }
  }
  EntityListEditor::~EntityListEditor()
  {
  }

  void EntityListEditor::render()
  {
  }

  void EntityListEditor::update()
  {
  }

  bool EntityListEditor::newButtonPressed(const CEGUI::EventArgs& _args)
  {
    listDisplay->hide();
    newEntityDisplay->show();
  }
  bool EntityListEditor::finishButtonPressed(const CEGUI::EventArgs& _args)
  {
    newEntityDisplay->hide();
    listDisplay->show();
  }
  bool EntityListEditor::cancelButtonPressed(const CEGUI::EventArgs& _args)
  {
    newEntityDisplay->hide();
    listDisplay->show();
  }
}
*/
