#include <Editor/UIObject.h>
#include <CEGUI/CEGUI.h>

namespace Editor
{
  UIObject::UIObject(CEGUI::Window* _window)
  {
    window = _window;
    window->setUserData(this);
    window->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(&UIObject::destroyed, this));

  }
  UIObject::~UIObject()
  {
    delete window;
  }

  bool UIObject::destroyed(const CEGUI::EventArgs& _args)
  {
    window = nullptr;
    delete this;
  }
}
