#include "CommandParser.h"
#include <Log/Log.h>
#include <AbstractFactory/InstanceParameters.h>
#include <Filesystem/Filesystem.h>
#include <Editor/FactoryTypeData.h>
#include <Editor/Data/DataManager.h>
#include <Level/PropertyBagData.h>
#include <Level/EntityListData.h>
#include <Level/FactoryListData.h>
#include <Level/FactoryData.h>
#include <Level/EntityData.h>
#include <Editor/Parser/Parser.tab.hpp>
#include <Editor/Editor.h>
#include <cstring>
extern int yyparse();

namespace Editor
{
  CommandParser::CommandParser(Editor* _editor, DataManager* _data)
  {
    editor = _editor;
    data = _data;
    undoControl = new UndoStack<std::string>;
    undoing = false;
  }
  CommandParser::~CommandParser()
  {
  }

  void CommandParser::executeCommand(const std::string* _command)
  {
    try
      {
	parse(_command, false);
	undoControl->action(new std::string(*_command));
      }
    catch (ConsoleInterface::Error& _error)
      {
	_error.catchError("Error executing command:" + *_command + ' ' + _error.getMessage());
      }
  }

  void CommandParser::undo()
  {
    std::string* command = undoControl->undo();
    try
      {
	parse(command, true);
      }
    catch (int i)
      {
	g_Log.warning(std::string("Error undoing command: ") + *command);
      }
  }

  void CommandParser::redo()
  {
    std::string* command = undoControl->redo();
    try
      {
	parse(command, false);
      }
    catch (int i)
      {
	g_Log.error("Error redoing command: " + *command);
      }
  }
  
  void CommandParser::parse(const std::string* _command, bool _undo)
  {
    globalParser = this;
    input = *_command + '\n';
    iterator = 0;
    undoing = _undo;
    yyparse();
    assert(globalParser == this);
  }

  CommandParser* CommandParser::global()
  {
    assert(globalParser);
    return globalParser;
  }

  int CommandParser::getInput()
  {
    assert(iterator <= input.size());
    int ret = input[iterator];
    iterator++;
    return ret;
  }

  void CommandParser::newCommand(const std::string& _type, const std::string& _name, const std::string& _address)
  {
    if (!undoing)
      {
	InstanceParameters* params = new InstanceParameters(data->getRoot(), _name);
	params->setType(_type);
	InstanceParameters* parent = dynamic_cast<InstanceParameters*>(data->getRoot()->getNode(_address));
	parent->appendChild(params);
	editor->rebuild(data->getRoot());
      }
    else
      {
	undoing = false;
	deleteCommand(_name);
      }
  }
  void CommandParser::deleteCommand(const std::string& _name)
  {
    auto object = data->getObject(_name);
    //g_Log.message("new " + object->getType() + " named " + _name);
    delete object;
  }

  void CommandParser::setParameterCommand(const std::string& _param, const std::string& _object, const std::string& _type, const std::string& _value)
  {
    InstanceParameters* object = data->getObject(_object);
    object->getTypeTable()->addDynamicValue(_type, _param, _value);
  }
  /*AbstractFactories* CommandParser::getFactories()
  {
    GameComponent* factories = Filesystem::global()->getNode("/dev/factories");
    assert(dynamic_cast<AbstractFactories*>(factories));
    return static_cast<AbstractFactories*>(factories);
    }*/
  CommandParser* CommandParser::globalParser = nullptr;
}
