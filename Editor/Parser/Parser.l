%{
#include <string>
extern void yyerror(const char* _message);
int yywrap(){return 1;}
#define YY_SKIP_YYWRAP
#include <Editor/Parser/CommandParser.h>
#include "Parser.tab.hpp"
#include <iostream>
namespace Editor
{   
  int getNextCharacter();
}
#define YY_INPUT(buf, result, max_size)\
 {\
   int c = Editor::getNextCharacter();		\
   if (c)\
     {\
       buf[0] = c;\
       result = 1;\
     }\
   else\
     {\
       result = 0;\
     }\
 }
%}



%%

new {return NEW;}
delete {return DELETE;}
set {return SET;}
named {return NAMED;}
to {return TO;}
in {return IN;}

[a-zA-Z0-9_/\.]* {
strcpy(yylval.string, yytext);
return ID;
}
\"[^\"\n]\" {
memcpy(yylval.string, yytext, yyleng-1);
yylval.string[yyleng-2] = 0;
return STRING;
}
\"[^\"\n]\n {yyerror("Unterminated string");}

\n {return END_LINE;}
[\n\t\ ]
x. {yyerror((std::string("Stray character caught'") + yytext + "'").c_str());}
%%

//undo {return UNDO;}
//redo {return REDO;}
/// FIXME put these two rules back in

#include <cassert>

int Editor::getNextCharacter()
{
  return g_CommandParser->getInput();
}
