%{
#include <string>
extern int yylex();
 extern void yyerror(const char*);
 #include <Editor/Parser/CommandParser.h>
%}



%token NEW DELETE SET TO NAMED IN END_LINE UNDO REDO
%token<string> ID STRING

%union
 {
   char string[64];
   EntityListData* entityList;
   FactoryListData* factoryList;
   EntityData* entity;
   FactoryData* factory;
   Editor::FactoryTypeData* factoryType;
}

%%

Commands:
    Commands Command
    | Command
    ;


Command:
NEW ID NAMED ID IN ID END_LINE {Editor::g_CommandParser->newCommand($2, $4, $6);}|
DELETE ID END_LINE {Editor::g_CommandParser->deleteCommand($2);}|
SET ID IN ID TO ID ID END_LINE {Editor::g_CommandParser->setParameterCommand($2, $4, $6, $7);}|
UNDO END_LINE {Editor::g_CommandParser->undo();}|
REDO END_LINE {Editor::g_CommandParser->redo();}|
error END_LINE
    ;

%%

#include <Log/Log.h>

void yyerror(const char* _message)
{
  g_Log.error("Parsing error: " + std::string(_message));
}



			      
