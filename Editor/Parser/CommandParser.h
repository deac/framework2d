#pragma once

#include <Types/UndoControl.h>
#include <Editor/Data/DataList.h>
#include <string>
#include <unordered_map>
class EntityListData;
class FactoryListData;
class FactoryData;
class EntityData;
class AbstractFactoryBase;
class AbstractFactories;
class GameComponent;
typedef EntityListData EntityList;
typedef FactoryListData FactoryList;
namespace Editor
{
  class FactoryTypeData;
  class Editor;
  class DataManager;

  class CommandParser
  {
  public:
    CommandParser(Editor* _editor, DataManager* _data);
    ~CommandParser();
    void executeCommand(const std::string* _command);
    void undo();
    void redo();

    /*EntityList* getEntityList(const std::string& _name);
    FactoryList* getFactoryList(const std::string& _name);
    EntityData* getEntity(const std::string& _name);
    FactoryData* getType(const std::string& _name);
    FactoryTypeData* getFactoryType(const std::string& _name);*/
    
    void newCommand(const std::string& _type, const std::string& _object, const std::string& _address);
    void deleteCommand(const std::string& _object);
    void setParameterCommand(const std::string& _param, const std::string& _object, const std::string& _type, const std::string& _value);

    int getInput();
    static CommandParser* global(); /// For use by bison only
  private:
    //AbstractFactories* getFactories();
    void parse(const std::string* _command, bool _undo);
    UndoControl<std::string>* undoControl;
    Editor* editor;

    DataManager* data;
    std::string input;
    bool undoing;
    unsigned int iterator;
    static CommandParser* globalParser; /// For the bison parser
  };
}

#define g_CommandParser CommandParser::global()
