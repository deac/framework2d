#ifndef _ACTIONTASK_H
#define _ACTIONTASK_H

#include <Tasks/Task.h>
#include <Objects/GameComponent.h>

namespace Tasks
{
  class TaskProfile;
  class ActionTask : public Task
  {
  public:
    ActionTask(GameComponent* _object, ActionHandle* _action, TaskProfile* _profile);
    ActionTask(const std::string& _objectDotAction, TaskProfile* _profile); 
    ~ActionTask();
    void virtualExecute();
  private:
    GameComponent* object;
    ActionHandle* action;
  };
}
#endif /* _ACTIONTASK_H */

