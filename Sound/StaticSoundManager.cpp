#include "StaticSoundManager.h"
#include <Sound/SDLSoundManager.h>
#include <Sound/NullSoundManager.h>
#include <Log/Log.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>
using namespace std;

StaticSoundManager::StaticSoundManager()
{
  //ctor
  soundManager = nullptr;
}

StaticSoundManager::~StaticSoundManager()
{
  //dtor
}

void StaticSoundManager::init()
{
  try
    {
      soundManager = new SDLSoundManager();
    }
  catch (ConsoleInterface::Error &_error)
    {
      _error.catchError("Running without audio");
      soundManager = new NullSoundManager;
    }
}
void StaticSoundManager::quit()
{
  delete soundManager;
  soundManager = nullptr;
}

SoundManager* StaticSoundManager::soundManager = nullptr;
