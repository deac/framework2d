#ifndef _THREAD_H
#define _THREAD_H

#include <SDL/SDL_thread.h>
#include <Objects/GameComponent.h>

class Thread: public TypedGameComponent<Thread>
{
 public:
  Thread(GameComponent* _object, ActionHandle* _main);
  ~Thread();
  void run();
  static std::string name()
  {
    return "Thread";
  }
  static void registerActions(GameComponentType* _type);
 private:
  static int internalMain(void* _data);
  int internalRun();
  GameComponent* object;
  ActionHandle* main;
  SDL_Thread* thread;
};

#endif /* _THREAD_H */

