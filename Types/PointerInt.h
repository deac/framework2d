#pragma once

#if __i386__ // 32 bit 
#define POINTER_INT unsigned long
#else
#if __x86_64__ // 64 bit
#define POINTER_INT uint64_t
#else
#error Please specify POINTER_INT size
#endif
#endif
