#ifndef _LISTNODE_H
#define _LISTNODE_H

#include <string>

template <typename Object> /// <Type stored in the List>
class ListNode;

template <typename Object> /// <Type stored in the List>
class List
{
public:
  List(){children = nullptr;}
  ~List();
  void prepend(Object* _child);
  void append(Object* _child);
  void remove(Object* _child);
  Object* front();
  unsigned int countChildren();
protected:
  Object* children;
  virtual void childAttached(Object* _child){}
  virtual void childDetached(Object* _child){}
};

template <typename Node> /// <Type of the List>
class ListNode
{
public:
  ListNode(){next = prev = nullptr;}
  ~ListNode();
  Node* getNext(){return next;}
  Node* getPrev(){return prev;}
  Node* getPrevNode(){return static_cast<Node*>(static_cast<ListNode<Node>*>(this)->getPrev());}
  Node* getNextNode(){return static_cast<Node*>(next);}
  Node& operator()(){return static_cast<Node&>(*this);}
protected:
  friend class List<Node>;
  virtual void detached(List<Node>* _oldParent){}
  virtual void newParent(List<Node>* _newParent){}
  Node* next,* prev;
};

template <typename Node> /// <Type of the Tree>
class TreeNode: public ListNode<Node>, public List<Node>
{
public:
  TreeNode(){parent = nullptr;}
  virtual ~TreeNode()
  {
    if (parent)
      parent->detachChild(static_cast<Node*>(this));
    while (getChildrenNode())
      {
	delete getChildrenNode();
      }
  }
  TreeNode<Node>* getParent(){return parent;}
  Node* getParentNode(){return parent;}
  Node* getPrevNode(){return static_cast<Node*>(static_cast<ListNode<TreeNode<Node>>*>(this)->getPrev());}
  Node* getNextNode(){return static_cast<ListNode<Node>*>(this)->getNext();}
  Node* getChildrenNode(){return static_cast<List<Node>*>(this)->front();}
  Node& operator()(){return static_cast<Node&>(*this);}
  void attachChild(Node* _child);
  void appendChild(Node* _child);
  void detachChild(Node* _child);
  Node* front(){return getChildrenNode();}
  Node* getChildren(){return getChildrenNode();}
protected:
  virtual void parentChanged(TreeNode<Node>* _oldParent){}
  Node* parent;
};

template <typename Node> /// <Type of the Tree>
class NamedTreeNode: public TreeNode<Node>
{
public:
  NamedTreeNode(const std::string& _name){objectName = _name;}
  ~NamedTreeNode(){}
  const std::string& getName(){return objectName;}
  Node* getChildByName(const std::string& _name){return operator [](_name);}
  Node* operator [](const std::string& _name);
  void setName(const std::string& _name){objectName = _name;}
protected:
  std::string objectName;
};

#include <cassert>
template <typename Object>
List<Object>::~List()
{
}

template <typename Object>
Object* List<Object>::front()
{
  return children;
}

template <typename Object>
unsigned int List<Object>::countChildren()
{
  unsigned int count = 0;
  for (auto child = children; child; child = child->getNext())
    count++;
  return count;
}
template <typename Object>
void List<Object>::prepend(Object* _object)
{
  _object->next = children;
  _object->prev = nullptr;
  if (children != nullptr)
    {
      children->prev = _object;
    }
  children = _object;
  childAttached(_object);
}
template <typename Object>
void List<Object>::append(Object* _object)
{
  Object* prevChild = children;
  Object* child = children;
  if (!child)
    {
      prepend(_object);
    }
  else
    {
      for (child; child; child = child->getNext())
	{
	  prevChild = child;
	}
      assert(prevChild);
      assert(!prevChild->next);
      prevChild->next = _object;
      _object->prev = prevChild;
      childAttached(_object);
    }
}
template <typename Object>
void List<Object>::remove(Object* _object)
{
  if (_object->prev)
    _object->prev->next = _object->next;
  else
    {
      assert(_object == children);
      children = _object->next;
    }

  if (_object->next)
    _object->next->prev = _object->prev;

  _object->prev = nullptr;
  _object->next = nullptr;
  childDetached(_object);
}
template <typename Node>
ListNode<Node>::~ListNode()
{
}

void errorLog(const std::string& _name);
#include <cstring>
#include <iostream>
template <typename Object>
Object* NamedTreeNode<Object>::operator [](const std::string& _address)
{
  Object* child = this->getChildren();
  Object* prevChild = nullptr;
  while (child)
    {
      assert(child->getName().size() > 0);
      assert(_address.size() > 0);
      if (child->getName() == _address)
	{
	  return child;
	}
      prevChild = child;
      child = child->getNext();
    }
  errorLog("No " + _address + " attached to " + this->objectName);
}
template <typename Object>
void TreeNode<Object>::attachChild(Object* _child)
{
  auto oldParent = _child->parent;
  if (oldParent)
    oldParent->detachChild(_child);
  assert(dynamic_cast<Object*>(this));
  _child->parent = static_cast<Object*>(this);

  static_cast<List<Object>*>(this)->prepend(_child);
  static_cast<TreeNode<Object>*>(_child)->parentChanged(oldParent);
  assert(_child->parent != _child);
}
template <typename Object>
void TreeNode<Object>::appendChild(Object* _child)
{
  auto oldParent = _child->parent;
  if (oldParent)
    oldParent->detachChild(_child);
  assert(dynamic_cast<Object*>(this));
  _child->parent = static_cast<Object*>(this);

  static_cast<List<Object>*>(this)->append(_child);
  static_cast<TreeNode<Object>*>(_child)->parentChanged(oldParent);
  assert(_child->parent != _child);
}


template <typename Object>
void TreeNode<Object>::detachChild(Object* _child)
{
  static_cast<List<Object>*>(this)->remove(_child);
  _child->parent = nullptr;
}

#endif /* _LISTNODE_H */

