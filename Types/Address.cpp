#include <Types/Address.h>

Address::Address(const char* _address)
{
  std::string address(_address);
  unsigned int iter = 0;
  if (address[iter] == '/')
    {
      nodes.push_back("/");
      iter++;
    }
  while (address.size() > iter)
    {
      int next = address.find('/', iter);
      if (next == -1)
	next = address.size();
      std::string node = address.substr(iter, next - iter);
      nodes.push_back(node);
      iter = next+1;
    }
}

Address::~Address()
{
}

std::string Address::getFirst()const
{
  return nodes[0];
}

bool Address::popFirst()
{
  nodes.erase(nodes.begin());
  if (nodes.empty())
    return false;
  else return true;
}

std::string Address::cpp_str()const
{
  std::string ret = nodes[0];
  for (unsigned int i = 1; i != nodes.size(); i++)
    {
      if (ret != "/")
	ret += "/";
      ret += nodes[i];
    }
  return ret;
}
