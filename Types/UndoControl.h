#pragma once

template <typename Action>
class UndoControl
{
public:
  UndoControl();
  ~UndoControl();
  virtual void action(Action* _action)=0;
  virtual Action* undo()=0;
  virtual Action* redo()=0;
};

#include <vector>

template <typename Action>
class UndoStack: public UndoControl<Action>
{
public:
  UndoStack();
  ~UndoStack();
  void action(Action* _action);
  Action* undo(); /// This needs to be inverted
  Action* redo();
private:
  std::vector<Action*> actions;
  unsigned int iterator;
};


#include <cassert>

template <typename Action>
UndoControl<Action>::UndoControl()
{
}
template <typename Action>
UndoControl<Action>::~UndoControl()
{
}
template <typename Action>
UndoStack<Action>::UndoStack()
{
  iterator = 0;
}
template <typename Action>
UndoStack<Action>::~UndoStack()
{
  for (unsigned int i = 0; i != actions.size(); i++)
    {
      delete actions[i];
    }
}

template <typename Action>
void UndoStack<Action>::action(Action* _action)
{
  while (actions.size() != iterator)
    {
      delete actions.back();
      actions.pop_back();
    }
  actions.push_back(_action);
  iterator++;
}

template <typename Action>
Action* UndoStack<Action>::undo()
{
  assert(iterator != 0);
  iterator--;
  return actions[iterator];
}

template <typename Action>
Action* UndoStack<Action>::redo()
{
  assert(iterator != actions.size());
  Action* ret = actions[iterator];
  iterator++;
  return ret;
}
