#pragma once

#include <vector>
#include <string>

class Address
{
public:
  Address(const char* _address);
  ~Address();
  std::string cpp_str()const;
  std::string getFirst()const;
  bool popFirst(); /// Returns false if the resultant Address is empty
private:
  std::vector<std::string> nodes;
};
