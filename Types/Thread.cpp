#include "Thread.h"

Thread::Thread(GameComponent* _object, ActionHandle* _main)
{
  object = _object;
  main = _main;
  thread = nullptr;
}
Thread::~Thread()
{
}

void Thread::registerActions(GameComponentType* _type)
{
}
int Thread::internalMain(void* _data)
{
  return static_cast<Thread*>(_data)->internalRun();
}
int Thread::internalRun()
{
  main->execute(object);
  return 0;
}
void Thread::run()
{
  thread = SDL_CreateThread(internalMain, this);
}
