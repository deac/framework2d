#include <Objects/Singleton.h>
#include "Timer.h"
#include <Filesystem/Filesystem.h>
#include <Objects/GameObject.h>
#include <Filesystem/Folder.h>
#include <SDL/SDL.h>
#include <cassert>

Timer::Timer()
  :TypedGameComponent<Timer>("timer", nullptr)
{
}
void Timer::init()
{
  int result = SDL_Init(SDL_INIT_TIMER);
  assert(result == 0);
  startTime = SDL_GetTicks();
  currentlyPaused = false;
  frame = 0;
  GameComponent* filesystem = Filesystem::global();
  GameComponent* _this = this;
  assert(dynamic_cast<Filesystem*>(filesystem));
  assert(!dynamic_cast<Timer*>(filesystem));
  assert(dynamic_cast<Timer*>(_this));
  assert(dynamic_cast<Filesystem*>(filesystem));
  assert(filesystem != this);
  assert(parent == nullptr);
  GameComponent* dev = filesystem->getIndividualNode("dev");
  dev->attachChild(this);
  assert(getParent() == dev);
}
void Timer::pause()
{
    pauseTime = SDL_GetTicks();
    currentlyPaused = true;
}
void Timer::unpause()
{
    unsigned int totalPauseTime = SDL_GetTicks() - pauseTime;
    startTime += totalPauseTime;
    currentlyPaused = false;
}
unsigned int Timer::getTicks()
{
    if (currentlyPaused)
    {
        return pauseTime - startTime;
    }
    else
    {
        unsigned int ticks = SDL_GetTicks();
        return ticks - startTime;
    }
}
unsigned int Timer::getFrame()
{
    return frame;
}
void Timer::tick()
{
    frame++;
}

void Timer::registerActions(GameComponentType* _type)
{
    _type->createActionHandle("pause", &Timer::pause);
    _type->createActionHandle("unpause", &Timer::unpause);
}
Timer* Timer::global()
{
    static Timer* timer = new Timer;
    return timer;
}







