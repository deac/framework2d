#include "Game.h"
#include <Graphics/GraphicsManager.h>
#include <Physics/PhysicsManager.h>
#include <Input/InputManager.h>
#include <Objects/GameInterfaces/RenderInterface.h>
#include <Objects/GameInterfaces/UpdateFunctionInterface.h>
#include <Filesystem/Filesystem.h>
#include <GameModes/PlayMode.h>
#include <GameModes/ShooterGame.h>
#include <GameModes/CarneGame.h>
#include <Timer.h>
#include <GameModes/Editor/EditorMode.h>
#include <Level/Level.h>
#include <Graphics/Camera/FreeCamera.h>
#include <AI/AIManager.h>
#include <AI/CharacterController.h>
#include <SharedContent/ContentManager.h>
#include <AbstractFactory/AbstractFactories.h>

#include <Sound/StaticSoundManager.h>
#include <Sound/SoundManager.h>
#include <Editor/Editor.h>

#include <cstring>

using namespace std;
Game g_Game;
Game::Game()
{
}
void Game::init()
{
    //ctor
    Timer::global()->init();
    Timer::global()->pause();

    StaticSoundManager::init(); /// FIXME remove these init functions

    //EditorMode* typedMode = new EditorMode(new ShooterGame());
    Editor::Editor* typedMode = new Editor::Editor();
    typedMode->instanceInit(nullptr, nullptr);

    CEGUI::EventArgs args;
    //    typedMode->activate(args);



    Timer::global()->unpause();
}
Game::~Game()
{
    //dtor
}
#include <GL/gl.h>
void Game::run()
{
  GameComponent* gameMode = Filesystem::global()->getNode("gameMode");
  while (gameMode)
    {
      g_GraphicsManager.beginScene();
      gameMode->getInterface<UpdateFunctionInterface>()->update();
      Filesystem::global()->getNode("game/entities/player/skin")->getInterface<RenderInterface>()->render();
      g_GraphicsManager.endScene();
      gameMode = Filesystem::global()->getNode("gameMode");
    }
  //delete gameMode;
}




















