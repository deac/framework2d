#ifndef TIMER_H
#define TIMER_H

#include <Objects/GameComponent.h>

class Timer: public TypedGameComponent<Timer>
{
    public:
        void init();
        void pause();
        void unpause();
        unsigned int getTicks();
        unsigned int getFrame();
        void tick();
        static std::string name()
        {
            return "Timer";
        }
        static void registerActions(GameComponentType* _type);
        static Timer* global();
    protected:
    private:
        Timer();
	std::string virtualPrint();
        unsigned int startTime;
        unsigned int pauseTime;
        unsigned int frame;
        bool currentlyPaused;
};

#endif // TIMER_H
