#ifndef JOINTBODYPARTFACTORY_H
#define JOINTBODYPARTFACTORY_H

#include <AbstractFactory/AbstractFactory.h>
#include <Physics/BodyParts/BodyPart.h>
#include <Box2D/Box2D.h>
class PhysicsManager;

class JointBodyPartFactory : public AbstractFactory<JointBodyPartFactory>
{
public:
  JointBodyPartFactory();
  virtual ~JointBodyPartFactory();
  static std::string name()
  {
    return "JointBodyPartFactory";
  }
  void init(PropertyBagInterface* _loader);
  GameComponent* useFactory(PropertyBagInterface* _parameters);
protected:
private:
  b2RevoluteJointDef jointDef;
  PhysicsManager* world;
};

#endif // JOINTBODYPARTFACTORY_H
