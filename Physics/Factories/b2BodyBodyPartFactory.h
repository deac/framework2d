#ifndef B2BODYBODYPARTFACTORY_H
#define B2BODYBODYPARTFACTORY_H

#include <AbstractFactory/AbstractFactory.h>
#include <Physics/BodyParts/BodyPart.h>
#include <Box2D/Dynamics/b2Body.h>
class PhysicsManager;

class b2BodyBodyPartFactory : public AbstractFactory<b2BodyBodyPartFactory>
{
 public:
  b2BodyBodyPartFactory();
  virtual ~b2BodyBodyPartFactory();
  void init(PropertyBagInterface* _loader);
  GameComponent* useFactory(PropertyBagInterface* _parameters);
  static std::string name()
  {
    return "b2BodyBodyPartFactory";
  }
 protected:
 private:
  PhysicsManager* physicsManager;
  b2BodyDef bodyDef;
};

#endif // B2BODYBODYPARTFACTORY_H
