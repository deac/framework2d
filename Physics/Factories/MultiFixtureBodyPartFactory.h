#ifndef MULTIFIXTUREBODYPARTFACTORY_H
#define MULTIFIXTUREBODYPARTFACTORY_H

#include <AbstractFactory/AbstractFactory.h>
#include <Physics/BodyParts/BodyPart.h>
#include <Box2D/Box2D.h>

class MultiFixtureBodyPartFactory : public AbstractFactory<MultiFixtureBodyPartFactory>
{
public:
  MultiFixtureBodyPartFactory();
  virtual ~MultiFixtureBodyPartFactory();
  void init(PropertyBagInterface* _loader);
  GameComponent* useFactory(PropertyBagInterface* _parameters);
  static std::string name()
  {
    return "MultiFixtureBodyPartFactory";
  }
protected:
private:
  FactoryInterface* bodyFactory;
  std::vector<FactoryInterface*> fixtureFactories;
};

#endif // MULTIFIXTUREBODYPARTFACTORY_H
