#ifndef CHARACTERBODYFACTORY_H
#define CHARACTERBODYFACTORY_H

#include <AbstractFactory/AbstractFactory.h>
class BodyPart;
class FactoryLoader;

class CharacterBodyFactory : public AbstractFactory<CharacterBodyFactory>
{
public:
  CharacterBodyFactory();
  virtual ~CharacterBodyFactory();
  void init(PropertyBagInterface* loader);
  GameComponent* useFactory(PropertyBagInterface* params);
  static std::string name()
  {
    return "CharacterBodyFactory";
  }
protected:
private:
  AbstractFactoryBase* bodyFactory;
  AbstractFactoryBase* wheelFactory;
  AbstractFactoryBase* jointFactory;
};

#endif // CHARACTERBODYFACTORY_H
