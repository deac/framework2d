#ifndef WATERPARTICLEBODYFACTORY_H
#define WATERPARTICLEBODYFACTORY_H

#include <AbstractFactory/AbstractFactory.h>
#include <Physics/BodyParts/BodyPart.h>

class FluidParticleBodyFactory : public AbstractFactory<FluidParticleBodyFactory>
{
public:
  FluidParticleBodyFactory();
  virtual ~FluidParticleBodyFactory();
  void init(PropertyBagInterface* _loader);
  GameComponent* useFactory(PropertyBagInterface* _parameters);
  static std::string name()
  {
    return "FluidParticleBodyFactory";
  }
protected:
private:
  AbstractFactoryBase* bodyFactory;
  AbstractFactoryBase* sensorFactory;
};

#endif // WATERPARTICLEBODYFACTORY_H
