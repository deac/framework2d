#include "MultiFixtureBodyPartFactory.h"

MultiFixtureBodyPartFactory::MultiFixtureBodyPartFactory()
{
    //ctor
}

MultiFixtureBodyPartFactory::~MultiFixtureBodyPartFactory()
{
    //dtor
}
void MultiFixtureBodyPartFactory::init(PropertyBagInterface* _loader)
{
  TypeTable* params = _loader->getTypeTable();
  bodyFactory = params->getInterface<FactoryInterface>("body", "b2BodyBodyPartFactory");

  fixtureFactories = params->getInterfaceArray<FactoryInterface>("fixtures", {"b2FixtureBodyPartFactory"});
}

GameComponent* MultiFixtureBodyPartFactory::useFactory(PropertyBagInterface* _parameters)
{
  TypeTable* params = _parameters->getTypeTable();
  GameComponent* body = bodyFactory->use(_parameters, nullptr);
  params->addValue<GameComponent*>("body", body);
  for (unsigned int i = 0; i != fixtureFactories.size(); i++)
    {
      fixtureFactories[i]->use(_parameters, body);
    }
  return body;
}
