#include "b2BodyBodyPartFactory.h"
#include <Physics/PhysicsManager.h>
#include <Physics/BodyParts/b2BodyBodyPart.h>

b2BodyBodyPartFactory::b2BodyBodyPartFactory()
{
  //ctor
  physicsManager = nullptr;
}

b2BodyBodyPartFactory::~b2BodyBodyPartFactory()
{
  //dtor
}

void b2BodyBodyPartFactory::init(PropertyBagInterface* _loader)
{
  TypeTable* params = _loader->getTypeTable();
  physicsManager = params->getValue<PhysicsManager*>("world", nullptr);
  bodyDef.bullet = params->getValue<bool>("bullet", false);
  bodyDef.fixedRotation = params->getValue<bool>("fixedRotation", false);
  bodyDef.type = static_cast<b2BodyType>(params->getValue<int>("bodyType", b2_dynamicBody)); /// FIXME loader needs to accept enums
}

GameComponent* b2BodyBodyPartFactory::useFactory(PropertyBagInterface* _parameters)
{
  TypeTable* params = _parameters->getTypeTable();
  bodyDef.position = params->getValue<Vec2f>("position", {0,0});
  bodyDef.linearVelocity = params->getValue<Vec2f>("velocity",{0,0});
  bodyDef.angle = params->getValue<float>("rotation", 0.0f);
  bodyDef.angularVelocity = params->getValue<float>("angularVelocity", 0.0f);

  b2Body* body = physicsManager->createBody(&bodyDef);
  b2BodyBodyPart* bodyPart = new b2BodyBodyPart(body);

  return bodyPart;
}
