#pragma once

#include <Types/Tree.h>
#include "WaterPhysicsSystem.h"
class WaterPhysicsSystem;
class Anchor;
class Vertex;
class Edge;
class Vec2f;

class Anchor: public Vertex
{
public:
  Anchor(const Vec2f& _position, Edge* _anchored, Boundary* _boundary);
  ~Anchor();
  Edge* getEdge(){return anchored;}
  Vertex* getLeftVertex();
  Vertex* getRightVertex();
  void setPosition(const Vec2f& _position){position = _position;}
private:
  Edge* anchored;
};
class Boundary: public ListNode<Boundary>
{
public:
  Boundary(WaterPhysicsSystem* _system);
  virtual ~Boundary();
  virtual void checkVolumes(){}
  virtual Vec2f getLeftPosition()=0;
  virtual Vec2f getRightPosition()=0;
  virtual void intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge)=0;
  virtual Vertex* getLeft()=0;
  virtual Vertex* getRight()=0;
  virtual Volume* getTop()=0;
  virtual Volume* getBottom()=0;
  WaterPhysicsSystem* getSystem(){return system;}
protected:
  WaterPhysicsSystem* system;
};

class UnfinishedBoundary: public Boundary
{
public:
  UnfinishedBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex, bool _leftEdge);
  ~UnfinishedBoundary();
  Vec2f getLeftPosition();
  Vec2f getRightPosition();
  void intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge);
  Vertex* getLeft();
  Vertex* getRight();
  Volume* getTop(){return nullptr;}
  Volume* getBottom(){return nullptr;}
private:
  RealVertex* vertex;
  bool leftEdge;
};
class GradientChangeBoundary: public Boundary
{
public:
  GradientChangeBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex, const Vec2f& _anchorPosition, Edge* _anchor, bool _leftEdge);
  ~GradientChangeBoundary();
  void checkVolumes();
  Vec2f getLeftPosition();
  Vec2f getRightPosition();
  void intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge);
  Vertex* getLeft();
  Vertex* getRight();
  Volume* getTop();
  Volume* getBottom();
private:
  Volume* top,* bottom;
  RealVertex* vertex;
  Anchor* anchor;
  bool leftEdge;
};

class BedBoundary: public Boundary
{
public:
  BedBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex);
  ~BedBoundary();
  Vec2f getLeftPosition();
  Vec2f getRightPosition();
  Vec2f getPosition(){return getLeftPosition();}
  void intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge);
  Vertex* getLeft();
  Vertex* getRight();
  Volume* getTop();
  Volume* getBottom();
private:
  RealVertex* vertex;
};
