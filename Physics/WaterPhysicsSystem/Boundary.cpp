#include "Boundary.h"
#include "WaterPhysicsSystem.h"


Boundary::Boundary(WaterPhysicsSystem* _system)
{
  system = _system;
  system->attachBoundary(this);
}

Boundary::~Boundary()
{
  system->detachBoundary(this);
}

UnfinishedBoundary::UnfinishedBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex, bool _leftEdge)
  :Boundary(_system)
{
  vertex = _vertex;
  leftEdge = _leftEdge;
}

UnfinishedBoundary::~UnfinishedBoundary()
{
  vertex->setBoundary(nullptr);
}
Vec2f UnfinishedBoundary::getLeftPosition()
{
  if (leftEdge)
    return vertex->position;
  else
    return vertex->position - Vec2f(100, 0);
}
Vec2f UnfinishedBoundary::getRightPosition()
{
  if (!leftEdge)
    return vertex->position;
  else
    return vertex->position + Vec2f(100, 0);
}

void UnfinishedBoundary::intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge)
{
  if (leftEdge != _leftEdge)
    {
      vertex->setBoundary(new GradientChangeBoundary(system, vertex, _position, _edge, leftEdge));
    }
}
Vertex* UnfinishedBoundary::getLeft()
{
  if (leftEdge)
    return vertex;
  else
    return nullptr;
}
Vertex* UnfinishedBoundary::getRight()
{
  if (!leftEdge)
    return vertex;
  else
    return nullptr;
}

GradientChangeBoundary::GradientChangeBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex, const Vec2f& _anchorPosition, Edge* _anchor, bool _leftEdge)
  :Boundary(_system)
{
  vertex = _vertex;
  anchor = new Anchor(_anchorPosition, _anchor, this);
  leftEdge = _leftEdge;
  top = bottom = nullptr;
  checkVolumes();
}

GradientChangeBoundary::~GradientChangeBoundary()
{
  vertex->setBoundary(nullptr);
}
Vec2f GradientChangeBoundary::getLeftPosition()
{
  if (leftEdge)
    return vertex->position;
  else
    return anchor->getPosition();
    }
Vec2f GradientChangeBoundary::getRightPosition()
{
  if (!leftEdge)
    return vertex->position;
  else
    if (anchor)
      return anchor->getPosition();
}
void GradientChangeBoundary::intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge)
{
  if (leftEdge != _leftEdge)
    {
      anchor->setPosition(_position);
    }
}
Vertex* GradientChangeBoundary::getLeft()
{
  if (leftEdge)
    return vertex;
  else
    return anchor;
}
Vertex* GradientChangeBoundary::getRight()
{
  if (!leftEdge)
    return vertex;
  else
    return anchor;
}
Volume* GradientChangeBoundary::getTop()
{
  return top;
}
Volume* GradientChangeBoundary::getBottom()
{
  return bottom;
}

BedBoundary::BedBoundary(WaterPhysicsSystem* _system, RealVertex* _vertex)
  :Boundary(_system)
{
  vertex = _vertex;
  system->makeSensor(this);
}

BedBoundary::~BedBoundary()
{
  vertex->setBoundary(nullptr);
}  

Vec2f BedBoundary::getLeftPosition()
{
  return vertex->getPosition();
}
Vec2f BedBoundary::getRightPosition()
{
  return vertex->getPosition();
}

void BedBoundary::intersect(Edge* _edge, const Vec2f& _position, bool _leftEdge)
{
  assert(false);
}
Vertex* BedBoundary::getLeft()
{
  return vertex;
}
Vertex* BedBoundary::getRight()
{
  return vertex;
}
Volume* BedBoundary::getTop()
{
  return vertex->getLeft()->getA()->getBoundary()->getBottom();
}
Volume* BedBoundary::getBottom()
{
  return nullptr;
}
