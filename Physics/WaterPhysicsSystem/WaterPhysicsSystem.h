#ifndef WATERPHYSICSSYSTEM_H
#define WATERPHYSICSSYSTEM_H

#include <Objects/GameComponent.h>
#include <Types/Vec2f.h>
#include <Types/Tree.h>
#include <unordered_map>
class FactoryInterface;
class b2FixtureBodyPart;
class WaterPhysicsSystem;
class RealVertex;
class Volume;
class Boundary;
class BedBoundary;
class Edge;

class Vertex
{
public:
  enum VertexShape
    {
      eLeft,
      eRight,
      eVertexShapeMax
    };
  Vertex(const Vec2f& _position);
  ~Vertex();
  Vec2f position;
  Boundary* getBoundary();
  void setBoundary(Boundary* _boundary);
  const Vec2f& getPosition(){return position;}
  virtual Vertex* getLeftVertex()=0;
  virtual Vertex* getRightVertex()=0;
protected:
  Boundary* boundary;
    
  //Edge* a,* b;
  //VertexType getVertexType();
  //VertexShape getVertexShape();
};
class RealVertex: public Vertex
{
public:
  enum VertexType
    {
      eBed,
      eRoof,
      eLeftWall,
      eRightWall,
      eDivergance,
      eConvergance,
      eVertexTypeMax,
    };
  RealVertex(const Vec2f& _position);
  ~RealVertex();
  void checkFinalised(WaterPhysicsSystem* _system, bool _leftEdge);
  VertexType getType();
  bool isLeftVertex();
  Edge* getLeft();
  Edge* getRight();
  void setLeft(Edge* _edge);
  void setRight(Edge* _edge);
  Vertex* getLeftVertex();
  Vertex* getRightVertex();
protected:
  Edge* left,* right;
};

class Edge
{
public:
  enum EdgeType
    {
      eLeftFloor,
      eRightFloor,
      eRightRoof,
      eLeftRoof,
      eEdgeTypeMax
    };
  Edge(){a = b = nullptr;}
  ~Edge();
  void setA(RealVertex* _a);
  void setB(RealVertex* _b);
  void setA(RealVertex* _a, const Vec2f& _bPosition);
  void setB(const Vec2f& _aPosition, RealVertex* _b);
  void setAB(RealVertex* _a, RealVertex* _b);
  RealVertex* getA(){return a;}
  RealVertex* getB(){return b;}
  Vec2f aPosition(){return a->position;}
  Vec2f bPosition(){return b->position;}
  EdgeType getEdgeType();
  Edge* split(Vec2f _point, Boundary* _boundary);
private:
  RealVertex* a,* b;
};

/*  class Boundary: public ListNode<Boundary>
  {
  public:
    Boundary(WaterPhysicsSystem* _system){system = _system; vertex = nullptr; top = bottom = nullptr; anchor = nullptr;}
    ~Boundary();
    void setLeft(RealVertex* _left);
    void setRight(RealVertex* _right);
    void setAnchor(Anchor* _anchor);
    Vertex* getLeft();
    Vertex* getRight();
    void removeVertex(Vertex* _removed);
    Vec2f getLeftPosition();
    Vec2f getRightPosition();
    void setTop(Volume* _top);
    void setBottom(Volume* _bottom);
    Volume* getBottom(){return bottom;}
    Volume* getTop(){return top;}
    void added(Vertex* _vertex);
    void removed(Vertex* _vertex);
  private:
    void checkVolumes();
    RealVertex* vertex;
    Anchor* anchor;
    bool leftVertex;
    Volume* top,* bottom;
    WaterPhysicsSystem* system;
    };*/
  class Volume: public ListNode<Volume>
  {
  public:
    enum VolumeType
      {
	eMiddle,
	eBottom,
	eTop
      };
    Volume(Boundary* _top, Boundary* _bottom);
    ~Volume();
    Vertex* getTopLeft();
    Vertex* getTopRight();
    Vertex* getBottomLeft();
    Vertex* getBottomRight();
    VolumeType getType(){return type;}
    void setTop(Boundary* _top);
    void setBottom(Boundary* _bottom);
    Boundary* getTop(){return top;}
    Boundary* getBottom(){return bottom;}
  private:
    Boundary* top,* bottom;
    VolumeType type;
  };

class WaterPhysicsSystem : public TypedGameComponent<WaterPhysicsSystem>
{
public:
  WaterPhysicsSystem();
  void init();
  virtual ~WaterPhysicsSystem();
  static std::string name()
  {
    return "WaterPhysicsSystem";
  }
  static void registerActions(GameComponentType* _type);
  void renderWireframe();
  void addFixture(b2FixtureBodyPart* _bodyPart);
  void _removeBoundary(Boundary* _boundary);
  std::vector<Vec2f> getContainer(const Vec2f& _position);
  std::vector<Vec2f> settledParticle(const Vec2f& _position, float _volume);
  void attachBoundary(Boundary* _boundary);
  void attachVolume(Volume* _volume);
  void detachBoundary(Boundary* _boundary);
  void detachVolume(Volume* _volume);
  Edge* findIntersectingEdge(Vec2f _a, Vec2f* _b, Edge* _exception = nullptr, Edge* _exception2 = nullptr);
  void makeSensor(BedBoundary* _boundary);
protected:
private:
  std::string virtualPrint();
  Edge* addEdge(const Vec2f& _a, const Vec2f& _b, b2FixtureBodyPart* _destructionListener);
  Edge* addEdge(RealVertex* _a, RealVertex* _b, b2FixtureBodyPart* _destructionListener);

  void addFinalisedVertex(RealVertex* _vertex, Edge* _edge, bool _leftEdge);
  void addFinalisedEdge(Edge* _edge);
  void createVertex(const Vec2f& _position, Edge* _a, Edge* _b, Vec2f* _bBPosition = nullptr);

  //std::unordered_map<b2FixtureBodyPart*, std::vector<Edge>> edgeMap;
  std::vector<Edge*> edges;
  List<Volume> volumes;
  List<Boundary> boundaries;
  FactoryInterface* sensorFactory;
  FactoryInterface* waterBodyFactory;
};

#endif // WATERPHYSICSSYSTEM_H
