#include "WaterPhysicsSystem.h"
#include "Boundary.h"
#include <AbstractFactory/InstanceParameters.h>
#include <Physics/BodyParts/b2FixtureBodyPart.h>
#include <Filesystem/Filesystem.h>
#include <Box2D/Box2D.h>
#include <Types/Vec2f.h>

WaterPhysicsSystem::WaterPhysicsSystem()
{
}

void WaterPhysicsSystem::init()
{
  //ctor
  sensorFactory = nullptr; //_factories->getFactory<Entity>("WaterMeshSensor");
  std::vector<Vec2f> points = {{-10, -11}, {-5, -6}, {-10, 0}, {-7, 10}, {5, 5}, {10, -10}};
  RealVertex* vertex = addEdge(points[0], points[1], nullptr)->getB();
  Vertex* hook;
  for (unsigned int i = 2; i != points.size(); i++)
    {
      RealVertex* newVertex = new RealVertex(points[i]);
      if (points[i] == Vec2f(-10, 0))
	hook = newVertex;
      addEdge(vertex, newVertex, nullptr);
      vertex = newVertex;
    }
  /*  addEdge({5,5}, {10,-10}, nullptr);
  addEdge({-10,0}, {-7,10}, nullptr);
  addEdge({-5,-6}, {-10,0}, nullptr);
  addEdge({-7,10}, {5,5}, nullptr);*/
  /*  for (unsigned int i = 0; i != edges.size(); i++)
    {
      Edge* edge = edges[i];
      edge->getA()->checkFinalised();
      edge->getB()->checkFinalised();
      }*/
}

WaterPhysicsSystem::~WaterPhysicsSystem()
{
  //dtor
}

void WaterPhysicsSystem::registerActions(GameComponentType* _type)
{

}
#include <iostream>
#define OUTPUT_VEC2F (a, b) a.x << ',' << a.y << ")->(" << b.x << ',' << b.y << ')'
void WaterPhysicsSystem::addFixture(b2FixtureBodyPart* _bodyPart)
{
  /*  b2Fixture* fixture = _bodyPart->getFixture();
  b2Shape* shape = fixture->GetShape();
  assert(dynamic_cast<b2PolygonShape*>(shape)); /// Can't support circles at this time
  b2PolygonShape* polygon = static_cast<b2PolygonShape*>(shape);
  for (int i = 0; i != polygon->GetVertexCount(); i++)
    {
      Vec2f a = polygon->GetVertex(i);
      Vec2f b = polygon->GetVertex((i+1)%polygon->GetVertexCount());
      std::cout << "Edges size: " << edges.size() << std::endl;
      for (unsigned int i = 0; i != edges.size(); i++)
        {
	  Vec2f a = edges[i]->aPosition();
	  Vec2f b = edges[i]->bPosition();
	  std::cout << "\t (" << a.x << ',' << a.y << ")->(" << b.x << ',' << b.y << ')' << std::endl;
        }
      std::cout << "Adding (" << a.x << ',' << a.y << ")->(" << b.x << ',' << b.y << ')' << std::endl;
      addEdge(a, b, _bodyPart);
    }
  std::cout << "Edges size: " << edges.size() << std::endl;
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      Vec2f a = edges[i]->aPosition();
      Vec2f b = edges[i]->bPosition();
      std::cout << "\t (" << a.x << ',' << a.y << ")->(" << b.x << ',' << b.y << ')' << std::endl;
      }*/
}

Edge* WaterPhysicsSystem::addEdge(const Vec2f& _a, const Vec2f& _b, b2FixtureBodyPart* _destructionListener)
{
  return addEdge(new RealVertex(_a), new RealVertex(_b), _destructionListener);
}
Edge* WaterPhysicsSystem::addEdge(RealVertex* _a, RealVertex* _b, b2FixtureBodyPart* _destructionListener)
{
  Edge* edge = new Edge;
  edge->setAB(_a, _b);
  addFinalisedEdge(edge);
  return edge;
  /*  Edge* intersections[2] = {0, 0};
  unsigned int numIntersections = 0;
  std::set<Edge*> ignoreSet;
  ignoreSet.insert(edge);
  if (_a->edges.size() != 0)
    ignoreSet.insert(_a->edges[0].edge);
  if (_b->edges.size() != 0)
    ignoreSet.insert(_b->edges[0].edge);
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      if (ignoreSet.find(edges[i]) == ignoreSet.end())
      try
        {
	  Vec2f position = intersectionPosition(_a->position, _b->position, edges[i]->aPosition(), edges[i]->bPosition());
	  assert(numIntersections < 2);
	  intersections[numIntersections] = edges[i];
	  numIntersections++;
        }
      catch (int ii)
        {
        }
    }
  switch (numIntersections)
    {
    case 0:
      {
	addFinalisedEdge(edge);
	break;
      }
    case 1:
      {
	float scalar = intersectionValue(_a->position, _b->position, intersections[0]->aPosition(), intersections[0]->bPosition());
	if (scalar < 0.2f)
	  {
	    Vec2f position = intersectionPosition(_a->position, _b->position, intersections[0]->aPosition(), intersections[0]->bPosition());
	    edge->setB(position, new Vertex(_b));
	    createVertex(position, intersections[0], edge);
	  }
	else if (scalar > 0.8f)
	  {
	    Vec2f position = intersectionPosition(_a->position, _b->position, intersections[0]->aPosition(), intersections[0]->bPosition());
	    edge->setA(new Vertex(_a), position);
	    createVertex(position, edge, intersections[0]);
	  }
	else
	  {
	    std::cout << "Error: scalar is " << scalar << std::endl;
	    assert(false);
	  }
	addFinalisedEdge(edge);
	break;
      }
    case 2:
      {
	float scalar0 = intersectionValue( _a, _b, intersections[0]->aPosition(), intersections[0]->bPosition());
	float scalar1 = intersectionValue( _a, _b, intersections[1]->aPosition(), intersections[1]->bPosition());
	if (scalar0 > scalar1)
	  {
	    {
	      Edge* temp = intersections[0];
	      intersections[0] = intersections[1];
	      intersections[1] = temp;
	    }
	    {
	      float temp = scalar0;
	      scalar0 = scalar1;
	      scalar1 = temp;
	    }
	  }
	Vec2f position0 = positionFromScalarValue(_a, _b, scalar0); /// FIXME might be the wrong line
	Vec2f position1 = positionFromScalarValue(_a, _b, scalar1);

	createVertex(position0, intersections[0], edge, &position1);
	createVertex(position1, edge, intersections[1]);
	addFinalisedEdge(edge);
	break;
      }
    default:
      {
	assert(false);
      }
    }
  return edge;*/
  //edges.push_back({_a, _b});
}

void WaterPhysicsSystem::makeSensor(BedBoundary* _boundary)
{
  InstanceParameters params(nullptr, "sensor");
  params.getTypeTable()->addValue<Vec2f>("position", _boundary->getPosition());
  params.getTypeTable()->addValue<BedBoundary*>("boundary", _boundary);
  //sensorFactory->build(&params, this);
}
Edge* WaterPhysicsSystem::findIntersectingEdge(Vec2f _a, Vec2f* _b, Edge* _exception, Edge* _exception2)
{
  Edge* ret = nullptr;
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      if (edges[i] != _exception && edges[i] != _exception2)
	try
	  {
	    *_b = intersectionPosition(_a, *_b, edges[i]->aPosition(), edges[i]->bPosition());
	    ret = edges[i];
	  }
	catch (int i)
	  {
	  }
    }
  return ret;
}
//void WaterPhysicsSystem::addFinalisedVertex(RealVertex* _vertex, Edge* _edge, bool _leftEdge)
void RealVertex::checkFinalised(WaterPhysicsSystem* _system, bool _leftEdge)
{
  Boundary* boundary = nullptr;
  if (getType() == RealVertex::eBed)
    boundary = new BedBoundary(_system, this);
  else
    {
      Vec2f a = position;
      Vec2f b;
      Vertex* left;
      Vertex* right;
      if (_leftEdge)
	{
	  b = a + Vec2f(500, 0);
	  left = this;
	}
      else
	{
	  b = a - Vec2f(500, 0);
	  right = this;
	}
      Edge* opposingEdge = _system->findIntersectingEdge(a, &b, getLeft(), getRight());
      if (opposingEdge)
	{
	  boundary = new GradientChangeBoundary(_system, this, b, opposingEdge, _leftEdge);
	}
      else
	{
	  boundary = new UnfinishedBoundary(_system, this, _leftEdge);
	}
    }
  setBoundary(boundary);
}
void WaterPhysicsSystem::addFinalisedEdge(Edge* _edge)
{
  edges.push_back(_edge);
  Edge::EdgeType type = _edge->getEdgeType();
  bool leftEdge;
  if (type == Edge::eLeftFloor || type == Edge::eLeftRoof)
    {
      leftEdge = true;
    }
  else
    {
      leftEdge = false;
    }
  Vec2f output = _edge->aPosition();
  std::cout << "Check1 : " << output << std::endl;
  _edge->getA()->checkFinalised(this, leftEdge);
  output = _edge->bPosition();
  std::cout << "Check2 : " << output << std::endl;
  _edge->getB()->checkFinalised(this, leftEdge);

  for (Boundary* boundary = boundaries.front(); boundary; boundary = boundary->getNextNode())
    {
      try
	{
	  Vec2f position = intersectionPosition(_edge->aPosition(), _edge->bPosition(), boundary->getLeftPosition(), boundary->getRightPosition());
	  if (position == boundary->getLeftPosition() || position == boundary->getRightPosition()) /// FIXME this is lazy
	    continue;
	  boundary->intersect(_edge, position, leftEdge);
	}
      catch (int i)
	{}
    }
  /*
  edges.push_back(_edge);
  Vec2f a = _edge->aPosition();
  Vertex* vertex = _edge->getA();
  bool jump = true;
  goto ACTUAL_START;
 START:
  vertex = _edge->getB();
  jump = false;
 ACTUAL_START:
  Vec2f b;
  Edge::EdgeType type = _edge->getEdgeType();
  bool leftEdge;
vv  if (type == Edge::eLeftFloor | type == Edge::eLeftRoof)
    {
      b = a + Vec2f(500, 0);
      leftEdge = true;
    }
  else
    {
      b = a - Vec2f(500, 0);
      leftEdge = false;
    }
  Edge* opposingEdge = findIntersectingEdge(a, &b, _edge);
  if (opposingEdge)
    {
      Vec2f a2 = opposingEdge->aPosition();
      Vec2f b2 = a2 + Vec2f((leftEdge)?-500:500,0);
      Edge* opposingOpposingEdge = findIntersectingEdge(a2, &b2, opposingEdge);
      if (opposingOpposingEdge == _edge)
	{
	  Edge* selfSplit = _edge->split(b2, nullptr);
	  edges.push_back(selfSplit);
	}
      //      else
      {
	a2 = opposingEdge->bPosition();
	b2 = a2 + Vec2f((leftEdge)?-500:500,0);
	opposingOpposingEdge = findIntersectingEdge(a2, &b2, opposingEdge);
	if (opposingOpposingEdge == _edge)
	  {
	    Edge* selfSplit = _edge->split(b2, nullptr);
	    edges.push_back(selfSplit);
	  }
      }
      Edge* newEdge = opposingEdge->split(b, nullptr);
      edges.push_back(newEdge);
    }
  if (jump)
    {
      a = _edge->bPosition();
      goto START;
      }*/
}
void WaterPhysicsSystem::createVertex(const Vec2f& _position, Edge* _a, Edge* _b, Vec2f* _bBPosition)
{
  RealVertex* vertex = nullptr;
  RealVertex* oldA = _a->getB();
  RealVertex* oldB = _b->getA();
  if (oldA == nullptr)
    {
      if (oldB == nullptr)
        {
	  vertex = new RealVertex(_position);
        }
      else
        {
	  vertex = oldB;
	  vertex->position = _position;
        }
    }
  else
    {
      vertex = oldA;
      vertex->position = _position;
    }
  assert(vertex);
  _a->setB(vertex);
  if (_bBPosition)
    _b->setA(vertex, *_bBPosition);
  else
    _b->setA(vertex);
}

std::vector<Vec2f> WaterPhysicsSystem::settledParticle(const Vec2f& _position, float _volume)
{
  RealVertex* bed = nullptr;
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      Edge* edge = edges[i];
      if (edge->getA()->getType() == RealVertex::eBed)
	{
	  bed = edge->getA();
	  break;
	}
      else if (edge->getB()->getType() == RealVertex::eBed)
	{
	  bed = edge->getB();
	  break;
	}
    }
  
}
Vec2f intersectionPosition(Vec2f _aStart, Vec2f _aEnd, Vec2f _bStart, Vec2f _bEnd, bool _aNeedsIntersect, bool _bNeedsIntersect);

std::vector<Vec2f> WaterPhysicsSystem::getContainer(const Vec2f& _position)
{
  Vec2f a = _position;
  Vec2f b = a;
  a.x -= 10000;
  b.x += 10000;

  Edge* left = nullptr;
  float leftX;
  Edge* right = nullptr;
  float rightX;
  unsigned int edgesSize = edges.size();
  for (unsigned int i = 0; i != edgesSize; i++)
    {
      Edge* edge = edges[i];
      try
        {
	  Vec2f position = intersectionPosition(edge->aPosition(), edge->bPosition(), a, b, true, false);
	  if (position.x < _position.x)
            {
	      if (left)
                {
		  if (leftX < position.x)
                    {
		      left = edge;
		      leftX = position.x;
                    }
                }
	      else
                {
		  left = edge;
		  leftX = position.x;
                }
            }
	  else
            {
	      if (right)
                {
		  if (rightX > position.x)
                    {
		      right = edge;
		      rightX = position.x;
                    }
                }
	      else
                {
		  right = edge;
		  rightX = position.x;
                }
            }
        }
      catch (int i)
        {
        }
    }
  if (left == nullptr || right == nullptr)
    {
      assert(false);
    }
  std::vector<Vec2f> shape;
  shape.push_back({rightX, _position.y});
  while (true)
    {
      if (right == left)
        {
	  shape.push_back({leftX, _position.y});
	  return shape;
        }
      //if (right->getA()->getVertexShape() == Vertex::eLeft) /// FIXME make this get the next on the vertex
      {
	shape.push_back(right->aPosition());
	right = right->getA()->getRight();
      }
      //else assert(false);
    }
}

#include <GL/gl.h>
#include <set>
void WaterPhysicsSystem::renderWireframe()
{
  std::set<Vertex*> vertices;
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      Edge* edge = edges[i];
      vertices.insert(edge->getA());
      vertices.insert(edge->getB());
      glBegin(GL_LINES);
      {
	glColor3f(1,1,1);
	glVertex2f(edge->aPosition().x, edge->aPosition().y);
	glVertex2f(edge->bPosition().x, edge->bPosition().y);
      }
      glEnd();
      glBegin(GL_POINTS);
      {
	glColor3f(1,0,0);
	glVertex2f(edge->aPosition().x, edge->aPosition().y);
	glVertex2f(edge->bPosition().x, edge->bPosition().y);
      }
      glEnd();
    }
  //   std::cout << "Vertices: " << vertices.size() << std::endl;
  for (auto iter = vertices.begin() ;iter != vertices.end(); iter++)
    {
      Vertex* vertex = *iter;
      Vec2f p = vertex->position;
    }
  glColor3f(0,0,1);
  unsigned int num = 0;
  for (Volume* volume = volumes.front(); volume; volume = volume->getNextNode())
    {
      num++;
      //      if (volume->getTopRight() && volume->getTopLeft())
	{
	  Vec2f center;
	  center = volume->getTopLeft()->position;
	  center += volume->getBottomLeft()->position;
	  center += volume->getTopRight()->position;
	  center += volume->getBottomRight()->getPosition();
	  center /= 4.0f;
	  glBegin(GL_LINES);
	  {
	    glVertex2f(volume->getTopLeft()->position.x, volume->getTopLeft()->position.y);
	    glVertex2f(center.x, center.y);
	    glVertex2f(volume->getTopRight()->position.x, volume->getTopRight()->position.y);
	    glVertex2f(center.x, center.y);

	    glVertex2f(volume->getBottomRight()->position.x, volume->getBottomRight()->position.y);
	    glVertex2f(center.x, center.y);
	    glVertex2f(volume->getBottomLeft()->position.x, volume->getBottomLeft()->position.y);
	    glVertex2f(center.x, center.y);
	  }
	  glEnd();
	}
    }
  num = 0;
  glBegin(GL_LINES);
  glColor3f(1,1,0.5f);
  unsigned int boundariesSize = 0;
  for (Boundary* boundary = boundaries.front(); boundary; boundary = boundary->getNextNode())
    //  for (unsigned int i = 0; i != edges.size(); i++)
    {
      boundariesSize++;
      //      Boundary* boundary = edges[i]->getA()->getBoundary();
      //      if (!boundary) continue;
      Vec2f p = boundary->getLeftPosition();
      glVertex2f(p.x, p.y);
      p = boundary->getRightPosition();
      glVertex2f(p.x, p.y);
    }
  glEnd();
}
/*Vertex::VertexShape WaterPhysicsSystem::Vertex::getVertexShape()
  {
  Vec2f dirA = a->aPosition() - position;
  Vec2f dirB = b->bPosition() - position;
  dirA /= dirA.Length();
  dirB /= dirB.Length();
  float angleA = atan2(dirA.x, dirA.y);
  float angleB = atan2(dirB.x, dirB.y);
  float angleDiff = angleB - angleA;
  return eLeft;
  }*/

void Edge::setA(RealVertex* _a)
{
  if (a)
    if (a == _a)
      return;
    else
      a->setRight(nullptr);
  a = _a;
  a->setRight(this);
}
void Edge::setB(RealVertex* _b)
{
  if (b)
    if (b == _b)
      return;
    else
      b->setLeft(nullptr);
  b = _b;
  b->setLeft(this);
}
void Edge::setA(RealVertex* _a, const Vec2f& _b)
{
  if (a)
    {
      RealVertex* oldA = a;
      a = nullptr;
      oldA->setRight(nullptr);
    }
  a = _a;
  a->setRight(this);
}
void Edge::setB(const Vec2f& _a, RealVertex* _b)
{
  if (b)
    {
      RealVertex* oldB = b;
      b = nullptr;
      oldB->setLeft(nullptr);
    }
  b = _b;
  b->setLeft(this);
}
void Edge::setAB(RealVertex* _a, RealVertex* _b)
{
  if (a)
    a->setRight(nullptr);
  if (b)
    b->setLeft(nullptr);
  a = _a;
  b = _b;
  a->setRight(this);
  b->setLeft(this);
}
Vertex::Vertex(const Vec2f& _position)
{
  position = _position;
  boundary = nullptr;
}
RealVertex::RealVertex(const Vec2f& _position)
  :Vertex(_position)
{
  left = right = nullptr;
}
void Vertex::setBoundary(Boundary* _boundary)
{
  if (boundary != _boundary)
    {
      if (boundary)
	{
	  Boundary* b = boundary;
	  boundary = nullptr;
	  delete b;
	}
      boundary = _boundary;
    }
}
/*void Boundary::removeVertex(Vertex* _vertex)
{
  if (_vertex == left)
    {
      left = nullptr;
      //      if (right == nullptr)
	//	system->_removeBoundary(this);
//	      delete this;
    }
  else if (_vertex == right)
    {
      assert(_vertex == right);
      right = nullptr;
      //      if (left == nullptr)
	// delete this
	//	system->_removeBoundary(this);
    }
}
Edge* Vertex::getNextOutfacing(Edge* _b)
{
  for (unsigned int i = 0; i != edges.size(); i++)
    {
      if (edges[i].edge == _b)
        {
	  i++;
	  if (i == edges.size())
	    i = 0;
	  return edges[i].edge;
        }
    }
  throw -1;
}
void Vertex::addInwardsEdge(Edge* _edge)
{
  addInwardsEdge(_edge->aPosition(), _edge);
}
void Vertex::addOutwardsEdge(Edge* _edge)
{
  addOutwardsEdge(_edge, _edge->bPosition());
}
Vertex::~Vertex()
{
  assert(false);
  }*/
void GradientChangeBoundary::checkVolumes()
{
  float closestDown;
  Vertex* nextDown = nullptr;
  Vertex* nextUp = nullptr;
  
  Vertex* left;
  Vertex* right;
  if (leftEdge)
    {
      left = vertex;
      right = anchor;
      if (vertex->getRight())
	nextDown = left->getRightVertex();
      if (nextDown)
	{
	  if (right->getLeftVertex()->getPosition().y < nextDown->getPosition().y)
	    nextDown = right->getLeftVertex();
	}
      else
	{
	  nextDown = right->getLeftVertex();
	}
      nextUp = left->getLeftVertex();
      if (nextUp)
	{
	  if (right->getRightVertex()->getPosition().y > nextUp->getPosition().y)
	    nextUp = right->getRightVertex();
	}
      else
	{
	  nextUp = right->getRightVertex();
	}
    }
  else
    {
      left = anchor;
      right = vertex;
      if (vertex->getLeft())
	{
	  nextDown = left->getRightVertex();
	  if (right->getLeftVertex()->getPosition().y < nextDown->getPosition().y)
	    nextDown = right->getLeftVertex();
	}
      if (vertex->getRight())
	{
	  nextUp = left->getLeftVertex();
	  if (nextUp)
	    if (right->getRightVertex()->getPosition().y > nextUp->getPosition().y)
	      nextUp = right->getRightVertex();
	    else
	      nextUp = right->getRightVertex();
	}
    }
  //assert(nextDown->position.y > vertex->position.y);
  //assert(nextUp->position.y < vertex->position.y);

  if (!bottom && nextDown && nextDown->getBoundary() && anchor && vertex && nextDown->getBoundary()->getLeft() && nextDown->getBoundary()->getRight())
    {
      if (nextDown->getBoundary()->getTop())
	bottom = nextDown->getBoundary()->getTop();
      else
	{
	  Volume* b = new Volume(this, nextDown->getBoundary());
	  system->attachVolume(b);
	  bottom = b;
	}
    }
  else
    {
      Vec2f v = left->getPosition();
      std::cout << "Could not create bottom volume for " << v << std::endl;
    }
  RealVertex* v = dynamic_cast<RealVertex*>(nextUp);
  if (!top && nextUp && nextUp->getBoundary() && anchor && vertex && nextUp->getBoundary()->getLeft() && nextUp->getBoundary()->getRight())
    {
      /*            Vertex* pointlessFace = nextUp->getBoundary()->getLeft();
	    if (pointlessFace)
	      Vec2f pointless = pointlessFace->getPosition();*/
      Volume* t = new Volume(nextUp->getBoundary(), this);
      system->attachVolume(t);
      top = t;
    }
}


/*void Vertex::addInwardsEdge(const Vec2f& _a, Edge* _edge)
{
  assert(_edge->getB() == this);
  Vec2f dir = _a - position;
  dir /= dir.Length();
  float angle = atan2(dir.y, dir.x);
 PART_1:
  {
    for (auto i = edges.begin(); i != edges.end(); i++)
      {
	if (i->angle > angle)
	  {
	    edges.insert(i, {angle, _edge});
	    goto PART_2;
	  }
      }
    edges.push_back({angle, _edge});
  }
 PART_2:
  {
    if (getType() == VertexType::eBed)
      {
	std::cout << "Bed: " << position << std::endl;
	delete boundary;
	boundary = nullptr;
      }
  }
}
void Vertex::addOutwardsEdge(Edge* _edge, const Vec2f& _b)
{
  assert(_edge->getA() == this);
  Vec2f dir = _b - position;
  dir /= dir.Length();
  float angle = atan2(dir.y, dir.x);
 PART_1:
  {
    for (auto i = edges.begin(); i != edges.end(); i++)
      {
	if (i->angle > angle)
	  {
	    edges.insert(i, {angle, _edge});
	    goto PART_2;
	  }
      }
    edges.push_back({angle, _edge});
  }
 PART_2:
  {
    if (getType() == VertexType::eBed)
      {
	std::cout << "Bed: " << position << std::endl;
	delete boundary;
	boundary = nullptr;
      }
  }
}
void Vertex::removeEdge(Edge* _b)
{
  for (auto i = edges.begin(); i != edges.end(); i++)
    {
      if (i->edge == _b)
        {
	  edges.erase(i);
	  if (edges.size() == 0)
	    delete this;
	  return;
        }
    }
  throw -1;
}*/
Edge* RealVertex::getLeft()
{
  return left;
}
Edge* RealVertex::getRight()
{
  return right;
}
void RealVertex::setLeft(Edge* _edge)
{
  if (left)
    {
      Edge* l = left;
      left = nullptr;
      l->setB(nullptr);
    }
  left = _edge;
  try
    {
      checkFinalised(boundary->getSystem(), isLeftVertex());
    }
  catch (int i){}
}
void RealVertex::setRight(Edge* _edge)
{
  if (right)
    {
      Edge* r = right;
      right = nullptr;
      r->setA(nullptr);
    }
  right = _edge;
  try
    {
      checkFinalised(boundary->getSystem(), isLeftVertex());
    }
  catch (int i){}
}
RealVertex::VertexType RealVertex::getType()
{
  VertexType type = VertexType::eVertexTypeMax;
  //  if (edges.size() == 2 && edges[0].edge->getA() && edges[0].edge->getB() &&
  //      edges[1].edge->getA() && edges[1].edge->getB())
  if (left && right)
    {
    /*  -y
	
	0|3
     -x -|- x
	2|1
	
	 y
  0-0 0
  0-1 1
  1-0 2
  1-1 3
    */
      int quadrants[2];
      Edge* edges[2];
      edges[0] = left;
      edges[1] = right;
      for (unsigned int i = 0; i != 2; i++)
	{
	  Edge* edge = edges[i];
	  Vec2f direction = (i == 1)? edge->aPosition() - edge->bPosition() : edge->bPosition() - edge->aPosition();
	  std::cout << "Direction " << i << " " << direction << std::endl;
	  if (direction.x < 0)
	    if (direction.y < 0)
	      quadrants[i] = 1;
	    else
	      quadrants[i] = 3;
	  else
	    if (direction.y < 0)
	      quadrants[i] = 2;
	    else
	      quadrants[i] = 0;
	}
      if (edges[0]->getA() == this)
	{
	  //	  quadrants[0] &= !(quadrants[0] & 1);
	  //	  quadrants[1] &= !(quadrants[1] & 1);
	  std::cout << "Flipped ";
	}
      switch (quadrants[0])
	{
	case 0:
	  {
	    switch (quadrants[1])
	      {
	      case 0:
		{
		  type = VertexType::eBed;
		  break;
		}
	      case 1:
		{
		  type = VertexType::eLeftWall;
		  break;
		}
	      case 2:
		{
		  type = VertexType::eRightWall;
		  break;
		}
	      case 3:
		{
		  type = VertexType::eBed;
		  break;
		}
	      };
	    break;
	  }
	case 1:
	  {
	    switch (quadrants[1])
	      {
	      case 0:
		{
		  type = VertexType::eRightWall;
		  break;
		}
	      case 1:
		{
		  type = VertexType::eRoof;
		  break;
		}
	      case 2:
		{
		  type = VertexType::eRoof;
		  break;
		}
	      case 3:
		{
		  type = VertexType::eRightWall;
		  break;
		}
	      };
	    break;
	  }
	case 2:
	  {
	    switch (quadrants[1])
	      {
	      case 0:
		{
		  type = VertexType::eRightWall;
		  break;
		}
	      case 1:
		{
		  type = VertexType::eConvergance;
		  break;
		}
	      case 2:
		{
		  type = VertexType::eConvergance;
		  break;
		}
	      case 3:
		{
		  type = VertexType::eRightWall;
		  break;
		}
	      };
	    break;
	  }
	case 3:
	  {
	    switch (quadrants[1])
	      {
	      case 0:
		{
		  type = VertexType::eDivergance;
		  break;
		}
	      case 1:
		{
		  type = VertexType::eLeftWall;;
		  break;
		}
	      case 2:
		{
		  type = VertexType::eLeftWall;
		  break;
		}
	      case 3:
		{
		  type = VertexType::eBed;
		  break;
		}
	      };
	    break;
	  }
	};
      if ((quadrants[0] == 0 || quadrants[0] == 3) && (quadrants[1] == 0 || quadrants[1] == 3))
	type = VertexType::eBed;
      if ((quadrants[0] == 1 || quadrants[0] == 2) && (quadrants[1] == 1 || quadrants[1] == 2))
	type = VertexType::eRoof;
      std::cout << "Quadrants: " << quadrants[0] << ' ' << quadrants[1] << std::endl;;
    }
  std::cout << "Vertex: " << position << "  Typing: " << type << std::endl << std::endl;
  return type;
}
Edge::EdgeType Edge::getEdgeType()
{
  if (aPosition().y < bPosition().y)
    { /// Downwards pointing
      if (aPosition().x < bPosition().x)
	{
	  return eLeftFloor;
	}
      else
	{
	  return eLeftRoof;
	}
    }
  else
    { /// Upwards pointing
      if (aPosition().x < bPosition().x)
	{
	  return eRightFloor;
	}
      else
	{
	  return eRightRoof;
	}
    }
}
bool RealVertex::isLeftVertex()
{
  VertexType type = getType();
  switch (type)
    {
    case eLeftWall:
      return true;
    case eRightWall:
      return false;
    default:
      throw -1;
    };
}
Anchor::Anchor(const Vec2f& _position, Edge* _anchored, Boundary* _boundary)
  :Vertex(_position)
{
  anchored = _anchored;
  setBoundary(_boundary);
}
Vertex::~Vertex()
{
}
RealVertex::~RealVertex()
{
  Boundary* b = boundary;
  boundary = nullptr;
  delete b;
}

Vertex* Anchor::getLeftVertex()
{
  return anchored->getA();
}
Vertex* Anchor::getRightVertex()
{
  return anchored->getB();
}
Vertex* RealVertex::getLeftVertex()
{
  if (left)
    return left->getA();
  else
    return nullptr;
}
Vertex* RealVertex::getRightVertex()
{
  if (right)
    return right->getB();
  else
    return nullptr;
}
Boundary* Vertex::getBoundary()
{
  return boundary;
}

void WaterPhysicsSystem::attachBoundary(Boundary* _boundary)
{
  boundaries.append(_boundary);
}
void WaterPhysicsSystem::detachBoundary(Boundary* _boundary)
{
  boundaries.remove(_boundary);
}
void WaterPhysicsSystem::attachVolume(Volume* _volume)
{
  static std::set<Volume*> staticVolumes;
  if (staticVolumes.find(_volume) != staticVolumes.end())
    assert(false);
  staticVolumes.insert(_volume);
    volumes.append(_volume);
}
void WaterPhysicsSystem::detachVolume(Volume* _volume)
{
  volumes.remove(_volume);
}

Vertex* Volume::getTopLeft()
{
  return top->getLeft();
}
Vertex* Volume::getTopRight()
{
  return top->getRight();
}
Vertex* Volume::getBottomLeft()
{
  return bottom->getLeft();
}
Vertex* Volume::getBottomRight()
{
  return bottom->getRight();
}
Volume::Volume(Boundary* _top, Boundary* _bottom)
{
  static int num = 0;
  num++;
  if (num == 7)
    assert(false);
  top = _top;
  bottom = _bottom;
}

Volume::~Volume()
{
}




















