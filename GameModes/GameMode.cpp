#include "GameMode.h"
#include <Graphics/GraphicsManager.h>
#include <Objects/GameObject.h>
#include <Input/InputManager.h>
#include <Level/Level.h>
#include <Filesystem/Filesystem.h>
#include <Editor/Editor.h>
#include <Game.h>
#include <cassert>

GameMode::GameMode()
  :TypedGameComponent<GameMode>("gameMode", Filesystem::global(), true)
{
    //ctor
}

GameMode::~GameMode()
{
    //dtor
}

void GameMode::registerActions(GameComponentType* _type)
{
  _type->createActionHandle("run", &GameMode::run);
  
}

bool GameMode::update()
{
    return v_Update();
}

void GameMode::run()
{
  while (update());
}
