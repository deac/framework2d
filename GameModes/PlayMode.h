#ifndef PLAYMODE_H
#define PLAYMODE_H

#include "GameMode.h"
#include <Input/Mouse/ClickDragEvent.h>
#include <AI/Brain.h>
#include <AI/BrainFactory/PlayerInputBrainFactory.h>
class b2Body;
class PlayerInputBrain;
class GameServerInterface;

class PlayMode : public ClickDragEvent, public GameMode
{
    public:
        PlayMode();
        virtual ~PlayMode();
        void start(unsigned char button);
        void mouseMove(Vec2i mouse);
        bool activate(const CEGUI::EventArgs&);
        Level* getLevel(){return activeLevel;}
    protected:
        bool v_Update();
        void setLevel(Level* _level);
        PlayerInputBrain* playerOneBrain;
    private:
        Level* activeLevel;
};

#endif // PLAYMODE_H
