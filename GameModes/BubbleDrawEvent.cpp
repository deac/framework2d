#include "BubbleDrawEvent.h"
#include <AbstractFactory/InstanceParameters.h>

BubbleDrawEvent::BubbleDrawEvent(SelectionBox* _selectionBox, FactoryInterface* _bubbleFactory)
{
    //ctor
    selectionBox = _selectionBox;
    bubbleFactory = _bubbleFactory;
}

BubbleDrawEvent::~BubbleDrawEvent()
{
    //dtor
}

void BubbleDrawEvent::start(unsigned char button)
{

}
void BubbleDrawEvent::mouseMove(Vec2i mouse)
{

}
void BubbleDrawEvent::buttonUp(Vec2i mouse, unsigned char button)
{
    Vec2f position = startPos.ScreenToWorldSpace();
    float radius = (position-mouse.ScreenToWorldSpace()).Length();
    if (radius != 0.0f)
    {
      InstanceParameters _parameters(nullptr, "bubble");
        _parameters.getTypeTable()->addValue<Vec2f>("position",position);
        _parameters.getTypeTable()->addValue<float>("radius",radius);
        _parameters.getTypeTable()->addValue<std::string>("materialName","defaultBubble");
        //def.type = (Bubble::BubbleType)selectionBox->getCurrentSelection();
        //bubbleFactory->build(&_parameters, nullptr);
    }
}

