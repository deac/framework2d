#ifndef GAMEMODE_H
#define GAMEMODE_H

#include <Objects/GameComponent.h>
class Camera;
class InputContext;
class Level;
class GameMode : public TypedGameComponent<GameMode>
{
public:
  GameMode();
  virtual ~GameMode();
  //virtual void renderGameMode()=0;
  void run();
  bool update();
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "GameMode";
  }
protected:
  virtual bool v_Update()=0;
private:
  std::string virtualPrint();
};

#endif // GAMEMODE_H
