#include <Objects/Engine.h>
#include <Log/GameConsole.h>
#include <Filesystem/Filesystem.h>
#include <Log/Log.h>
class GameConsoleCommand
{
public:
  GameConsoleCommand(std::string _name);
  const std::string& getName(){return name;}
  virtual std::string execute(const std::string& _parameters)=0;
protected:
  friend class Engine;
  Engine* console;
private:
  std::string name;
};


Engine::~Engine()
{
}

void Engine::executeCommand(const std::string& _name)
{
  if (_name.length() >= 1)
    {
      std::string::size_type commandEnd = _name.find(" ", 0);
      std::string command = _name.substr(0, commandEnd);
      std::string commandArgs;
      if (commandEnd != -1)
        {
	  commandArgs = _name.substr(commandEnd + 1, _name.length() - (commandEnd + 1));
        }
      for(std::string::size_type i=0; i < command.length(); i++)
        {
	  command[i] = tolower(command[i]);
        }

      auto commandPtr = commands.find(command);
      if (commandPtr == commands.end())
        {
	  std::string outString = command + " is an invalid command.";
	  g_Log.error(outString);
        }
      else
        {
	  std::string str = commandPtr->second->execute(commandArgs);
	  output(&str);
        }
    }

}

void Engine::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<Engine, ConsoleInterface>();
}

class GameConsole;

GameConsoleCommand::GameConsoleCommand(std::string _name)
{
  console = nullptr;
  name = _name;
}


class SayCommand: public GameConsoleCommand
{
public:
  SayCommand()
    :GameConsoleCommand("say")
  {
  }
  std::string execute(const std::string& _parameters)
  {
    std::string outString = "You:" + _parameters; // Append our 'name' to the message we'll display in the list
    return outString;
  }
};
class PrintCommand: public GameConsoleCommand
{
public:
  PrintCommand()
    :GameConsoleCommand("print")
  {
  }
  std::string execute(const std::string& _parameters)
  {
    try
      {
	GameComponent* _node = console->getCurrentDirectory()->getNode(_parameters);
	std::string outString = _node->getType()->print(_node);
	return outString;
      }
    catch (int i)
      {
	return "Failed";
      }
  }
private:
};
/// For some reason std::string::{rfind and find} are not working correctly
unsigned int rfind(const std::string& _string, char _char)
{
  for (unsigned int pos = _string.size()-1; pos != 0; pos--)
    {
      if (_string[pos] == _char)
        {
	  return pos;
        }
    }
  return -1;
}
class ExecuteActionCommand: public GameConsoleCommand
{
public:
  ExecuteActionCommand()
    :GameConsoleCommand("run")
  {
  }
  std::string execute(const std::string& _parameters)
  {
    try
      {
	unsigned int index = rfind(_parameters, '.');
	GameComponent* _node = console->getCurrentDirectory()->getNode(_parameters.substr(0, index));
	ActionHandle* handle = _node->getType()->getActionHandle(_parameters.substr(index+1, -1));
	if (handle)
	  {
	    handle->execute(_node);
	  }
	else
	  {
	    g_Log.error("Object has no action called '" + _parameters.substr(index+1, -1) +'\'');
	  }
	//outputText(_parameters.substr(index+1, -1));
      }
    catch (int i)
      {
	return "Operation failed";
      }
  }
};
class ChangeDirectoryCommand: public GameConsoleCommand
{
public:
  ChangeDirectoryCommand()
    :GameConsoleCommand("cd")
  {
  }
  std::string execute(const std::string& _parameters)
  {
    try
      {
	GameComponent* _node = console->getCurrentDirectory()->getNode(_parameters);
	console->setCurrentDirectory(_node);
      }
    catch (int i)
      {
	return "Failed";
      }
  }
private:
};

Engine::Engine()
  :TypedGameComponent<Engine>("engineMobile"),
   ConsoleInterface(false)
{
  currentDirectory = Filesystem::global();
  addCommand(new SayCommand());
  addCommand(new PrintCommand());
  addCommand(new ExecuteActionCommand());
  addCommand(new ChangeDirectoryCommand());
}
void Engine::addCommand(GameConsoleCommand* _command)
{
  if (commands.find((_command->getName())) != commands.end())
    throw -1;
  _command->console = this;
  commands[_command->getName()] = _command;
}
