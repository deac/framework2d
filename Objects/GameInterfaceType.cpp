#include <Objects/GameInterfaceType.h>

void GameInterfaceType::selfRegister(GameInterface* (*_factory)(GameComponent* _implementation))
{
  factory = _factory;
}

GameInterface* GameInterfaceType::instantiate(GameComponent* _implementation)
{
  return factory(_implementation);
}

std::map<std::string, GameInterfaceType*> GameInterfaceType::interfaceTypes;
