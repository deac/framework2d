#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <Objects/GameComponent.h>
#include <string>
#include <map>
#include <cassert>
class PropertyBagWindow;
class CollisionObject;
class GameInterface;
class PropertyBagInterface;
class TypeTable;

class GameObject: public TypedGameComponent<GameObject> //public NamedTreeNode<GameObjectBase>
{
public:

  GameObject();
  virtual ~GameObject();

  /// Members for GameComponents attaching and detaching
  //void addEvent(const std::string& _name, EventHandle* _handle, GameComponent* _eventSource);
  //void addAction(const std::string& _name, ActionHandle* _handle, GameComponent* _actionTarget);
  void init(PropertyBagInterface* _params);
  void addInterface(const std::string& _name, GameInterface* _interface);
  void removeInterface(const std::string& _name);
  template <typename InterfaceType>
  InterfaceType* getInterface(const std::string& _name);
  //void removeEvent(const std::string& _name);
  //void removeAction(const std::string& _name);

  std::vector<std::string> interfacesContents();
  unsigned int interfacesSize(){return interfaces.size();}
  GameComponent* getInterface(const std::string& _name);
  template <typename Interface>
  GameComponent* getInterface();
  void killActionBy(CollisionObject* _object); /// Will trigger deathEvent
  static std::string name()
  {
    return "GameObject";
  }
  static void registerActions(GameComponentType* _type);
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
protected:
  void parentChanged(GameComponent* _oldParent){}
  void childAttached(GameComponent* _child);
  void childDetached(GameComponent* _child);
private:
  //std::map<std::string, std::pair<EventHandle*, GameComponent*>> events;
  //std::map<std::string, std::pair<ActionHandle*, GameComponent*>> actions;
  std::map<std::string, GameInterface*> interfaces;
};

class GameObjectEventListener
{
    public:
        virtual void fire()=0;
        virtual void eventObjectDeleted()=0;
};

template <typename InterfaceType>
InterfaceType* GameObject::getInterface(const std::string& _name)
{
  auto iter = interfaces.find(_name);
  if (iter == interfaces.end())
    return nullptr;
  GameInterface* interface = iter->second;
  assert(dynamic_cast<InterfaceType*>(iter->second));
  return static_cast<InterfaceType*>(iter->second);
}

//*/
/*template <typename DerivedObject>
void GameObject<DerivedObject>::print(std::string* _output)
{
    _output->append(getObjectName() + "\nChildren:\n");

    for (GameObjectBase* iter = getChildren(); iter; iter = iter->getNext())
    {
        _output->append(iter->getObjectName() + ",\n");
    }
    _output->append("Events:\n");
    for (auto iter = eventHandles().begin(); iter != eventHandles().end(); iter++)
    {
        _output->append(iter->first);
    }
}*/

template <typename InterfaceType>
GameComponent* GameObject::getInterface()
{
  return interfaces[InterfaceType::name()].second;
}
#endif // GAMEOBJECT_H

































