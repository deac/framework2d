#pragma once

#include <Objects/GameInterface.h>
class GameComponentType;

class UpdateFunctionInterface: public TypedGameInterface<UpdateFunctionInterface>
{
public:
  virtual void update()=0;
  static std::string interfaceName()
  {
    return "UpdateFunctionInterface";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type){}
};
