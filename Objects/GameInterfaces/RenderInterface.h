#pragma once

#include <Objects/GameInterface.h>
class GameComponentType;
class MaterialContext;
class TransformInterface;

class RenderInterface: public TypedGameInterface<RenderInterface>
{
public:
  RenderInterface();
  ~RenderInterface();
  void render();
  static std::string interfaceName()
  {
    return "RenderInterface";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type){}
protected:
  void setMaterial(MaterialContext* _material);
  virtual void virtualRender()=0;
private:
  friend class StaticSkinNew; /// FIXME
  friend class StaticSkin; /// FIXME
  TransformInterface* transform;
  MaterialContext* material;
};
