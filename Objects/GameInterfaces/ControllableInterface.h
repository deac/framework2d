#pragma once

#include <Objects/GameInterface.h>
#include <Objects/GameInterfaces/UserInputInterface.h>
class PropertyBagInterface;

class ControllableInterface: public TypedGameInterface<ControllableInterface>
{
  //$TypeParameter<UserInputInterface> inputProvider;
protected:
  struct ControllableInterfaceStaticData;
public:
  ControllableInterface();
  ~ControllableInterface();
  void instancePostInit(ControllableInterfaceStaticData* _data, PropertyBagInterface* _propertyBag); /// FIXME need to make sure this is called
  static std::string interfaceName()
  {
    return "ControllableInterface";
  }
  static void derivedRegister();
  virtual void actionFired(const std::string& _name)=0;
protected:
  /// Will create an EventHandle listening on inputProvider:_userEvent, and will fire getComponent():_ourEvent with the parameter
  void createEvent(const std::string& _userEvent, const std::string& _ourEvent);
  struct ControllableInterfaceStaticData
  {
    UserInputInterface* inputProvider;
  };
  /// <UserEvent, OurEvent>
  std::unordered_map<std::string, std::string> eventMap;
private:
  virtual ActionHandle* getActionHandle(const std::string& _name);
  /// UserInputInterface needs to use this function
  friend class UserInputInterface;
  void eventFired(UserInputInterface::UserInputEvent* _event);

  
  ControllableInterfaceStaticData* data;
};
