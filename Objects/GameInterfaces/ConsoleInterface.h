#pragma once

#include <Objects/GameInterface.h>
#include <Objects/GameComponentType.h>
#include <fstream>
class InstanceEventHandle;
class GameComponentType;

class ConsoleInterface: public TypedGameInterface<ConsoleInterface>
{
public:
  /* Instance of Error is thrown upon calling error().
     The error can be caught by passing the instance into catchError.
     When Error is deconstructed, the program closes unless the error was caught. 
   */
  class Error
  {
  public:
    Error(ConsoleInterface* _owner);
    ~Error();
    void catchError();
    void catchError(const std::string& _catchMessage);
    const std::string& getMessage(){return message;}
  private:
    bool wasCaught;
    std::string message;

    friend class ConsoleInterface;
    void caught();
    Error(const std::string& _message);
    ConsoleInterface* owner;
  };
  
  ConsoleInterface(bool _readOnly);
  ~ConsoleInterface();
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type);
  static void registerInterfaceActions(GameComponentType* _type){}
  static std::string interfaceName()
  {
    return "ConsoleInterface";
  }
  
  ActionHandle* getInputAction(){return inputAction;}
  ActionHandle* getOutputAction(){return outputAction;}
  ActionHandle* getErrorAction(){return errorAction;}
  InstanceEventHandle* getTextOutputHandle(){return &textOutputted;}
  InstanceEventHandle* getTextInputHandle(){return &textInputted;}
  InstanceEventHandle* getErrorHandle(){return &errorThrown;}

  void attachInputFile(FILE* _file);
  void attachOutputFile(FILE* _file);
  void attachBiStream(FILE* _input, FILE* _output);

  void input(std::string* _message); /// Will throw an error if isReadOnly()
  void output(std::string* _message);
  void error(std::string* _message);
  
  bool isReadOnly(){return readOnly;}
  
  /*$Event std::string input; /// Will throw an error if isReadOnly()
  $Event std::string output;
  $Event std::string error;*/

  virtual GameComponent* getComponent()=0; /// FIXME need to make this automatic somehow
protected:
  static ActionHandle* inputAction;
  static ActionHandle* outputAction;
  static ActionHandle* errorAction;
  InstanceEventHandle textOutputted;
  InstanceEventHandle textInputted;
  InstanceEventHandle errorThrown;
  static EventHandle* staticTextOutputted;
  static EventHandle* staticTextInputted;
  static EventHandle* staticErrorThrown;
  virtual void virtualOutputText(const std::string& _message){}
  virtual void executeCommand(const std::string& _command); // Default implementation is an error
  friend class Error;
  void catchError(const std::string& _catchMessage, Error* _error);
private:
  bool readOnly;
};

template <typename DerivedType>
void ConsoleInterface::templateRegisterInterfaceActions(GameComponentType* _type)
{
  staticTextOutputted = _type->createEventHandle("textOutput");
  staticTextInputted = _type->createEventHandle("textInput");
  inputAction = _type->createInterfaceActionHandle<ConsoleInterface, std::string>("input", &ConsoleInterface::input);
    outputAction = _type->createInterfaceActionHandle<ConsoleInterface, std::string>("output", &ConsoleInterface::output);
}
