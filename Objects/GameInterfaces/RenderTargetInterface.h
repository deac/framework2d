#pragma once

#include <Objects/GameInterface.h>
class Renderer;
class GameComponentType;

class RenderTargetInterface: public TypedGameInterface<RenderTargetInterface>
{
public:
  virtual void enable(Renderer* _renderer)=0;
  static std::string interfaceName()
  {
    return "RenderTarget";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type);
};
