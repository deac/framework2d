#pragma once 

#include <Objects/GameInterface.h>
#include <Types/Vec2f.h>
template <typename VariableType>
class VariableHandle;
class GameComponentType;

class TransformInterface : public TypedGameInterface<TransformInterface>
{
public:
  TransformInterface();
  ~TransformInterface();
  static std::string interfaceName()
  {
    return "Transform";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type){}
  Vec2f getPosition(){return get_position();}
  float getAngle(){return get_rotation();}
  float getRotation(){return get_rotation();}
protected:
private:
  virtual const Vec2f& get_position()=0;
  virtual const float& get_rotation()=0;
  static VariableHandle<Vec2f>* position;
  static VariableHandle<float>* rotation;
};
