#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Objects/GameComponent.h>
#include <Filesystem/Filesystem.h>
EventHandle* PropertyBagInterface::staticModifiedEvent = nullptr;

PropertyBagInterface::PropertyBagInterface()
  :modifiedEvent(staticModifiedEvent)
{
}
PropertyBagInterface::~PropertyBagInterface()
{
}

void PropertyBagInterface::derivedRegister()
{
}

void PropertyBagInterface::registerInterfaceActions(GameComponentType* _type)
{
  staticModifiedEvent = _type->createEventHandle("modified");
}

#include <AbstractFactory/InstanceParameters.h>
PropertyBagInterface* PropertyBagInterface::getFactoryProperties()
{
  GameComponent* root = getComponent();
  while (dynamic_cast<InstanceParameters*>(root)) /// FIXME
    root = root->getParentNode();
  GameComponent* factory = root->getNode("game")->getNode(getProductType());
  return factory->getInterface<PropertyBagInterface>();
}
#define DEBUG 
/// Debugging this parallel traversal by mapping the products to the interfaces
DEBUG std::map<GameComponent*, PropertyBagInterface*> assertionMap;
GameComponent* PropertyBagInterface::__build__(GameComponent* _parent)
{
  DEBUG  assertionMap.clear();
  setRoot(Filesystem::global());
  GameComponent* object = getFactory()->createObject(_parent,getProductName());
  setRoot(object);
  createObjects(object);
  initObjects(object);
  return object;
}
void PropertyBagInterface::createObjects(GameComponent* _object)
{
  DEBUG  assertionMap[_object] = this;
  for (PropertyBagInterface* child = getChildren(); child; child = child->getNext())
    {
      child->setRoot(getRoot());
      GameComponent* childObject = child->getFactory()->createObject(_object,child->getProductName());
      child->createObjects(childObject);
  }
}
void PropertyBagInterface::initObjects(GameComponent* _object)
{
  //DEBUG  assert(assertionMap[_object] == this);
  getFactory()->initObject(_object, this);
  
  //PropertyBagInterface* childParams = getChildren();
  //for (GameComponent* child = _object->getChildren(); child; child = child->getNext())

  GameComponent* child = _object->getChildren();
  for (PropertyBagInterface* childParams = getChildren(); childParams; childParams = childParams->getNext())
    {
      if (childParams->getProductName() != child->getName())
	assert(false); /// If this fails, it means the objects are giving themselves children before their init function, which is bad: need to forcefully line up params with their children by name, which could lead to collisions as well as being slower.
      childParams->initObjects(child);
      //childParams = childParams->getNext();
      child = child->getNext();
    }
  //assert(childParams == nullptr);
}
