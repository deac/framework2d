#include <Objects/GameInterfaces/RenderInterface.h>
#include <Objects/GameInterfaces/TransformInterface.h>
#include <Graphics/Contexts/MaterialContext.h>
#include <Objects/GameComponentType.h>
#include <GL/gl.h>
#include <cassert>

RenderInterface::RenderInterface()
{
  material = nullptr;
  transform = nullptr;
}
RenderInterface::~RenderInterface()
{
}

void RenderInterface::derivedRegister()
{
}
/*void RenderInterface::registerInterfaceActions(GameComponentType* _type)
{
  //RenderInterface* null = nullptr;
  //_type->requestVariable<TransformInterface>(&(null->transform));
  }*/
void RenderInterface::setMaterial(MaterialContext* _material)
{
  assert(material == nullptr);
  material = _material;
}

void RenderInterface::render()
{
  material->bind();
  const Vec2f & center = transform->getPosition();
  float rotation = -transform->getAngle();
  glPushMatrix();
  glTranslatef(center.x,center.y,0);
  if (rotation != 0.0f)
    glRotatef(rotation*(180.0/M_PI),0,0,-1);
  virtualRender();
  glPopMatrix();
}
