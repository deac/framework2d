#pragma once

#include <Objects/GameComponentType.h>
#include <Objects/GameInterfaces/FactoryInterface.h>
#include <Types/TypeTable.h>
class PropertyBagWindow;
class GameComponentType;

class PropertyBagInterface: public TypedGameInterface<PropertyBagInterface>
{
public:
  PropertyBagInterface();
  virtual ~PropertyBagInterface();
  GameComponent* __build__(GameComponent* _parent);

  static std::string interfaceName()
  {
    return "PropertyBagInterface";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type);
  virtual TypeTable* getTypeTable()=0;
  virtual PropertyBagWindow* createWindow()=0;
  virtual std::string getProductName()=0;
  virtual std::string getProductType()=0;
  virtual GameComponent* getComponent()=0;
  InstanceEventHandle* getModifiedEventHandle(){return &modifiedEvent;}
  PropertyBagInterface* getFactoryProperties();
protected:
private:
  InstanceEventHandle modifiedEvent;
  static EventHandle* staticModifiedEvent;
  void createObjects(GameComponent* _object);
  void initObjects(GameComponent* _object);
  
  virtual GameComponent* getRoot()=0;
  virtual void setRoot(GameComponent* _root)=0;
  virtual FactoryInterface* getFactory()=0;
  virtual PropertyBagInterface* getChildren()=0;
  virtual PropertyBagInterface* getNext()=0;
  virtual void setProductName(const std::string& _name)=0;
};
