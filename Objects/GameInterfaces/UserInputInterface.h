#pragma once

#include <Objects/GameInterface.h>
#include <vector>
#include <unordered_map>
class ActionHandle;
class ControllableInterface;
class GameComponentType;
class Controls;

class UserInputInterface: public TypedGameInterface<UserInputInterface>
{
public:
  class UserInputEvent
  {
  public:
    UserInputEvent(const std::string& _eventName);
    ~UserInputEvent();
    
    /// This isn't necessary yet
    /*enum Type
      {
	e_void,
	e_bool,
	e_int,
	e_intArray,
	e_float,
	e_floatArray,
	eTypeMax
      };*/
    
    const std::string& getEventName(){return eventName;}
    //$MappedEvent<void, std::string> listen; /// This is limited and could probably easily be made better. Why not just use ControllableInterface though
  private:
    /// ControllableInterface needs this function
    friend class ControllableInterface;
    //virtual void passPayload(ActionHandle* _action)=0; /// Takes an ActionHandle and executes it with the derived type as the parameter.
    
    std::string eventName;
  };
  UserInputInterface();
  ~UserInputInterface();
  //$MappedEvent<UserInputEvent, std::string> inputMap;
  virtual GameComponent* getComponent()=0;
  static std::string interfaceName()
  {
    return "UserInput";
  }
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type);

  Controls* getControls();
protected:
  
private:
  /// Controls needs this function
  friend class Controls;
  void fireEvent(UserInputEvent* _event);
  
  /// ControllableInterface needs these functions...
  friend class ControllableInterface;
  void addControllableInterface(ControllableInterface* _interface);
  void subscribeEvent(const std::string& _userEvent, ControllableInterface* _interface);
  /// ...up to here.
  /// 
  std::vector<ControllableInterface*> controllingList;
  
  /// <userEvent, interface>
  std::unordered_map<std::string, ControllableInterface*> eventListeners;

  Controls* controls;
};

/*  enum InternalEvent: uint32
      {
	/// Reserved event id's
	/// 0x0 to 0xFFFF (0 to 65536)
	/// Use the macro "IS_RESERVED(x)" to see if an id falls within this range
	/// FIXME possibly change this to a pcp function
	eButtonDownRange = 0,
	eButtonDownRangeEnd = 255,
	eButtonUpRange,
	eButtonUpRangeEnd = 511,
	eAxis0d,
	eAxis1d,
	eAxis2d,
	eAxis10d = eAxis0d+10,
	ePointer0d,
	eReservedStart,
	eReservedEnd = 0xFFFF,
	
	/// etc.
      };
*/
