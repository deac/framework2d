#pragma once

class EditorInterface: public TypedGameInterface<EditorInterface>
{
public:
  EditorInterface(GameComponent* _component);
  ~EditorInterface();
  void undo();
  void redo();
  void command(const std::string& _name);
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type){}
private:
};
