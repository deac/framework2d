#include <Objects/GameInterfaces/FactoryInterface.h>
#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>

FactoryInterface::FactoryInterface()
{
}
FactoryInterface::~FactoryInterface()
{
}

GameComponent* FactoryInterface::createObject(GameComponent* _parent, const std::string& _name)
{
  GameComponent* object = createObject();
  object->setName(_name);
  _parent->appendChild(object);
  return object;
}
void FactoryInterface::derivedRegister()
{
}


    
