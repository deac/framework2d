#include <Objects/GameInterfaces/ConsoleInterface.h>
#include <Filesystem/Filesystem.h>
#include <Log/Log.h>
#include <Objects/Engine.h>
#include <Log/FileStreamer.h>

ActionHandle* ConsoleInterface::inputAction = nullptr;
ActionHandle* ConsoleInterface::outputAction = nullptr;
ActionHandle* ConsoleInterface::errorAction = nullptr;
EventHandle* ConsoleInterface::staticTextInputted = nullptr;
EventHandle* ConsoleInterface::staticTextOutputted = nullptr;
EventHandle* ConsoleInterface::staticErrorThrown = nullptr;

ConsoleInterface::ConsoleInterface(bool _readOnly)
  :textOutputted(staticTextOutputted),
   textInputted(staticTextInputted),
   errorThrown(staticErrorThrown)
{
  readOnly = _readOnly;
  //$/dev/log/allErrors.errorListener.attach(error);
}
ConsoleInterface::~ConsoleInterface()
{
  //$/dev/log/allErrors.errorListener.detach(error);
}
void ConsoleInterface::derivedRegister()
{
}

void ConsoleInterface::attachBiStream(FILE* _input, FILE* _output)
{
  FileStreamer* file = new FileStreamer(_input, _output);
  getComponent()->attachChild(file);
  file->getTextOutputHandle()->registerListener(getComponent(), inputAction);
  getTextOutputHandle()->registerListener(file, file->getOutputAction());
}
/*void ConsoleInterface::attachInputFile(FILE* _file)
{
  FileStreamer* file = new FileStreamer(_file, true);
  getComponent()->attachChild(file);
  file->getTextOutputHandle()->registerListener(getComponent(), inputAction);
}
void ConsoleInterface::attachOutputFile(FILE* _file)
{
  FileStreamer* file = new FileStreamer(_file, false);
  getComponent()->attachChild(file);
  getTextOutputHandle()->registerListener(file, file->getInputAction());
  }*/

void ConsoleInterface::executeCommand(const std::string& _command)
{
  g_Log.error("Console is read only");
}

void ConsoleInterface::input(std::string* _message)
{
  executeCommand(*_message);
  textInputted.fire<std::string>(getComponent(), _message);
}
void ConsoleInterface::output(std::string* _message)
{
  virtualOutputText(*_message);
  textOutputted.fire<std::string>(getComponent(), _message);
}

void ConsoleInterface::error(std::string* _message)
{
  errorThrown.fire<std::string>(getComponent(), _message);
  throw Error(*_message);
}
/*void ConsoleInterface::uncaughtError(const std::string& _message)
{
  std::string message = _message;
  getNode("errors")->getInterface<ConsoleInterface>()->output(&message);
  }*/
void ConsoleInterface::catchError(const std::string& _message, Error* _error)
{
  std::string message = "Caught: " + _message;
  getComponent()->getNode("errors")->getInterface<ConsoleInterface>()->output(&message);
}
void ConsoleInterface::Error::catchError(const std::string& _message)
{
  //owner->getComponent()->getNode("errors");
  //owner->catchError(_message, this);
  caught();
}
void ConsoleInterface::Error::catchError()
{
  caught();
}
ConsoleInterface::Error::Error(const std::string& _message)
  :message(_message)
{
  wasCaught = false;
}
ConsoleInterface::Error::Error(ConsoleInterface* _owner)
{
  owner = _owner;
  assert(owner);
  assert(reinterpret_cast<unsigned int>(owner) > 0x100);
  owner->getComponent()->getNode("errors");
}
ConsoleInterface::Error::~Error()
{
  if (!wasCaught)
    exit(-1);
}

void ConsoleInterface::Error::caught()
{
  assert(!wasCaught);
  wasCaught = true;
}
