#pragma once

#include <Objects/GameInterface.h>
#include <Objects/GameComponent.h>
#include <cassert>
class PropertyBagWindow;
class PropertyBagInterface;
class TypeTable;

class FactoryInterface: public TypedGameInterface<FactoryInterface>
{
public:
  FactoryInterface();
  ~FactoryInterface();
  //GameComponent* build(PropertyBagInterface* _params, GameComponent* _parent);
  //virtual std::string generateName()=0;
  static void derivedRegister();
  template <typename DerivedType>
  static void templateRegisterInterfaceActions(GameComponentType* _type){}
  static void registerInterfaceActions(GameComponentType* _type){}
  static std::string interfaceName()
  {
    return "FactoryInterface";
  }
  virtual void renderParameters(PropertyBagWindow* _parameters, TypeTable* _table)=0;
  virtual GameComponentType* getProductType()=0;
protected:
private:
  friend class PropertyBagInterface;
  GameComponent* createObject(GameComponent* _parent, const std::string& _name);
  virtual GameComponent* createObject()=0;
  virtual void initObject(GameComponent* _object, PropertyBagInterface* _params)=0;
};

template <typename Product>
class TemplateFactoryInterfaceBase: public FactoryInterface
{
public:
  GameComponent* createObject()
  {
    return new Product;
  }
  GameComponentType* getProductType()
  {
    return TypedGameComponent<Product>::staticGetType();
  }
};

template <typename Product, typename DerivedType>
class TemplateFactoryInterface: public TemplateFactoryInterfaceBase<Product>
{
public:
  void initObject(GameComponent* _object, PropertyBagInterface* _params)
  {
    assert(dynamic_cast<Product*>(_object));
    static_cast<DerivedType*>(this)->initObject(static_cast<Product*>(_object), _params);
  }
};
