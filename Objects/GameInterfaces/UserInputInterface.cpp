#include <Objects/GameInterfaces/UserInputInterface.h>
#include <Objects/GameInterfaces/ControllableInterface.h>
#include <Objects/Components/Controls.h>
#include <cassert>

UserInputInterface::UserInputInterface()
{
}

UserInputInterface::~UserInputInterface()
{
}

UserInputInterface::UserInputEvent::UserInputEvent(const std::string& _eventName)
{
  eventName = _eventName;
}
UserInputInterface::UserInputEvent::~UserInputEvent()
{
}

void UserInputInterface::derivedRegister()
{
}
void UserInputInterface::registerInterfaceActions(GameComponentType* _type)
{
}

void UserInputInterface::addControllableInterface(ControllableInterface* _interface)
{
  controllingList.push_back(_interface);
}

void UserInputInterface::subscribeEvent(const std::string& _userEvent, ControllableInterface* _interface)
{
  assert(eventListeners.find(_userEvent) == eventListeners.end());
  eventListeners[_userEvent] = _interface;
}

void UserInputInterface::fireEvent(UserInputEvent* _event)
{
  auto listener = eventListeners[_event->getEventName()];
  if (listener)
    listener->eventFired(_event);
}

Controls* UserInputInterface::getControls()
{
  GameComponent* child = getComponent()->getNode("controls");
  assert(child);
  assert(dynamic_cast<Controls*>(child));
  return static_cast<Controls*>(child);
}
