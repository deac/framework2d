#include <Objects/GameInterfaces/ControllableInterface.h>
#include <cassert>

ControllableInterface::ControllableInterface()
{
}
ControllableInterface::~ControllableInterface()
{
}
void ControllableInterface::derivedRegister()
{
}
ActionHandle* ControllableInterface::getActionHandle(const std::string& _name)
{
  return nullptr;
}
void ControllableInterface::createEvent(const std::string& _userEvent, const std::string& _ourEvent)
{
  assert(eventMap.find(_userEvent) == eventMap.end());
  eventMap[_userEvent] = _ourEvent;
  data->inputProvider->subscribeEvent(_userEvent, this);
}
/*void ControllableInterface::eventFired(UserInputInterface::UserInputEvent* _event)
{
  
  _event->passPayload(getActionHandle(eventMap[eventMap_event->getEventName()]));
  }*/

void ControllableInterface::instancePostInit(ControllableInterfaceStaticData* _data, PropertyBagInterface* _propertyBag)
{
  data = _data;
  data->inputProvider->addControllableInterface(this);
  for (auto iter = eventMap.begin(); iter != eventMap.end(); iter++)
    {
      data->inputProvider->subscribeEvent(iter->first, this);
    }
}
void ControllableInterface::eventFired(UserInputInterface::UserInputEvent* _event)
{
  actionFired(eventMap[_event->getEventName()]);
}
