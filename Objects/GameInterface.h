#pragma once

#include <Objects/GameInterfaceType.h>
#include <map>
#include <string>
class GameComponent;

class GameInterface
{
public:
  GameInterface(){}
protected:
};


class InstantiateInterface
{
public:
  virtual void check()const=0;
};
class InstantiationList
{
public:
  void run();
  void add(InstantiateInterface* _interface);
  static InstantiationList* global();
};
template <typename DerivedType>
class AutoInstantiateInterface : public InstantiateInterface
{
public:
  AutoInstantiateInterface(GameInterfaceType* _type);
  void check()const;
private:
};

template <typename DerivedType>
class TypedGameInterface: public GameInterface
{
public:
  TypedGameInterface()
  {
    registrar.check();
  }
  static GameInterfaceType* getInterfaceType(){return &type;}
  static DerivedType* factory(GameComponent* _component){return DerivedType::createInstance(_component);}
  unsigned int getInterfaceHandle(){return interfaceHandle();}
  static unsigned int interfaceHandle()
  {
    static unsigned int handle = newInterfaceHandle();
    return handle;
  }
private:
  static unsigned int newInterfaceHandle()
  {
    static unsigned int handle = 0;
    return handle++;
  }
  static GameInterfaceType type;
  friend class AutoInstantiateInterface<DerivedType>;
  const static AutoInstantiateInterface<DerivedType> registrar;
  static void baseRegister();
};

template <typename DerivedType>
GameInterfaceType TypedGameInterface<DerivedType>::type(DerivedType::interfaceName());

template <typename DerivedType>
const AutoInstantiateInterface<DerivedType> TypedGameInterface<DerivedType>::registrar(&TypedGameInterface<DerivedType>::type);

template <typename DerivedType>
AutoInstantiateInterface<DerivedType>::AutoInstantiateInterface(GameInterfaceType* _type)
{
  //type = _type;
  InstantiationList::global()->add(this);
}

template <typename DerivedType>
void AutoInstantiateInterface<DerivedType>::check() const
{
  TypedGameInterface<DerivedType>::baseRegister();
}

template <typename DerivedType>
void TypedGameInterface<DerivedType>::baseRegister()
{
  //interfaces[DerivedType::name()] = &type;
  //type.selfRegister(&TypedGameInterface<DerivedType>::untypedCreateInstance);
  DerivedType::derivedRegister();
}
