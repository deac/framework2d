#ifndef SINGLETON_H
#define SINGLETON_H

#include <Objects/GameComponent.h>

class SingletonInterface
{
public:
  virtual void singletonInit()=0;
};

template <typename Type>
class Singleton: public TypedGameComponent<Singleton<Type>>, public SingletonInterface
{
 public:
  Singleton(const std::string& _name, bool _attachToSlashDev = true);
  ~Singleton();
  static std::string name()
  {
    return Type::name();
  }
  static void registerActions(GameComponentType* _type);
  static Type* singleton();
private:
  static Type** singletonPrivate();
};

template <typename Type>
Singleton<Type>::~Singleton()
{
  *singletonPrivate() = nullptr;
}

template <typename Type>
void Singleton<Type>::registerActions(GameComponentType* _type)
{
  singleton();
  Type::singletonRegisterActions(_type);
}

template <typename Type>
Type* Singleton<Type>::singleton()
{
  return *singletonPrivate();
}
template <typename Type>
Type** Singleton<Type>::singletonPrivate()
{
  static Type* singleton = new Type;
  return &singleton;
}

#endif
#ifndef CIRCULER_BARRIER
#ifndef CIRCULER_CODE
#define CIRCULER_CODE

#include <Filesystem/Filesystem.h>

template <typename Type>
Singleton<Type>::Singleton(const std::string& _name, bool _attachToSlashDev)
  :TypedGameComponent<Singleton<Type>>(_name, nullptr, false)
{
  if (_attachToSlashDev)
    Filesystem::singleton()->getNode("dev")->attachChild(this);
}
 
#endif
 


#endif


