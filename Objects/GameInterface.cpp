#include <Objects/GameInterface.h>
#include <vector>

InstantiationList* InstantiationList::global()
{
  static InstantiationList list;
  return &list;
}

std::vector<InstantiateInterface*>* getInterfaces()
{
  static std::vector<InstantiateInterface*>* interfaces = new std::vector<InstantiateInterface*>;
  return interfaces;
}
void InstantiationList::run()
{
  for (auto iter = getInterfaces()->begin(); iter != getInterfaces()->end(); iter++)
    {
      (*iter)->check();
    }
  delete getInterfaces();
}

void InstantiationList::add(InstantiateInterface* _interface)
{
  getInterfaces()->push_back(_interface);
}


