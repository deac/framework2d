#include <Objects/GameComponentType.h>
#include <Objects/GameObject.h>
#include <Objects/GameInterface.h>
#include <Objects/GameComponent.h>
#include <AbstractFactory/PropertyBagWindow.h>
#include <Log/Log.h>
#include <cassert>

void errorLog(const std::string& _name)
{
  g_Log.error(_name);
}
std::unordered_map<std::string,EventHandle*>& GameComponentType::g_EventHandles()
{
  static std::unordered_map<std::string,EventHandle*> handles;
  return handles;
}
std::unordered_map<std::string,ActionHandle*>& GameComponentType::g_ActionHandles()
{
  static std::unordered_map<std::string,ActionHandle*> handles;
  return handles;
}
std::unordered_map<std::string,VariableHandleBase*>& GameComponentType::g_VariableHandles()
{
  static std::unordered_map<std::string,VariableHandleBase*> handles;
  return handles;
}

GameComponentType::GameComponentType(const std::string& _name)
 :name(_name)
{
  //ctor
}
#include <set>
std::set<GameComponentType*> g_deletedTypes;
GameComponentType::~GameComponentType()
{
  //dtor
  g_deletedTypes.insert(this);
}

EventHandle* GameComponentType::createEventHandle(const std::string& _name)
{
  assert(eventHandles.find(_name) == eventHandles.end());
  EventHandle* handle = new EventHandle(eventHandles.size());
  eventHandles[_name] = handle;
  if (g_EventHandles().find(_name) == g_EventHandles().end())
    {
      g_EventHandles()[_name] = handle;
    }
  return handle;
}


unsigned int GameComponentType::eventsSize()
{
  return eventHandles.size();
}

ActionHandle* GameComponentType::getActionHandle(const std::string& _name)
{
  assert(g_deletedTypes.find(this) == g_deletedTypes.end());
  return actionHandles[_name];
}

EventHandle* GameComponentType::staticGetEventHandle(const std::string& _name)
{
  return g_EventHandles()[_name];
}
ActionHandle* GameComponentType::staticGetActionHandle(const std::string& _name)
{
  return g_ActionHandles()[_name];
}

std::string GameComponentType::print(GameComponent* _object)
{
  std::string ret;
  ret.append(getName() + ": '" + _object->getName() + "'\n");
  {
    unsigned short numberOfChildren = 0;
    std::string children;
    for (GameComponent* child = _object->getChildrenNode(); child; child = child->getNextNode())
      {

	children.append(child->getType()->getName() + ": '" + child->getName() + "',\n");
	numberOfChildren++;
      }
    std::string buffer;
    buffer.resize(42);
    if (numberOfChildren == 1)
      sprintf(&buffer[0], "%d child:\n", numberOfChildren);
    else
      sprintf(&buffer[0], "%d children:\n", numberOfChildren);
    ret.append(buffer + children);
  }
  {
    unsigned short numberOfEvents = 0;
    std::string events;
    for (auto iter = eventHandles.begin(); iter != eventHandles.end(); iter++)
      {
	events.append(iter->first + ",\n");
	numberOfEvents++;
      }
    std::string buffer;
    buffer.resize(40);
    switch (numberOfEvents)
      {
      case 0:
	sprintf(&buffer[0], "No events\n");
	break;
      case 1:
	sprintf(&buffer[0], "1 event:\n");
	break;
      default:
	sprintf(&buffer[0], "%d events:\n", numberOfEvents);
	break;
      };
    ret.append(buffer + events);
  }
  {
    unsigned short numberOfActions = 0;
    std::string action;
    for (auto iter = actionHandles.begin(); iter != actionHandles.end(); iter++)
      {
	action.append(iter->first + ",\n");
	numberOfActions++;
      }
    std::string buffer;
    buffer.resize(40);
    switch (numberOfActions)
      {
      case 0:
	sprintf(&buffer[0], "No actions\n");
	break;
      case 1:
	sprintf(&buffer[0], "1 action:\n");
	break;
      default:
	sprintf(&buffer[0], "%d actions:\n", numberOfActions);
	break;
      };
    ret.append(buffer + action);
  }
  return ret;
}

void GameComponentType::renderEvents(PropertyBagWindow* _window)
{
  for (auto iter = eventHandles.begin(); iter != eventHandles.end(); iter++)
    {
      _window->setEvent(iter->first);
    }
}

void GameComponentType::createVariableHandle(const std::string& _name, VariableHandleBase* _handle)
{
  assert(variableHandles.find(_name) == variableHandles.end());
  variableHandles[_name] = _handle;
}

/*void GameComponentType::exposeInterface(const std::string& _name)
{
  interfaceHandles[_name] = GameInterfaceType::getInterfaceType(_name);
  }*/

void GameComponentType::attachToParent(GameComponent* _this, GameObject* _parent)
{
  /*for (auto iter = eventHandles.begin(); iter != eventHandles.end(); iter++)
    {
    _parent->addEvent(iter->first, iter->second, _this);
    }
    for (auto iter = actionHandles.begin(); iter != actionHandles.end(); iter++)
    {
    _parent->addAction(iter->first, iter->second, _this);
    }*/
  for (auto iter = interfaceOffsets.begin(); iter != interfaceOffsets.end(); iter++)
    {
      POINTER_INT base = reinterpret_cast<POINTER_INT>(_this);
      POINTER_INT offset = reinterpret_cast<POINTER_INT>(iter->second);
      GameInterface* interface = reinterpret_cast<GameInterface*>(base + offset);
      _parent->addInterface(iter->first->getName(), interface);
    }
  for (auto iter = variableOffsets.begin(); iter != variableOffsets.end(); iter++)
    {
      GameInterface* interface = _parent->getInterface<GameInterface>(iter->first);
      if (interface)
	{
	  GameInterface** variable = reinterpret_cast<GameInterface**>(reinterpret_cast<POINTER_INT>(_this) + iter->second);
	  *variable = interface;
	}
      else
	{
	  g_Log.error(getName() + " requires " + iter->first + " variable from parent");
	}
    }
  /*for (auto iter = interfaceHandles.begin(); iter != interfaceHandles.end(); iter++)
    {
      _parent->addInterface(iter->first, iter->second->instantiate(_this));
      }*/
}
void GameComponentType::detachFromParent(GameComponent* _this, GameObject* _parent)
{
  /*for (auto iter = eventHandles.begin(); iter != eventHandles.end(); iter++)
    {
    _parent->removeEvent(iter->first);
    }
    for (auto iter = actionHandles.begin(); iter != actionHandles.end(); iter++)
    {
    _parent->removeAction(iter->first);
    }*/
  for (auto iter = interfaceOffsets.begin(); iter != interfaceOffsets.end(); iter++)
    {
      _parent->removeInterface(iter->first->getName());
    }
  /*for (auto iter = interfaceHandles.begin(); iter != interfaceHandles.end(); iter++)
    {
      _parent->removeInterface(iter->first);
      }*/
}

InstanceEventHandle::InstanceEventHandle(EventHandle* _event)
{
  event = _event;
}
void InstanceEventHandle::registerListener(GameComponent* _object, ActionHandle* _listener)
{
  assert(_object);
  assert(_listener);
  listeners.push_back({_object, _listener});
}
