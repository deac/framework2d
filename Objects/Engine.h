#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>
class GameConsoleCommand;
class GameConsole;

class Engine: public TypedGameComponent<Engine>, public ConsoleInterface
{
public:
  Engine();
  ~Engine();
  void executeCommand(const std::string& _command);
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "Engine";
  }
  GameComponent* getCurrentDirectory(){return currentDirectory;}
  void setCurrentDirectory(GameComponent* _directory){currentDirectory = _directory;}
  GameComponent* getComponent(){return this;}
private:
  void addCommand(GameConsoleCommand* _command);
  GameComponent* currentDirectory;
  std::unordered_map<std::string, GameConsoleCommand*> commands;
};

