#include "GameObject.h"
#include <Filesystem/Filesystem.h>
#include <Filesystem/OrphanList.h>
#include <Log/Log.h>
#include <AbstractFactory/DefaultFactories.h>

GameObject::GameObject()
{
}
/*void GameObjectBase::orphaned() FIXME remove old code
{
    Filesystem::global()->getOrphanList()->attachOrphan(this);
}*/
GameObject::~GameObject()
{
}


#include <Physics/BodyParts/BodyPart.h>

void GameObject::init(PropertyBagInterface* _params)
{
}

void GameObject::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
}
/*void GameObject::addAction(const std::string& _name, ActionHandle* _handle, GameComponent* _actionTarget)
{
  assert(actions.find(_name) == actions.end());
  actions[_name] = {_handle, _actionTarget};
  }*/
void GameObject::killActionBy(CollisionObject* _object)
{
    delete this;
}

void GameObject::registerActions(GameComponentType* _type)
{
  DefaultFactories::singleton()->registerSimpleFactory<GameObject>();
}

/*void GameObject::addEvent(const std::string& _name, EventHandle* _handle, GameComponent* _eventSource)
{
  assert(events.find(_name) == events.end());
  events[_name] = {_handle, _eventSource};
}
void GameObject::addAction(const std::string& _name, ActionHandle* _handle, GameComponent* _actionTarget)
{
  assert(actions.find(_name) == actions.end());
  actions[_name] = {_handle, _actionTarget};
  }*/
void GameObject::addInterface(const std::string& _name, GameInterface* _handle)
{
  assert(interfaces.find(_name) == interfaces.end());
  interfaces[_name] = _handle;
}

/*void GameObject::removeEvent(const std::string& _name)
{
  assert(events.find(_name) != events.end());
  events.erase(_name);
}
void GameObject::removeAction(const std::string& _name)
{
  assert(actions.find(_name) != actions.end());
  actions.erase(_name);
  }*/
void GameObject::removeInterface(const std::string& _name)
{
  assert(interfaces.find(_name) != interfaces.end());
  interfaces.erase(_name);
}

void GameObject::childAttached(GameComponent* _child)
{
  _child->getType()->attachToParent(_child, this);
}
void GameObject::childDetached(GameComponent* _child)
{
  _child->getType()->detachFromParent(_child, this);
}
std::vector<std::string> GameObject::interfacesContents()
{
  std::vector<std::string> names;
  names.reserve(interfaces.size());
  for (auto iter = interfaces.begin(); iter != interfaces.end(); iter++)
    {
      names.push_back(iter->first);
    }
  return names;
}


























