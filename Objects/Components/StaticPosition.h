#pragma once

#include <Objects/GameComponent.h>
class TypeTable;
class PropertyBagInterface;
class PropertyBagWindow;

#include <Objects/GameInterfaces/TransformInterface.h>
class StaticPosition: public TypedGameComponent<StaticPosition>, public TransformInterface
{
  struct StaticPositionStaticData
  {
    static std::string name()
    {
      return "StaticPosition";
    }
  };
public:
  StaticPosition();
  ~StaticPosition();
  void init(PropertyBagInterface* _properties);
  const Vec2f& get_position(){return position;}
  const float& get_rotation(){return rotation;}
  static void registerActions(GameComponentType* _type);
  static std::string name(){return "StaticPosition";};
  static void staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table);
private:
  Vec2f position;
  float rotation;
};
