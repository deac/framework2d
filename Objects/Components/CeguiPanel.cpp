#include <Objects/Components/CeguiPanel.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Objects/Components/Controls.h>
#include <CEGUI/CEGUI.h>

void CeguiPanel::enable(Renderer* _renderer)
{
}
using namespace CEGUI;

CeguiPanel::CeguiPanel()
{
  window = nullptr;
}
CeguiPanel::~CeguiPanel()
{
}
void CeguiPanel::typeInit(CeguiPanelStaticData* _data, PropertyBagInterface* _propertyBag)
{
}

void CeguiPanel::instanceInit(CeguiPanelStaticData* _data, PropertyBagInterface* _propertyBag)
{
  attachChild(new Controls(this));
  /*std::string windowName = _propertyBag->getTypeTable()->getValue<std::string>("windowName", "Root");
  if (windowName == "Root")
    window = CEGUI::System::getSingleton().getGUISheet();
  else
    window = CEGUI::System::getSingleton().getGUISheet()->getChildRecursive(windowName);
  /// FIXME subscribe to all the CEGUI events on window that I care about.
  window->subscribeEvent(Window::EventMouseMove, Event::Subscriber(&CeguiPanel::mouseMoveHandle, this));
  window->subscribeEvent(Window::EventKeyUp, Event::Subscriber(&CeguiPanel::buttonPressHandle, this));
  
  //$this.inputMap["menu"].subscribe($this.menuOpen)
  keyEventMap[Key::Escape] = "menu";
  actionMap["menu"] = {menuOpen, this};
  std::cout << "Created" << std::endl;*/
}

void CeguiPanel::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<CeguiPanel, UserInputInterface>();
  _type->exposeInterface<CeguiPanel, RenderTargetInterface>();
  DefaultFactories::singleton()->addFactory<CeguiPanel, CeguiPanelStaticData>();
}

bool CeguiPanel::mouseMoveHandle(const EventArgs& _event)
{
  std::cout << "Moved" << std::endl;
  return true;
}
bool CeguiPanel::buttonPressHandle(const EventArgs& _event)
{
  /*std::cout << "Derp" << std::endl;
  const KeyEventArgs& castEvent = static_cast<const KeyEventArgs&>(_event);
  auto event = keyEventMap.find(castEvent.scancode);
  if (event != keyEventMap.end())
    {
      auto action = actionMap.find(event->second);
      if (action != actionMap.end())
	{
	  action->second.first->execute(action->second.second);
	}
    }
    return true;*/
}

void CeguiPanel::CeguiPanelStaticData::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
}
void CeguiPanel::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
}

/*bool CeguiPanel::mouseMoveHandle(const EventArgs& _event)
{
  UserInputEvent* event = controls->mouseMove({event.x,event.y});
  if (event)
    fireEvent(event);
}

void CeguiPanel::enable(Renderer* _renderer)
{
  _renderer->transform(window->getAbsoluteCoords());
  }*/
