#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/ControllableInterface.h>
#include <unordered_map>
class TypeTable;

class PlayerController: public TypedGameComponent<PlayerController>, public ControllableInterface
{
  struct StaticData
  {
    static std::string name()
    {
      return "PlayerController";
    }
    static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
    std::vector<std::string> events;
    std::vector<std::string> actions;
  };
public:
  void instanceInit(StaticData* _data, PropertyBagInterface* _propertyBag);
  static void registerActions(GameComponentType* _type);
  static void typeInit(StaticData* _data, PropertyBagInterface* _loader);
  static std::string name()
  {
    return "PlayerController";
  }
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
private:
  void actionFired(const std::string& _name);
};
