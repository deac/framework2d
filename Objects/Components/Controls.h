#pragma once

#include <Objects/GameComponent.h>
class UserInputInterface;

class Controls: public TypedGameComponent<Controls>
{
public:
  Controls(UserInputInterface* _input);
  ~Controls();
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "Controls";
  }
  /// These are fired by the hardware interface
  void keyUp(unsigned int _key);
  void keyDown(unsigned int _key);
  void mouseMove();
private:
  UserInputInterface* input;
  std::unordered_map<unsigned int, std::string> keyUpMap;
  std::unordered_map<unsigned int, std::string> keyDownMap;
  std::string mouseMoveEvent;
};
