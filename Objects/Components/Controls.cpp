#include <Objects/Components/Controls.h>
#include <Objects/GameInterfaces/UserInputInterface.h>
#include <CEGUI/CEGUI.h>

Controls::Controls(UserInputInterface* _input)
  :TypedGameComponent("controls")
{
  input = _input;
  keyDownMap[CEGUI::Key::W] = "up";
  keyUpMap[CEGUI::Key::W] = "upStop";
}

Controls::~Controls()
{
}

void Controls::registerActions(GameComponentType* _type)
{
}

void Controls::keyUp(unsigned int _key)
{
  auto iter = keyUpMap.find(_key);
  if (iter != keyUpMap.end())
    {
      UserInputInterface::UserInputEvent event(iter->second);
      input->fireEvent(&event);
    }
}
void Controls::keyDown(unsigned int _key)
{
  auto iter = keyDownMap.find(_key);
  if (iter != keyDownMap.end())
    {
      UserInputInterface::UserInputEvent event(iter->second);
      input->fireEvent(&event);
    }
}
