#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/RenderTargetInterface.h>
#include <Objects/GameInterfaces/UserInputInterface.h>
#include <unordered_map>
#include <CEGUI/CEGUI.h>
class PropertyBagInterface;
class Controls;
class TypeTable;

class CeguiPanel: public TypedGameComponent<CeguiPanel>, public RenderTargetInterface, public UserInputInterface
{
  struct CeguiPanelStaticData
  {
    static std::string name()
    {
      return "CeguiPanel";
    }
    static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
  };
public:
  CeguiPanel();
  ~CeguiPanel();
  GameComponent* getComponent(){return this;}
  //$InstanceParameter<CEGUI::Window*> window;
  //$InstanceParameter<Controls*> controls;
  //$Event menuOpen;
  void instanceInit(CeguiPanelStaticData* _data, PropertyBagInterface* _propertyBag);
  void enable(Renderer* _renderer);
  static void registerActions(GameComponentType* _type);
  static void typeInit(CeguiPanelStaticData* _data, PropertyBagInterface* _loader);
  static std::string name()
  {
    return "CeguiPanel";
  }
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
  void fireEvent(UserInputEvent* _event);
private:
  ActionHandle* menuOpen;
  CEGUI::Window* window;
  Controls* controls;
  std::unordered_map<unsigned int, std::string> keyEventMap;
  std::unordered_map<std::string, std::pair<ActionHandle*, GameComponent*>> actionMap;
  bool buttonPressHandle(const CEGUI::EventArgs& _event);
  bool mouseMoveHandle(const CEGUI::EventArgs& _event);
};
