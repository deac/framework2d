#include <Objects/Components/PlayerController.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Filesystem/Filesystem.h>
#include <UI/UIManager.h>

void PlayerController::registerActions(GameComponentType* _type)
{
  DefaultFactories::singleton()->addFactory<PlayerController, StaticData>();
}


void PlayerController::StaticData::staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table)
{
}

void PlayerController::typeInit(StaticData* _data, PropertyBagInterface* _loader)
{
  TypeTable* table = _loader->getTypeTable();
  _data->events = table->getArray<std::string>("events", {"up", "upStop"});
  _data->actions = table->getArray<std::string>("actions", {"moveUp", "moveUpStop"});
  assert(_data->events.size() == _data->actions.size());
}
void PlayerController::instanceInit(StaticData* _data, PropertyBagInterface* _propertyBag)
{
  for (unsigned int i = 0; i != _data->events.size(); i++)
    {
      //actionMap[_data->events[i]] = {getParentNode()->getType()->getActionHandle(_data->actions[i]), getParentNode()};
      eventMap[_data->events[i]] = _data->actions[i];
    }
  ControllableInterfaceStaticData data;
  data.inputProvider = static_cast<UIManager*>(Filesystem::singleton()->getNode("dev/graphics/ui"))->getActiveInterface(); /// FIXME replace with _propertyBag->getValue<UserInputInterface>("inputProvider", "/dev/graphics/ui/activeInterface")
  instancePostInit(&data, _propertyBag);
}
void PlayerController::staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table)
{
}
void PlayerController::actionFired(const std::string& _name)
{
  if (_name == "moveUp")
    g_Log.message("Player moving up");
  if (_name == "moveUpStop")
    g_Log.message("Player stop moving up");
}
