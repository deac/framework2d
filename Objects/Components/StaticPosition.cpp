#include <Objects/Components/StaticPosition.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <AbstractFactory/PropertyBagWindow.h>

StaticPosition::StaticPosition()
{
}
StaticPosition::~StaticPosition()
{
}
void StaticPosition::init(PropertyBagInterface* _loader)
{
	TypeTable* table = _loader->getTypeTable();
	this->position = table->getValue<Vec2f>("position", {0,0});
	this->rotation = table->getValue<float>("rotation", {0});
}
#include <AbstractFactory/DefaultFactories.h>
void StaticPosition::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<StaticPosition, TransformInterface>();
  DefaultFactories::singleton()->registerSimpleFactory<StaticPosition>();
}
std::string compose(float _in);
void StaticPosition::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
  _window->setProperty("Vec2f", "position", compose(Vec2f({0,0})));
  _window->setProperty("float", "rotation", compose(float(0)));
}
