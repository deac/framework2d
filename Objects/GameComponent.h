#pragma once

#include <Types/Tree.h>
#include <unordered_map>
#include <vector>
class GameObject;
class GameComponentType;
class EventHandle;
class ActionHandle;
class PropertyBagWindow;

class GameComponent: public NamedTreeNode<GameComponent>
{
public:
  GameComponent(GameComponentType* _type, const std::string& _name);
  GameComponent(GameComponentType* _type, const std::string& _name, GameComponent* _parent, bool _orphan);
  virtual ~GameComponent();

  GameComponent* getNode(const std::string& _address);
  virtual GameComponent* getIndividualNode(const std::string& _address);
  template <typename Type>
  Type* getChildByType();
  template <typename Type>
  Type* getInterface();

  GameComponentType* getType();
  GameObject* getRoot();
  std::string print();
  void killAction(); /// Will trigger deathEvent
  void fireEvent(EventHandle* _eventHandle);
  void executeCommand(const std::string& _command);

  std::string check(); /// Tests for corruption

  void renderEvents(PropertyBagWindow* _window);
protected:
  void childAttached(GameComponent* _child);
  void childDetached(GameComponent* _child);
  void parentChanged(GameComponent* _oldParent);
  static std::unordered_map<std::string,ActionHandle*>& actionHandles()
  {
    static std::unordered_map<std::string,ActionHandle*> handles;
    return handles;
  }
  void orphaned();
  static void registerBaseActions(GameComponentType* _type);
private:
  //virtual std::string virtualPrint()=0;
  friend class OrphanList;

  static EventHandle* deathEvent;
  static EventHandle* childAttachEvent;
  static EventHandle* childDetachEvent;
  GameComponentType* type;
};


template <typename DerivedObject>
class AutoInstantiate
{
    public:
        AutoInstantiate(GameComponentType* _type);
        void check()const{}
};

template <typename Type>
class TypedGameComponent: public GameComponent
{
public:
  TypedGameComponent(const std::string& _name = "noName");
  TypedGameComponent(const std::string& _name, GameComponent* _parent, bool _orphan = false);
  ~TypedGameComponent();
  static GameComponentType* staticGetType(){return &type;}
private:
  static GameComponentType type;
  friend class AutoInstantiate<Type>;
  static void baseRegisterActions(GameComponentType* _type);
  const static AutoInstantiate<Type> autoInstantiationPlease;
};

#include <Objects/GameComponentType.h>


void errorLog(const std::string& _name);
template <typename Type>
Type* GameComponent::getChildByType()
{
    for (GameComponent* child = getChildrenNode(); child; child = child->getNextNode())
    {
        if (dynamic_cast<Type*>(child))
        {
            return static_cast<Type*>(child);
            break;
        }
    }
    errorLog("No child of type " + Type::name() + " attached to " + getName());
}

template <typename DerivedObject>
AutoInstantiate<DerivedObject>::AutoInstantiate(GameComponentType* _type)
{
    GameComponent::registerBaseActions(_type);
    TypedGameComponent<DerivedObject>::baseRegisterActions(_type);
}

template <typename Type>
TypedGameComponent<Type>::TypedGameComponent(const std::string& _name)
  :GameComponent(&type, _name)
{
    autoInstantiationPlease.check();
}
template <typename Type>
TypedGameComponent<Type>::TypedGameComponent(const std::string& _name, GameComponent* _parent, bool _orphan)
  :GameComponent(&type, _name, _parent, _orphan)
{
    autoInstantiationPlease.check();
}

template <typename Type>
TypedGameComponent<Type>::~TypedGameComponent()
{
  fireEvent(deathEvent);
}

template <typename DerivedObject>
void TypedGameComponent<DerivedObject>::baseRegisterActions(GameComponentType* _type)
{
    DerivedObject::registerActions(_type);
}

template <typename T>
const std::string& generateName()
{
    static unsigned int count = 0;
    static std::string ret = T::name();
    ret += count;
    return ret;
}

template <typename Type>
Type* GameComponent::getInterface()
{
  return type->getInterface<Type>(this);
}
template <typename Type>
GameComponentType TypedGameComponent<Type>::type(Type::name());

template <typename DerivedObject>
const AutoInstantiate<DerivedObject> TypedGameComponent<DerivedObject>::autoInstantiationPlease(&type);
