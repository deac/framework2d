#pragma once

#include <string>
#include <map>
class GameComponent;
class GameInterface;
typedef unsigned short GameInterfaceEnum;

class GameInterfaceType
{
public:
  GameInterfaceType(const std::string& _name){name = _name;}
  static GameInterfaceType* getInterfaceType(const std::string& _name){return interfaceTypes[_name];}
  void selfRegister(GameInterface* (*_factory)(GameComponent* _implementation));
  GameInterface* instantiate(GameComponent* _implementation);
  const std::string& getName(){return name;}
private:
  static std::map<std::string, GameInterfaceType*> interfaceTypes;
  GameInterface* (*factory)(GameComponent* _implementation);
  std::string name;
};

