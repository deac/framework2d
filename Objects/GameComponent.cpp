#include <Objects/GameComponent.h>
#include <Filesystem/Filesystem.h>
#include <Objects/GameObject.h>
#include <Filesystem/OrphanList.h>
#include <AbstractFactory/PropertyBagWindow.h>
#include <Types/Address.h>

GameComponent::GameComponent(GameComponentType* _type, const std::string& _name)
  :NamedTreeNode<GameComponent>(_name)
{
  type = _type;
  Filesystem::global()->getOrphanList()->attachChild(this);
  children = nullptr;
}


GameComponent::GameComponent(GameComponentType* _type, const std::string& _name, GameComponent* _parent, bool _orphan)
  :NamedTreeNode<GameComponent>(_name)
{
  type = _type;
  if (_parent)
    {
      //_parent->check();
      _parent->attachChild(this);
    }
  else if (_orphan)
    {
      Filesystem::global()->getOrphanList()->attachOrphan(this);
    }
  else
    {
      try
	{
	  //Filesystem::global()->getOrphanList()->attachOrphan(this);
	  //assert("Object flagged as not an orphan, but orphan list exists" == nullptr);
	}
      catch (...)
	{
	}
    }
}


GameComponent::~GameComponent()
{
}

#include <iostream>

void GameComponent::registerBaseActions(GameComponentType* _type)
{
    static bool done = false;
    //if (!done)
    {
        done = true;
        _type->createActionHandle("kill", &GameComponent::killAction);
        _type->createActionHandle("printObject", &GameComponent::killAction);
        //_type->createActionHandle("KillBy", &GameComponent::killActionBy);
        deathEvent = _type->createEventHandle("onDeath");
        childAttachEvent = _type->createEventHandle("onChildAttach");
        childDetachEvent = _type->createEventHandle("onChildDetach");
    }
}
void GameComponent::renderEvents(PropertyBagWindow* _window)
{
  _window->setEvent("kill"); 
}
void GameComponent::killAction()
{
    delete this;
}

void GameComponent::fireEvent(EventHandle* _eventHandle){_eventHandle->fire(this);}

GameComponent* GameComponent::getNode(const std::string& _address)
{
  Address address(_address.c_str());
  std::string node = address.getFirst();
  GameComponent* currentNode = this;
  if (node == "/")
    currentNode = Filesystem::global();
  else if (node == ".")
    currentNode = this;
  else
    {
      currentNode = getIndividualNode(node);
    }
  if (address.popFirst())
    return currentNode->getNode(address.cpp_str());
  else
    return currentNode;
  /*GameComponent* node = this;
  bool first = true;
  unsigned int iter = 0;
    if (_address[0] == '/')
    {
        node = Filesystem::global();
        iter = 1;
	first = false;
    }
    else if (_address[0] == '.')
    {
        iter = 1;
	first = false;
    }
    while (true)
    {
        if (_address.size() > iter)
        {
            int next = _address.find('/', iter);
            std::string address = _address.substr(iter, next - iter);
            if (address == "..")
            {
	      node = node->getParentNode();
	      first = false;
            }
            else
            {
	      if (first)
		if (address == objectName)
		  {
		    first = false;
		    iter = next+1;
		    continue;
		  }
	      
	      node = node->getIndividualNode(address);
	      if (!node)
		throw -1;
	      if (next == -1)
		break;
	      else
		iter = next+1;
	    }
        } else break;
    }
    return node;*/
}
GameComponent* GameComponent::getIndividualNode(const std::string& _address)
{
  //check();
  return getChildByName(_address);
}
GameComponentType* GameComponent::getType()
{
  return type;
}

GameObject* GameComponent::getRoot()
{
  GameObject* rootThis = dynamic_cast<GameObject*>(this);
  if (rootThis)
    return rootThis;
  else
    if (parent)
      return getParentNode()->getRoot();
    else
      return nullptr;
}
void GameComponent::parentChanged(GameComponent* _oldParent)
{
  assert(parent != _oldParent);
  GameObject* root = getRoot();
  GameObject* oldRoot = _oldParent->getRoot();
  assert(root != oldRoot);
  if (root)
    {
      type->attachToParent(this, root);
    }
  if (oldRoot)
    {
      type->detachFromParent(this, oldRoot);
    }
  /*      {
	auto handles = actionHandles();
	for (auto iter = handles.begin(); iter != handles.end(); iter++)
	  {
	    root->addAction(iter->first, iter->second, this);
	  }
      }
      {
	auto handles = eventHandles();
      }
    }
    assert("This function needs to be written, move events and actions around" == nullptr);*/
}
void GameComponent::childAttached(GameComponent* _component)
{
  childAttachEvent->fire(this);
}
void GameComponent::childDetached(GameComponent* _component)
{
  childDetachEvent->fire(this);
}
void GameComponent::executeCommand(const std::string& _command)
{
  type->getActionHandle(_command)->execute(this);
}
EventHandle* GameComponent::deathEvent;
EventHandle* GameComponent::childAttachEvent;
EventHandle* GameComponent::childDetachEvent;
