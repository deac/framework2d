#pragma once

#include <Types/PointerInt.h>
#include <string>
#include <unordered_map>
#include <vector>
#include <cassert>
class GameComponent;
class GameObject;
class GameInterface;
class GameInterfaceType;
class GameObjectEventListener;
class ActionHandle;
class PropertyBagWindow;

class EventHandle
{
public:
  EventHandle(unsigned int _eventIndex){eventIndex = _eventIndex;}
private:
  virtual void fire(GameComponent* _object){}
  friend class GameComponent;
  unsigned int eventIndex;
};
class InstanceEventHandle
{
public:
  InstanceEventHandle(EventHandle* _event);
  template <typename EventObject>
  void fire(GameComponent* _object, EventObject* _eventObject);
  void registerListener(GameComponent* _object, GameObjectEventListener* _listener);
  void registerListener(GameComponent* _object, ActionHandle* _listener);
private:
  std::vector<std::pair<GameComponent*, ActionHandle*>> listeners;
  EventHandle* event;
};
class ActionHandle
{
public:
  virtual void execute(GameComponent* _object)=0;
  template <typename EventObject>
  void execute(GameComponent* _object, EventObject* _eventObject);
  virtual void execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle)=0;
protected:
  template <typename EventObject>
  static unsigned int getObjectHandle()
  {
    static unsigned int handle = newObjectHandle();
    return handle;
  }
private:
  static unsigned int newObjectHandle()
  {
    static unsigned int handle = 0;
    return handle++;
  }
};
template <typename DerivedObject>
class TemplateActionHandle : public ActionHandle
{
public:
  TemplateActionHandle(void (DerivedObject::*_action)()){action = _action;}
  void execute(GameComponent* _object);
  void execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle){execute(_object);}
private:
  void (DerivedObject::*action)();
};
template <typename DerivedObject, typename EventObject>
class TemplateActionWithObjectHandle : public ActionHandle
{
public:
  TemplateActionWithObjectHandle(void (DerivedObject::*_action)(EventObject* _eventObject)){action = _action;}
  void execute(GameComponent* _object);
  void execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle);
private:
  void (DerivedObject::*action)(EventObject* _eventObject);
};
template <typename DerivedObject, typename EventObject>
class TemplateInterfaceActionWithObjectHandle : public ActionHandle
{
public:
  TemplateInterfaceActionWithObjectHandle(void (DerivedObject::*_action)(EventObject* _eventObject)){action = _action;}
  void execute(GameComponent* _object);
  void execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle);
private:
  void (DerivedObject::*action)(EventObject* _eventObject);
};

class VariableHandleBase
{
public:
  virtual ~VariableHandleBase(){}
  template <typename TestType>
  bool isType();
};
template <typename VariableType>
class VariableHandle: public VariableHandleBase
{
public:
  virtual VariableType getValue(GameComponent* _component){throw -1;}
  virtual void setValue(GameComponent* _component, VariableType& _value){throw -1;}
};

template <typename VariableType, typename Object>
class ReadMemberFunction: public VariableHandle<VariableType>
{
public:
  ReadMemberFunction(VariableType (Object::*_function)());
  VariableType getValue(Object* _component);
private:
  VariableType (Object::*function)();
};

class GameComponentType
{
public:
  GameComponentType(const std::string& _name);
  virtual ~GameComponentType();
  const std::string& getName(){return name;}
  EventHandle* createEventHandle(const std::string& _name);
  ActionHandle* createActionHandle(const std::string& _name);
  unsigned int eventsSize();
  std::string print(GameComponent* _object);
  EventHandle* getEventHandle(const std::string& _name);
  ActionHandle* getActionHandle(const std::string& _name);
  template <typename VariableType>
  VariableHandle<VariableType>* getVariableHandle(const std::string& _name);

  template <typename InterfaceType>
  InterfaceType* getInterface(GameComponent* _component);
  GameInterface* getInterface(GameComponent* _component, const std::string& _name);
  template <typename DerivedType, typename InterfaceType>
  void exposeInterface();

  template <typename InterfaceType>
  InterfaceType** getVariable(GameComponent* _component);
  template <typename InterfaceType>
  InterfaceType** getVariable(GameComponent* _component, const std::string& _name);
  template <typename InterfaceType>
  void requestVariable(InterfaceType** _offset);
  
  template <typename Type>
  Type* getInterface();
  static EventHandle* staticGetEventHandle(const std::string& _name);
  static ActionHandle* staticGetActionHandle(const std::string& _name);
  template <typename InterfaceType>
  static VariableHandle<InterfaceType>* staticGetVariableHandle(const std::string& _name);

  template <typename DerivedObject>
  ActionHandle* createActionHandle(const std::string& _name, void (DerivedObject::*_action)());
  template <typename DerivedObject, typename EventObject>
  ActionHandle* createActionHandle(const std::string& _name, void (DerivedObject::*_action)(EventObject* _object));
  template <typename InterfaceType, typename EventObject>
  ActionHandle* createInterfaceActionHandle(const std::string& _name, void (InterfaceType::*_action)(EventObject* _object));
  void createVariableHandle(const std::string& _name, VariableHandleBase* _handle);
  
  void attachToParent(GameComponent* _child, GameObject* _parent);
  void detachFromParent(GameComponent* _child, GameObject* _parent);
  void renderEvents(PropertyBagWindow* _window);
protected:
private:
  std::string name;
  std::unordered_map<std::string,EventHandle*> eventHandles;
  std::unordered_map<std::string,ActionHandle*> actionHandles;
  std::unordered_map<std::string,VariableHandleBase*> variableHandles;
  std::unordered_map<std::string,GameInterfaceType*> interfaceHandles;
  
  /// FIXME should make these vectors
  std::unordered_map<GameInterfaceType*, POINTER_INT> interfaceOffsets;
  std::unordered_map<std::string, POINTER_INT> variableOffsets; /// Interfaces that are needed by this object
  
  static std::unordered_map<std::string,EventHandle*>& g_EventHandles();
  static std::unordered_map<std::string,ActionHandle*>& g_ActionHandles();
  static std::unordered_map<std::string,VariableHandleBase*>& g_VariableHandles();
};

#include <Objects/GameInterface.h>
#include <Objects/GameComponent.h>

template <typename EventObject>
void InstanceEventHandle::fire(GameComponent* _object, EventObject* _eventObject)
{
  for (unsigned int i = 0; i != listeners.size(); i++)
    {
      listeners[i].second->execute<EventObject>(listeners[i].first, _eventObject);
    }
}
template <typename DerivedObject>
ActionHandle* GameComponentType::createActionHandle(const std::string& _name, void (DerivedObject::*_action)())
{
  assert(actionHandles.find(_name) == actionHandles.end());
  ActionHandle* handle = new TemplateActionHandle<DerivedObject>(_action);
  actionHandles[_name] = handle;
  if (g_ActionHandles().find(_name) == g_ActionHandles().end())
    {
      g_ActionHandles()[_name] = handle;
    }
  return handle;
}
template <typename DerivedObject, typename EventObject>
ActionHandle* GameComponentType::createActionHandle(const std::string& _name, void (DerivedObject::*_action)(EventObject* _object))
{
  assert(actionHandles.find(_name) == actionHandles.end());
  ActionHandle* handle = new TemplateActionWithObjectHandle<DerivedObject, EventObject>(_action);
  actionHandles[_name] = handle;
  if (g_ActionHandles().find(_name) == g_ActionHandles().end())
    {
      g_ActionHandles()[_name] = handle;
    }
  return handle;
}
template <typename InterfaceType, typename EventObject>
ActionHandle* GameComponentType::createInterfaceActionHandle(const std::string& _name, void (InterfaceType::*_action)(EventObject* _object))
{
  assert(actionHandles.find(_name) == actionHandles.end());
  ActionHandle* handle = new TemplateInterfaceActionWithObjectHandle<InterfaceType, EventObject>(_action);
  actionHandles[_name] = handle;
  if (g_ActionHandles().find(_name) == g_ActionHandles().end())
    {
      g_ActionHandles()[_name] = handle;
    }
  return handle;
}


template <typename TestType>
bool VariableHandleBase::isType()
{
  return dynamic_cast<VariableHandle<TestType>*>(this);
}

template <typename InterfaceType>
VariableHandle<InterfaceType>* GameComponentType::getVariableHandle(const std::string& _name)
{
  VariableHandleBase* handleBase = variableHandles[_name];
  assert(dynamic_cast<VariableHandle<InterfaceType>*>(handleBase));
  VariableHandle<InterfaceType>* handle = static_cast<VariableHandle<InterfaceType>*>(handleBase);
  return handle;
}

template <typename InterfaceType, typename Object>
ReadMemberFunction<InterfaceType, Object>::ReadMemberFunction(InterfaceType (Object::*_function)())
{
  function = _function;
}
template <typename InterfaceType, typename Object>
InterfaceType ReadMemberFunction<InterfaceType, Object>::getValue(Object* _component)
{
  return (_component->*function)();
}

template <typename Type>
Type* GameComponentType::getInterface()
{
  /// FIXME this is shit
  for (auto iter = interfaceHandles.begin(); iter != interfaceHandles.end(); iter++)
    {
      GameInterface* handle = iter->second;
      Type* castHandle = dynamic_cast<Type*>(handle);
      if (castHandle)
	return castHandle;
    }
  return nullptr;
}
template <typename InterfaceType>
VariableHandle<InterfaceType>* GameComponentType::staticGetVariableHandle(const std::string& _name)
{
  VariableHandleBase* handle = g_VariableHandles()[_name];
  assert(handle);
  assert(dynamic_cast<VariableHandle<InterfaceType>*>(handle));
  return static_cast<VariableHandle<InterfaceType>*>(handle);
}

template <typename EventObject>
void ActionHandle::execute(GameComponent* _object, EventObject* _eventObject)
{
  execute(_object, static_cast<void*>(_eventObject), getObjectHandle<EventObject>());
}

template <typename DerivedObject>
void TemplateActionHandle<DerivedObject>::execute(GameComponent* _object)
{
  {
    DerivedObject* dynCast = dynamic_cast<DerivedObject*>(_object);
    assert(dynCast);
  }
  DerivedObject* object = static_cast<DerivedObject*>(_object);
  (object->*action)();
}
template <typename DerivedObject, typename EventObject>
void TemplateActionWithObjectHandle<DerivedObject, EventObject>::execute(GameComponent* _object)
{
  assert(false);
}
template <typename DerivedObject, typename EventObject>
void TemplateActionWithObjectHandle<DerivedObject, EventObject>::execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle)
{
  {
    DerivedObject* dynCast = dynamic_cast<DerivedObject*>(_object);
    assert(dynCast);
    assert(_typeHandle == getObjectHandle<EventObject>());
  }
  DerivedObject* object = static_cast<DerivedObject*>(_object);
  EventObject* eventObject = static_cast<EventObject*>(_eventObject);
  assert(dynamic_cast<EventObject*>(eventObject));
  (object->*action)(eventObject);
}
template <typename InterfaceType, typename EventObject>
void TemplateInterfaceActionWithObjectHandle<InterfaceType, EventObject>::execute(GameComponent* _object)
{
  assert(false);
}
template <typename InterfaceType, typename EventObject>
void TemplateInterfaceActionWithObjectHandle<InterfaceType, EventObject>::execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle)
{
  InterfaceType* object = _object->getInterface<InterfaceType>();
  EventObject* eventObject = static_cast<EventObject*>(_eventObject);
  assert(dynamic_cast<EventObject*>(eventObject));
  (object->*action)(eventObject);
}

/*template <typename DerivedObject, typename EventObject>
  void TemplateActionWithObjectHandle<DerivedObject, EventObject>::execute(GameComponent* _object)
  {
  }
  template <typename DerivedObject, typename EventObject>
  void TemplateActionWithObjectHandle<DerivedObject, EventObject>::execute(GameComponent* _object, void* _eventObject, unsigned int _typeHandle)
  {
  }*/

template <typename InterfaceType>
InterfaceType* GameComponentType::getInterface(GameComponent* _object)
{
  GameInterfaceType* handle = InterfaceType::getInterfaceType();
  auto iter = interfaceOffsets.find(handle);
  if (iter == interfaceOffsets.end())
    throw -1; /// FIXME report a proper error
  //else
  assert(sizeof(POINTER_INT) == sizeof(void*));
  POINTER_INT offset = reinterpret_cast<POINTER_INT>(iter->second);
  POINTER_INT object = reinterpret_cast<POINTER_INT>(_object);
  POINTER_INT final = object + offset;
  InterfaceType* ret = reinterpret_cast<InterfaceType*>(final);
  assert(dynamic_cast<TypedGameInterface<InterfaceType>*>(ret));
  return ret;
}

template <typename DerivedType, typename InterfaceType>
void GameComponentType::exposeInterface()
{
  GameInterfaceType* handle = InterfaceType::getInterfaceType();
  assert(interfaceOffsets.find(handle) == interfaceOffsets.end());
  char* charNull = reinterpret_cast<char*>(0x100);
  DerivedType* null = reinterpret_cast<DerivedType*>(charNull);
  assert(reinterpret_cast<POINTER_INT>(static_cast<GameComponent*>(null)) == 0x100);
  InterfaceType* offset = null;
  POINTER_INT intOffset = reinterpret_cast<POINTER_INT>(offset);
  assert(intOffset > sizeof(void*) + 0x100 && intOffset < 0x200); /// Sanity check, upper bound could be increased
  assert(reinterpret_cast<POINTER_INT>(static_cast<DerivedType*>(offset)) == 0x100);
  intOffset -= 0x100;
  interfaceOffsets[handle] = reinterpret_cast<POINTER_INT>(intOffset);

  assert(DerivedType::staticGetType() == this);
  InterfaceType::template templateRegisterInterfaceActions<DerivedType>(this);
  InterfaceType::registerInterfaceActions(this);
}

template <typename InterfaceType>
InterfaceType** GameComponentType::getVariable(GameComponent* _component)
{
  return getVariable(_component, InterfaceType::interfaceName());
}

template <typename InterfaceType>
InterfaceType** GameComponentType::getVariable(GameComponent* _component, const std::string& _name)
{
  POINTER_INT object = reinterpret_cast<POINTER_INT>(_component);
  POINTER_INT offset = variableOffsets[_name];
  return reinterpret_cast<InterfaceType**>(object + offset);
}

template <typename InterfaceType>
void GameComponentType::requestVariable(InterfaceType** _offset)
{
  assert(variableOffsets.find(InterfaceType::interfaceName()) == variableOffsets.end());
  variableOffsets[InterfaceType::interfaceName()] = reinterpret_cast<POINTER_INT>(_offset);
}
