#ifndef FOLDER_H
#define FOLDER_H

#include <Objects/GameComponent.h>
class TypeTable;
class PropertyBagWindow;
class PropertyBagInterface;

class Folder : public TypedGameComponent<Folder>
{
public:
  Folder();
  Folder(const std::string& _name, GameComponent* _parent, bool _orphan = true);
  virtual ~Folder();
  void init(PropertyBagInterface* _params);
  static std::string name()
  {
    return "Folder";
  }
  static void registerActions(GameComponentType* _type);
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
protected:
private:
  std::string virtualPrint();
};

#endif // FOLDER_H
