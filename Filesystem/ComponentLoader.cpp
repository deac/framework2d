#include <Filesystem/ComponentLoader.h>
#include <Log/Log.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Filesystem/Shortcut.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>

ComponentLoader::ComponentLoader()
{
}
ComponentLoader::~ComponentLoader()
{
}

void ComponentLoader::init(PropertyBagInterface* _params)
{
}

GameComponent* ComponentLoader::getIndividualNode(const std::string& _address)
{
  try
    {
      GameComponent* ret = GameComponent::getIndividualNode(_address);
      assert(ret);
      assert(dynamic_cast<Shortcut*>(ret));
      return static_cast<Shortcut*>(ret)->dereference(); 
    }
  catch (ConsoleInterface::Error& _error)
    {
      _error.catchError();
      GameComponent* ret = loadComponent(getName() + "/lib" + _address + ".so", _address + "_dynamicLoad");
      ret->setName(_address);
      if (ret != nullptr)
	{
	  attachChild(new Shortcut(ret, _address));
	  return ret;
	}
      else g_Log.error("No such component on file: " + getName() + '/' + _address);
    }
}
#include <dlfcn.h>
#include <iostream>
GameComponent* ComponentLoader::loadComponent(const std::string& _filename, const std::string& _loadFunction)
{
  std::string filename = _filename;
  system(("make " + filename).c_str());
  dlerror();

  void *handle = dlopen(filename.c_str(), RTLD_NOW | RTLD_GLOBAL);
  if (handle == nullptr)
    {
      std::string message = std::string("Failed to load component: ") + dlerror();
      g_Log.error(message);
    }
  else dlerror();

  void* dynamicLoad = dlsym(handle, _loadFunction.c_str());
  if (dynamicLoad == nullptr)
    {
      std::string message = std::string("Failed to find dynamicLoad function: ") + dlerror();
      std::cout << message << std::endl;
      assert(false);
      g_Log.error(message);
    }
  else dlerror();

  typedef GameComponent*(*LoadFunction)();
  GameComponent* component = reinterpret_cast<LoadFunction>(dynamicLoad)();
  assert(component);
  //assert("Succeeded" == nullptr);
  return component;
}

void ComponentLoader::registerActions(GameComponentType* _type)
{
  DefaultFactories::singleton()->registerSimpleFactory<ComponentLoader>();
}

void ComponentLoader::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
}
