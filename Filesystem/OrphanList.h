#ifndef ORPHANLIST_H
#define ORPHANLIST_H

#include <Objects/Singleton.h>

class OrphanList : public Singleton<OrphanList>
{
public:
  OrphanList();
  virtual ~OrphanList();
  void singletonInit();
  void attachOrphan(GameComponent* _node);
  static void singletonRegisterActions(GameComponentType* _type);
  static std::string name()
  {
    return "OrphanList";
  }
  EventHandle* getEventHandle(const std::string& _name){throw -1;}
  void print(std::string* _output);
protected:
private:
  std::string virtualPrint();
};

#endif // ORPHANLIST_H
