#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#define CIRCULER_BARRIER
#include <Objects/Singleton.h>
#undef CIRCULER_BARRIER
class OrphanList;
class Folder;

class Filesystem: public Singleton<Filesystem>
{
public:
  void shutdown();
  OrphanList* getOrphanList(); /// "/dev/orphans"
  void singletonInit();
  static Filesystem* global(); /// "/"
  static void singletonRegisterActions(GameComponentType* _type);
  static std::string name()
  {
    return "Filesystem";
  }
  Folder* makeFolders(const std::string& _name); /// Can specify multiple folders with '/' symbol
  Filesystem();
protected:
private:
  std::string virtualPrint();
  virtual ~Filesystem();
  OrphanList* orphanList;
};

#include <Objects/Singleton.h>

#endif // FILESYSTEM_H
