#include "Folder.h"
#include <AbstractFactory/DefaultFactories.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <AbstractFactory/PropertyBagWindow.h>

Folder::Folder()
{
}
Folder::Folder(const std::string& _name, GameComponent* _parent, bool _orphan)
  :TypedGameComponent<Folder>(_name, _parent)
{
  //ctor
}

Folder::~Folder()
{
  //dtor
}

void Folder::init(PropertyBagInterface* _properties)
{
  //TypeTable* table = _properties->getTypeTable();
  //setName(table->getValue<std::string>("name", getName()));
}

void Folder::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
  _window->setProperty("derp","ferp","herp");
  _window->setProperty("derp","ferp","merp");
}
#include <iostream>
void Folder::registerActions(GameComponentType* _type)
{
  DefaultFactories::global()->registerSimpleFactory<Folder>();
}
