#pragma once

#include <Objects/GameComponent.h>
class TypeTable;
class PropertyBagInterface;

class ComponentLoader: public TypedGameComponent<ComponentLoader>
{
public:
  ComponentLoader();
  ~ComponentLoader();
  void init(PropertyBagInterface* _params);
  GameComponent* getIndividualNode(const std::string& _address);
  static std::string name()
  {
    return "ComponentLoader";
  }
  static void registerActions(GameComponentType* _type);
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
private:
  GameComponent* loadComponent(const std::string& _filename, const std::string& _loadFunction);
};
