#pragma once

#include <Objects/GameComponent.h>

class Shortcut: public TypedGameComponent<Shortcut>
{
public:
  Shortcut(GameComponent* _target, const std::string& _name);
  ~Shortcut();
  GameComponent* dereference(){return target;}
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "Shortcut";
  }
private:
  GameComponent* target;
};
