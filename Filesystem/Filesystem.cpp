#include "Filesystem.h"
#include <Filesystem/OrphanList.h>
#include <Filesystem/Folder.h>
#include <Filesystem/ComponentLoader.h>
#include <AbstractFactory/DefaultFactories.h>
#include <iostream>

Filesystem::Filesystem()
 :Singleton("/", false)
{
    //ctor
  assert(getChildren() == nullptr);
  GameComponent* dev = new Folder("dev", this);
}

Filesystem::~Filesystem()
{
    //dtor
}

void Filesystem::singletonInit()
{
  //dev->attachChild(DefaultFactories::global());
  orphanList = new OrphanList;
  attachChild(orphanList);
}
OrphanList* Filesystem::getOrphanList()
{
  static OrphanList* orphanList = new OrphanList;
  getNode("dev")->attachChild(orphanList);
  return orphanList;
}
Filesystem* Filesystem::global()
{
  return singleton();
}
void Filesystem::singletonRegisterActions(GameComponentType* _type)
{
}

void Filesystem::shutdown()
{
    while (getChildren())
    {
        delete getChildren();
    }
}

Folder* Filesystem::makeFolders(const std::string& _address)
{
    GameComponent* node = this; /// FIXME this code is duplicated
    unsigned int iter = 0;
    if (_address[0] == '/')
    {
        node = Filesystem::global();
        iter = 1;
    }
    else if (_address[0] == '.')
    {
        iter = 1;
    }
    int next;
    try
    {
        while (true)
        {
            if (_address.size() > iter)
            {
                next = _address.find('/', iter);
                std::string address = _address.substr(iter, next - iter);
                if (address == "..")
                {
                    node = node->getParentNode();
                }
                else
                {
                    if (next == -1)
                    {
                        node = node->getIndividualNode(address);
                        break;
                    }
                    else
                    {
                        node = node->getIndividualNode(address);
                        iter = next+1;
                    }
                }
            } else break;
        }
    }
    catch (...)
    {
        while (true)
        {
            if (_address.size() > iter)
            {
                next = _address.find('/', iter);
                std::string address = _address.substr(iter, next - iter);
                if (next == -1)
                {
		  Folder* folder = new Folder(address, node);
                    node = folder;
                    break;
                }
                else
                {
		  Folder* folder = new Folder(address, node);
                    node = folder;
                    iter = next+1;
                }
            } else break;
        }
    }
    assert(dynamic_cast<Folder*>(node));
    return static_cast<Folder*>(node);;
}
