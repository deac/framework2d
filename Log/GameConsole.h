#ifndef GAMECONSOLE_H
#define GAMECONSOLE_H

#include <CEGUI/CEGUI.h>
#include <unordered_map>
#include <Objects/GameComponent.h>
#include <Log/Logger.h>
class GameComponent;
class ConsoleInterface;
class ConsoleBox;

class GameConsole : public TypedGameComponent<GameConsole>, public Logger
{
public:
  GameConsole(GameComponent* _consoleInterface);
  ~GameConsole();
  Logger* newLogger(Logger* _newLogger);
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "GameConsole";
  }
  void newConsole(ConsoleInterface* _console, const std::string& _name);
private:
  friend class GameConsoleCommand;
  void createCEGUIWindow();                                  // The function which will load in the CEGUI Window and register event handlers
  bool handleTextSubmitted(const CEGUI::EventArgs &e);      // Handle when we press Enter after typing
  bool handleTabSelectionChanged(const CEGUI::EventArgs &e);      // Handle when we press Enter after typing
  bool handleSendButtonPressed(const CEGUI::EventArgs &e);  // Handle when we press the Send button
  bool handleKeyUp(const CEGUI::EventArgs &e);  // Handle when we press the Send button
  void parseText(CEGUI::String inMsg);                       // Parse the text the user submitted.

  CEGUI::Window *consoleWindow;                            // This will be a pointer to the ConsoleRoot window.
  ConsoleBox* activeConsole;
private:
  void enterText();
};
#endif // GAMECONSOLE_H
