#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>

class FileStreamer: public TypedGameComponent<FileStreamer>, public ConsoleInterface
{
public:
  FileStreamer(FILE* _input, FILE* _output);
  ~FileStreamer();
  static std::string name()
  {
    return "FileStreamer";
  }
  static void registerActions(GameComponentType* _type);
  GameComponent* getComponent(){return this;}
  void readLoop();
  static ActionHandle* getMainHandle(){return mainHandle;}
private:
  void executeCommand(const std::string& _name);
  static ActionHandle* mainHandle;
  FILE* inputFile,* outputFile;
};
