#pragma once

#include <Objects/GameComponent.h>
class ConsoleInterface;
class FactoryInterface;
class PropertyBagInterface;
class TypeTable;
namespace CEGUI
{
  class Window;
}

class ConsoleBox: public TypedGameComponent<ConsoleBox>
{
public:
  ConsoleBox();
  void init(PropertyBagInterface* _params);
  ~ConsoleBox();
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "ConsoleBox";
  }
  static void staticRenderParameters(PropertyBagWindow* _parameters, TypeTable* _table);
  ConsoleInterface* getConsoleInterface(){return console;}
  
  /// Used by ./GameConsole.cpp
  static ConsoleBox* privateFactory(PropertyBagInterface* _params, GameComponent* _parent);
  bool isReadOnly();
  void input(const std::string& _message);
private:
  static ActionHandle* inputPrint;
  static ActionHandle* outputPrint;
  static ActionHandle* errorPrint;
  ConsoleInterface* console;
  CEGUI::Window* window;
  void outputText(std::string _msg, bool _leftAligned = true, unsigned int timeStamp = 0, unsigned int _colour = -1);
  void inputPrintAction(std::string* _message);
  void outputPrintAction(std::string* _message);
  void errorThrowAction(std::string* _message);
};
