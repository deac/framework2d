#pragma once

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>

class OutputConsole: public TypedGameComponent<OutputConsole>, public ConsoleInterface
{
public:
  OutputConsole(const std::string& _name);
  ~OutputConsole();
  static std::string name()
  {
    return "OutputConsole";
  }
  static void registerActions(GameComponentType* _type);
  GameComponent* getComponent(){return this;}
};
