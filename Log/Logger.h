#ifndef LOGGER_H
#define LOGGER_H

#include <string>

class Logger
{
public:
  Logger();
  virtual ~Logger();
  virtual void outputText(std::string _msg, unsigned int timeStamp, unsigned int _colour = -1){throw -1;}
  virtual Logger* newLogger(Logger* _newLogger)=0;
protected:
private:
};

#endif // LOGGER_H
