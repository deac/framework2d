#include <Log/OutputConsole.h>

OutputConsole::OutputConsole(const std::string& _name)
  :TypedGameComponent(_name),
   ConsoleInterface(true)
{
}

OutputConsole::~OutputConsole()
{
}

void OutputConsole::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<OutputConsole, ConsoleInterface>();
}
