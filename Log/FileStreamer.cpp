#include <Log/FileStreamer.h> /// FIXME move this to Objects
#include <Types/Thread.h>

ActionHandle* FileStreamer::mainHandle = nullptr;

FileStreamer::FileStreamer(FILE* _input, FILE* _output)
  :ConsoleInterface(false)
{
  inputFile = _input;
  outputFile = _output;
  Thread* thread = new Thread(this, mainHandle);
  attachChild(thread);
  thread->run();
}
FileStreamer::~FileStreamer()
{
}

void FileStreamer::readLoop()
{
  while (true)
    {
      char buffer[256];
      fgets(buffer, 256, inputFile);
      std::string str(buffer);
      if (str != "")
	output(&str);
    }
}

void FileStreamer::executeCommand(const std::string& _message)
{
  std::cout << _message << std::endl;
  fputs(_message.c_str(), outputFile);
}
void FileStreamer::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<FileStreamer, ConsoleInterface>();
  mainHandle = _type->createActionHandle<FileStreamer>("main", &FileStreamer::readLoop);
}
							   
