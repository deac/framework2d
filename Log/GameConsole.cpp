#include "GameConsole.h"
#include <Log/Log.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>
#include <Filesystem/Filesystem.h>
#include <AbstractFactory/InstanceParameters.h>
#include <Log/ConsoleBox.h>

GameConsole::GameConsole(GameComponent* _consoleInterface)
  :TypedGameComponent("console")
{
  auto root = CEGUI::System::getSingletonPtr()->getGUISheet();
  consoleWindow = root->getChildRecursive("Root/Group1/Console");
  assert(consoleWindow);

  CEGUI::TabControl* tabControl = dynamic_cast<CEGUI::TabControl*>(consoleWindow->getChild("Root/Group1/Console/Consoles"));
  assert(tabControl);
  
  auto sendButton = consoleWindow->getChild("Root/Group1/Console/SendButton");
  assert(sendButton);
  sendButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameConsole::handleSendButtonPressed,this));
  
  tabControl->subscribeEvent(CEGUI::TabControl::EventSelectionChanged, CEGUI::Event::Subscriber(&GameConsole::handleTabSelectionChanged, this));

  CEGUI::Editbox* input = static_cast<CEGUI::Editbox*>(consoleWindow->getChild("Root/Group1/Console/EditBox"));
  assert(input);
  assert(input->getType() == "TaharezLook/Editbox");
  input->subscribeEvent(CEGUI::Window::EventKeyUp, CEGUI::Event::Subscriber(&GameConsole::handleKeyUp, this));
  /*consoleWindow->getChild("Root/Console/EditBox")->subscribeEvent(CEGUI::Editbox::EventMouseClick,
    CEGUI::Event::Subscriber(&GameConsole::handleTextSubmitted,this));*/
  newConsole(_consoleInterface->getInterface<ConsoleInterface>(), "engine");
}

GameConsole::~GameConsole()
{
  //dtor
}

void GameConsole::createCEGUIWindow()
{
}

void GameConsole::newConsole(ConsoleInterface* _console, const std::string& _name)
{
  InstanceParameters params(this, _name);
  params.getTypeTable()->addValue<ConsoleInterface*>("console", _console);
  params.getTypeTable()->addValue<CEGUI::TabControl*>("tabControl", dynamic_cast<CEGUI::TabControl*>(consoleWindow->getChild("Root/Group1/Console/Consoles")));
  params.getTypeTable()->addValue<std::string>("name", _name);
  activeConsole = ConsoleBox::privateFactory(&params, this);
  //attachChild(new ConsoleBox(_console, newWindow, _name));
}
#include <iostream>
bool GameConsole::handleTabSelectionChanged(const CEGUI::EventArgs &e)
{
  const CEGUI::WindowEventArgs& args = dynamic_cast<const CEGUI::WindowEventArgs&>(e);
  CEGUI::TabControl* tabControl = dynamic_cast<CEGUI::TabControl*>(args.window);
  CEGUI::Window* tabPane = tabControl->getTabContentsAtIndex(tabControl->getSelectedTabIndex());
  ConsoleBox* console = static_cast<ConsoleBox*>(getNode(tabPane->getName().c_str()));
  activeConsole = console;

  CEGUI::Editbox* editBox = dynamic_cast<CEGUI::Editbox*>(consoleWindow->getChild("Root/Group1/Console/EditBox"));
  assert(editBox);
  editBox->setReadOnly(activeConsole->isReadOnly());
  if (activeConsole->isReadOnly())
    editBox->setText(tabPane->getName() + " is read only");
  else
    editBox->setText("");
}
bool GameConsole::handleKeyUp(const CEGUI::EventArgs &_e)
{
  const CEGUI::KeyEventArgs& e = static_cast<const CEGUI::KeyEventArgs&>(_e);
  if (e.scancode == CEGUI::Key::Return)
    enterText();
  return true;
}
bool GameConsole::handleSendButtonPressed(const CEGUI::EventArgs &_e)
{
  enterText();
  return true;
}
void GameConsole::enterText()
{
  if (!activeConsole->isReadOnly())
    {
      CEGUI::String Msg = consoleWindow->getChild("Root/Group1/Console/EditBox")->getText();
      parseText(Msg);
      consoleWindow->getChild("Root/Group1/Console/EditBox")->setText("");
    }
}
void GameConsole::parseText(CEGUI::String _inString)
{
  std::string inString = _inString.c_str();
  activeConsole->input(inString);
}

Logger* GameConsole::newLogger(Logger* _newLogger)
{
  throw "Error: Invalid command for this class";
}

void GameConsole::registerActions(GameComponentType* _type)
{
}
