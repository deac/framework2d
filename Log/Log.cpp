#include "Log.h"
#include <Log/GameConsole.h>
#include <Filesystem/Filesystem.h>
#include <Log/UninitialisedLogger.h>
#include <Log/OutputConsole.h>
#include <SDL/SDL_timer.h>
#include <iostream>

Log::Log()
  :Singleton<Log>("log")
{
  //ctor
}

Log::~Log()
{
  //dtor
}

void Log::singletonInit()
{
  logger = new UninitialisedLogger;
  attachChild(new OutputConsole("errors"));
  attachChild(new OutputConsole("warnings"));
  attachChild(new OutputConsole("messages"));
  //logger = logger->newLogger(new GameConsole(Filesystem::global()->getNode("dev/engine")));
  GameConsole* _logger = new GameConsole(Filesystem::global()->getNode("dev/engineMobile"));
  logger = _logger;
  _logger->newConsole(getNode("errors")->getInterface<ConsoleInterface>(), "errors");
  _logger->newConsole(getNode("warnings")->getInterface<ConsoleInterface>(), "warnings");
  _logger->newConsole(getNode("messages")->getInterface<ConsoleInterface>(), "messages");
  //_logger->newConsole(Filesystem::global()->getNode("gameMode")->getInterface<ConsoleInterface>(), "editor");
  assert(getParentNode()->getNode("log") == this);
}
/*void Log::outputText(ConsoleInterface* _interface, const std::string& _message)
{
  logger->outputText(_message, -1, -1);
}*/

void Log::newConsole(ConsoleInterface* _console, const std::string& _name)
{
  static_cast<GameConsole*>(logger)->newConsole(_console, _name);
}
std::string Log::getTimeString(unsigned int _timeStamp)
{
  unsigned int milliseconds = _timeStamp % 1000;
  unsigned int seconds = _timeStamp / 1000;
  unsigned int minutes = seconds / 60;
  seconds = seconds % 60;
  unsigned int hours = minutes / 60;
  minutes = minutes % 60;
  char buffer[16];
  sprintf(buffer, "%d:%d:%d", hours, minutes, seconds);
  //sprintf(buffer, "%d:%d:%d:%d", hours, minutes, seconds, milliseconds);
  return buffer;
}

void Log::warning(const std::string& _message)
{
  std::string message = _message;
  getNode("warnings")->getInterface<ConsoleInterface>()->output(&message);
}
void Log::message(const std::string& _message)
{
  std::string message = _message;
  getNode("messages")->getInterface<ConsoleInterface>()->output(&message);
}

void Log::error(const std::string& _message)
{
  std::cerr << "Fail derp " << _message;
  std::string message = _message;
  getNode("errors")->getInterface<ConsoleInterface>()->error(&message);
}
/*<<<<<<< HEAD
void Log::uncaughtError(const std::string& _message)
{
  std::string message = _message;
  getNode("errors")->getInterface<ConsoleInterface>()->output(&message);
}
void Log::catchError(const std::string& _message, Error* _error)
{
  std::string message = _message;
  getNode("errors")->getInterface<ConsoleInterface>()->output(&message);
  _error->caught();
}
Log::Error::Error(const std::string& _message)
  :message(_message)
{
  wasCaught = false;
}
Log::Error::~Error()
{
  if (!wasCaught)
    {
      std::cerr << "Exiting: " << message << std::endl;
      exit(-1);
    }
}

void Log::Error::catchError()
{
  std::cerr << " ...caught" << std::endl;
  wasCaught = true;
}
void Log::Error::catchError(const std::string& _catchMessage)
{
  g_Log.catchError(_catchMessage, this);
}
void Log::Error::caught()
{
  assert(!wasCaught);
  std::cerr << " ...caught" << std::endl;
  wasCaught = true;
}
=======
>>>>>>> Rewrote error handling into ConsoleInterface.*/

void Log::singletonRegisterActions(GameComponentType* _type)
{
}
