#include <Log/ConsoleBox.h>
#include <Objects/GameInterfaces/ConsoleInterface.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <CEGUI/CEGUI.h>
#include <AbstractFactory/DefaultFactories.h>
#include <Log/Log.h>

ConsoleBox::ConsoleBox()
{
  console = nullptr;
  window = nullptr;
}

ConsoleBox::~ConsoleBox()
{
}

void ConsoleBox::init(PropertyBagInterface* _params)
{
  TypeTable* table = _params->getTypeTable();
  console = table->getValue<ConsoleInterface*>("console", nullptr);
  std::string consoleName = table->getValue<std::string>("name", getName());
  setName(consoleName + "Console");
  assert(console);
  
  CEGUI::TabControl* tabControl = table->getValue<CEGUI::TabControl*>("tabControl", nullptr);
  assert(tabControl);
  window = tabControl->getTabContentsAtIndex(0)->clone(getName()); /// FIXME don't know another way to do this
  window->setText(consoleName);
  tabControl->addTab(window);
  window->setUserData(this);
  console->getTextOutputHandle()->registerListener(this, outputPrint);
  console->getTextInputHandle()->registerListener(this, inputPrint);
  console->getErrorHandle()->registerListener(this, errorPrint);
}
ActionHandle* ConsoleBox::outputPrint = nullptr;
ActionHandle* ConsoleBox::inputPrint = nullptr;
ActionHandle* ConsoleBox::errorPrint = nullptr;
void ConsoleBox::registerActions(GameComponentType* _type)
{
  inputPrint = _type->createActionHandle<ConsoleBox, std::string>("inputPrint", &ConsoleBox::inputPrintAction);
  outputPrint = _type->createActionHandle<ConsoleBox, std::string>("outputPrint", &ConsoleBox::outputPrintAction);
  errorPrint = _type->createActionHandle<ConsoleBox, std::string>("errorPrint", &ConsoleBox::errorThrowAction);
  //DefaultFactories::singleton()->createPrivateSimpleFactory<ConsoleBox>();
}

void ConsoleBox::staticRenderParameters(PropertyBagWindow* _window, TypeTable* _table)
{
}

ConsoleBox* ConsoleBox::privateFactory(PropertyBagInterface* _params, GameComponent* _parent)
{
  static auto factory = SimpleFactory<ConsoleBox>::privateFactory();
  auto ret = factory->createObject();
  _parent->attachChild(ret);
  factory->initObject(ret, _params);
  return static_cast<ConsoleBox*>(ret);
}

void ConsoleBox::inputPrintAction(std::string* _message)
{
  outputText("$:" + *_message);
}
void ConsoleBox::outputPrintAction(std::string* _message)
{
  outputText("> " + *_message, false);
}
void ConsoleBox::errorThrowAction(std::string* _message)
{
  outputText("> " + *_message, false, 0, 0xFFFF0000);
}

bool ConsoleBox::isReadOnly()
{
  return console->isReadOnly();
}

void ConsoleBox::input(const std::string& _message)
{
  std::string message = _message;
  console->input(&message);
}

void ConsoleBox::outputText(std::string _msg, bool _leftAligned, unsigned int _timeStamp, unsigned int _colour)
{
  CEGUI::Listbox *outputWindow = dynamic_cast<CEGUI::Listbox*>(window);
  //static_cast<CEGUI::Listbox*>(window->getChildRecursive("ChatBox"));
  assert(outputWindow);

  CEGUI::ListboxTextItem *newItem=0;
  newItem = new CEGUI::ListboxTextItem(Log::getTimeString(_timeStamp) + ' ' + _msg, _leftAligned?CEGUI::HTF_WORDWRAP_LEFT_ALIGNED:CEGUI::HTF_WORDWRAP_RIGHT_ALIGNED);
  newItem->setTextColours(_colour);
  outputWindow->addItem(newItem);
}

