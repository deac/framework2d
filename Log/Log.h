#ifndef LOG_H
#define LOG_H

#include <string>
#include <Objects/Singleton.h>
class Logger;
class ConsoleInterface;

class Log: public Singleton<Log>
{
public:
  void singletonInit();
  void debug(const std::string& _message);
  void warning(const std::string& _message);
  void message(const std::string& _message);
  void error(const std::string& _message);
  static std::string getTimeString(unsigned int _timeStamp);
  static void singletonRegisterActions(GameComponentType* _type);    
  static std::string name()
  {
    return "Logger";
  }
  void newConsole(ConsoleInterface* _console, const std::string& _name);
protected:
private:
  friend class Singleton<Log>;
  Log();
  virtual ~Log();
  void uncaughtError(const std::string& _message);
  Logger* logger;
};

#define g_Log (*Log::singleton())
#endif // LOG_H
