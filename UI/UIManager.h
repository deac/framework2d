#pragma once

#include <Objects/GameComponent.h>
namespace CEGUI
{
  class Window;
}

class UIManager: public TypedGameComponent<UIManager>
{
public:
  UIManager();
  ~UIManager();
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return "UIManager";
  }
  void changeWorkspace(const std::string& _workspaceName);
  CEGUI::Window* getWorkspace(){return currentWorkspace;}
  class UserInputInterface* getActiveInterface();
private:
  CEGUI::Window* root;
  CEGUI::Window* currentWorkspace;
};
