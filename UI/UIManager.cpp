#include <UI/UIManager.h>
#include <Filesystem/Filesystem.h>
#include <Objects/GameInterfaces/UserInputInterface.h>
#include <CEGUI/CEGUI.h>
using namespace CEGUI;

UIManager::UIManager()
  :TypedGameComponent("ui")
{
  root = System::getSingleton().getGUISheet();
  currentWorkspace = root->getChild("Root/Group2");
  changeWorkspace("Root/Group1");
}
UIManager::~UIManager()
{
}

void UIManager::registerActions(GameComponentType* _type)
{
}

void UIManager::changeWorkspace(const std::string& _workspace)
{
  currentWorkspace->hide();
  currentWorkspace->disable();
  currentWorkspace = root->getChild(_workspace.c_str());
  currentWorkspace->show();
  currentWorkspace->enable();
}

UserInputInterface* UIManager::getActiveInterface()
{
  return Filesystem::singleton()->getNode("game/controls")->getInterface<UserInputInterface>();
}
