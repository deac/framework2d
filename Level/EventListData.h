#ifndef EVENTLISTDATA_H
#define EVENTLISTDATA_H

#include <Level/LoadedData.h>
class TiXmlElement;

class EventListData : public LoadedData
{
public:
  EventListData(const std::string& _name);
  virtual ~EventListData();
  const std::string& getName(){return name;}
protected:
private:
  void virtualSave(XmlDataSaver* _saver, const std::string* _address);
  std::string name;
};

#endif // EVENTLISTDATA_H
