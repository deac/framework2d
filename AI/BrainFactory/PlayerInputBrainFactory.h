#ifndef PLAYERINPUTBRAINFACTORY_H
#define PLAYERINPUTBRAINFACTORY_H

#include <Objects/GameInterfaces/FactoryInterface.h>
#include <AI/PlayerInputBrain.h>

class PlayerInputBrainFactory : public TypedGameComponent<PlayerInputBrainFactory>, public TemplateFactoryInterface<PlayerInputBrain, PlayerInputBrainFactory>
{
public:
  PlayerInputBrainFactory();
  virtual ~PlayerInputBrainFactory();
  void init(PropertyBagInterface* _loader);
  static std::string name()
  {
    return "PlayerInputBrainFactory";
  }
protected:
private:
  std::vector<std::string> controls;
};

#endif // PLAYERINPUTBRAINFACTORY_H
