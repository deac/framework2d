#ifndef PLAYERINPUTBRAIN_H
#define PLAYERINPUTBRAIN_H

#include "Brain.h"
#include <Input/Mouse/InputContext.h>
#include <map>
class PathFollower;

class PlayerInputBrain : public Brain
{
public:
  PlayerInputBrain(unsigned short _entityKey, std::vector<std::string> controls);
  virtual ~PlayerInputBrain();
  void buttonDown(Vec2i mouse, unsigned char button);
  void mouseMove(Vec2i mouse);
  void buttonUp(Vec2i mouse, unsigned char button);
  void activate();
  void update();
protected:
private:
  std::string virtualPrint();
  PathFollower* follower;
  std::map<unsigned int, unsigned char> buttonActionMap;
};

#endif // PLAYERINPUTBRAIN_H
