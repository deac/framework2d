#include "Skin.h"
#include <GL/gl.h>
#include <Box2D/Box2D.h>
#include <Graphics/Contexts/MaterialContext.h>
#include <Physics/BodyParts/BodyPart.h>
#define M_PI 3.14159265358979323846
Skin::Skin()
{
  //ctor
  material = nullptr;
  transform = nullptr;
}

Skin::~Skin()
{
  //dtor
  material->release();
}

void Skin::setMaterial(MaterialContext* _material)
{
  assert(material == nullptr);
  material = _material;
}
void Skin::render(TransformInterface* _translation)
{
  material->bind();
  const Vec2f & center = _translation->getPosition();
  float rotation = -_translation->getAngle();
  glPushMatrix();
  glTranslatef(center.x,center.y,0);
  if (rotation != 0.0f)
    glRotatef(rotation*(180.0/M_PI),0,0,-1);
  vRender();
  glPopMatrix();
}

void Skin::update()
{ /// FIXME this is shit
  render(transform);
  /*for (GameComponent* object = getParentNode()->getChildren(); object; object = object->getNext())
    {
      try
	{
	  TransformInterface* interface = object->getInterface<TransformInterface>();
	  render(interface);
	  return;
	}
      catch (...) {}
    }*/
}
void Skin::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<Skin, UpdateFunctionInterface>();
  Skin* null = nullptr;
  _type->requestVariable<TransformInterface>(&(null->transform));
}
