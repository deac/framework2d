#include "BubbleSkin.h"
#include <GL/gl.h>

BubbleSkin::BubbleSkin()
{
  //ctor
}

BubbleSkin::~BubbleSkin()
{
  //dtor
}
void BubbleSkin::init(float _radius, MaterialContext* _material)
{
  radius = _radius;
  setMaterial(_material);
}

void BubbleSkin::vRender()
{
  glBegin(GL_QUADS);
  glTexCoord2f(0,0);
  glVertex2i(-radius,-radius);
  glTexCoord2f(1,0);
  glVertex2i(radius,-radius);
  glTexCoord2f(1,1);
  glVertex2i(radius,radius);
  glTexCoord2f(0,1);
  glVertex2i(-radius,radius);
  glEnd();
}
