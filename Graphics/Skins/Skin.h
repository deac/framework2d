#ifndef SKIN_H
#define SKIN_H

#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/UpdateFunctionInterface.h>
class BodyPart;
class MaterialContext;
class TransformInterface;
enum SkinType
  {
    eStaticSkinType,
    eConvexPolygonSkinType,
    eBubbleSkinType,
    eSkinTypeMax
  };

class Skin : public TypedGameComponent<Skin>, public UpdateFunctionInterface
{
public:
  Skin();
  virtual ~Skin();
  void render(TransformInterface* _translation);
  void update();
  virtual void vRender()=0;
  static void registerActions(GameComponentType* _type);

  static std::string name()
  {
    return "Skin__FIXME_needs_unique_names";
  }
protected:
  void setMaterial(MaterialContext* _material);
private:
  MaterialContext* material;
  TransformInterface* transform;
  std::string virtualPrint();
};

#endif // SKIN_H
