#ifndef BUBBLESKINFACTORY_H
#define BUBBLESKINFACTORY_H

#include <Objects/GameInterfaces/FactoryInterface.h>
#include <Graphics/Skins/BubbleSkin.h>

class BubbleSkinFactory : public TemplateFactoryInterface<BubbleSkin, BubbleSkinFactory>
{
public:
  BubbleSkinFactory();
  virtual ~BubbleSkinFactory();
  void initObject(BubbleSkin* _object, PropertyBagInterface* _params);
  void init(PropertyBagInterface* loader);
  static std::string name()
  {
    return "BubbleSkinFactory";
  }
protected:
private:
  std::string materialName;
  float radius;
};

#endif // BUBBLESKINFACTORY_H
