#ifndef CONVEXPOLYGONSKINFACTORY_H
#define CONVEXPOLYGONSKINFACTORY_H

#include <Objects/GameInterfaces/FactoryInterface.h>
#include <Graphics/Skins/ConvexPolygonSkin.h>

class ConvexPolygonSkinFactory : public TemplateFactoryInterface<ConvexPolygonSkin, ConvexPolygonSkinFactory>
{
public:
  ConvexPolygonSkinFactory();
  virtual ~ConvexPolygonSkinFactory();
  void init(PropertyBagInterface* loader);
  static std::string name()
  {
    return "ConvexPolygonSkinFactory";
  }
  void initObject(ConvexPolygonSkin* _skin, PropertyBagInterface* _parameters);
protected:
private:
  std::string materialName;
};

#endif // CONVEXPOLYGONSKINFACTORY_H
