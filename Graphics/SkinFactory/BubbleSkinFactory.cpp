#include "BubbleSkinFactory.h"
#include <Graphics/GraphicsManager.h> /// FIXME
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Types/TypeTable.h>

BubbleSkinFactory::BubbleSkinFactory()
{
  //ctor
}

BubbleSkinFactory::~BubbleSkinFactory()
{
  //dtor
}

void BubbleSkinFactory::init(PropertyBagInterface* _loader)
{
  TypeTable* table = _loader->getTypeTable();
  materialName = table->getValue<std::string>("materialName","player");
  radius = table->getValue<float>("radius",2.0f);
}
void BubbleSkinFactory::initObject(BubbleSkin* _object, PropertyBagInterface* _params)
{
  _object->init(radius, g_GraphicsManager.getMaterial(materialName.c_str()));
}
