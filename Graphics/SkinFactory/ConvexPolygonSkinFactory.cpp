#include "ConvexPolygonSkinFactory.h"
#include <Types/TypeTable.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Graphics/GraphicsManager.h> /// FIXME

ConvexPolygonSkinFactory::ConvexPolygonSkinFactory()
{
  //ctor
}

ConvexPolygonSkinFactory::~ConvexPolygonSkinFactory()
{
  //dtor
}

void ConvexPolygonSkinFactory::init(PropertyBagInterface* _loader)
{
  TypeTable* table = _loader->getTypeTable();
  materialName = table->getValue<std::string>("materialName","player");
}

void ConvexPolygonSkinFactory::initObject(ConvexPolygonSkin* _object, PropertyBagInterface* _parameters)
{
  TypeTable* table = _parameters->getTypeTable();
  std::vector<Vec2f> points = table->getArray<Vec2f>("points",{{0,0},{1,1},{-2,1}});
  _object->init(&points[0],points.size(),g_GraphicsManager.getMaterial(materialName.c_str()));
}
