#ifndef FREECAMERA_H
#define FREECAMERA_H

#include "Camera.h"

class FreeCamera : public Camera
{
    public:
        FreeCamera();
        virtual ~FreeCamera();
        void updateTransform(Vec2i resolution);
        void resetInput();
        void activate();
    protected:
    private:
        Vec2i position;
        Vec2f bias;
        bool enableBias;
};

#endif // FREECAMERA_H
