#include <AbstractFactory/DefaultFactories.h>

DefaultFactories* DefaultFactories::global()
{
  return singleton();
}

DefaultFactories::DefaultFactories()
  :Singleton("defaultFactories")
{
}
DefaultFactories::~DefaultFactories()
{
}

void DefaultFactories::singletonInit()
{
}

void DefaultFactories::singletonRegisterActions(GameComponentType* _type)
{
}
