#pragma once

#include <Objects/GameInterfaces/FactoryInterface.h>
#include <Objects/GameObject.h>

/*class Compilation: public TemplateFactoryInterface<GameObject,Compilation>
{
public:
  Compilation(GameComponent* _parent, const std::string& _name);
  void init(PropertyBagInterface* _loader);
  void initObject(GameObject* _object, PropertyBagInterface* _params);
  static std::string name()
  {
    return "GameObject";
  }
private:
  static FactoryInterface* gameObjectFactory();
  std::vector<FactoryInterface*> factories;
  std::vector<std::string> componentNames;
};*/
