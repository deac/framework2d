#include <AbstractFactory/PropertyBagWindow.h>
#include <AbstractFactory/InstanceParameters.h>
#include <Log/Log.h>
#include <Editor/Editors/DefaultEditor.h>
#include <UI/UIManager.h>
#include <CEGUI/CEGUI.h>
using namespace CEGUI;

PropertyBagWindow::PropertyBagWindow(PropertyBagInterface* _object)
{
  object = _object;
  window = WindowManager::getSingleton().loadWindowLayout("Component.layout", object->getProductName());
  static_cast<UIManager*>(Filesystem::singleton()->getNode("dev/graphics/ui"))->getWorkspace()->addChildWindow(window);
  window->setPosition({{0,0},{0,0}});
  window->invalidate();
  window->setEnabled(true);
  
  window->setText(object->getProductName());
  setType(object->getProductType());
  try
    {
      setParent(object->getComponent()->getParentNode()->getInterface<PropertyBagInterface>());
    }
  catch (...)
    {
      setParent(nullptr);
    }
  
  //CEGUI::System::getSingleton().getGUISheet()->addChildWindow(window);
  assert(dynamic_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties")));
  MultiColumnList* properties = static_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties"));
  properties->setSelectionMode(MultiColumnList::SelectionMode::CellSingle);
  properties->addColumn("Type",0, {0.3,0});
  properties->addColumn("Name",1, {0.3,0});
  properties->addColumn("Value",2, {0.3,0});
  properties->subscribeEvent(MultiColumnList::EventSelectionChanged, Event::Subscriber(&PropertyBagWindow::handlePropertySelectionChanged, this));

  Listbox* children = dynamic_cast<Listbox*>(window->getChild(object->getProductName() + "Children"));
  assert(children);
  for (GameComponent* childNode = object->getComponent()->getChildren(); childNode; childNode = childNode->getNext())
    {
      PropertyBagInterface* child = childNode->getInterface<PropertyBagInterface>();
      auto item = new ListboxTextItem(child->getProductName());
      item->setUserData(child);
      children->addItem(item);
    }
  children->subscribeEvent(Listbox::EventSelectionChanged, Event::Subscriber(&PropertyBagWindow::handleChildSelected, this));

  Window* type = window->getChild(object->getProductName() + "Type");
  type->subscribeEvent(Window::EventMouseButtonUp, Event::Subscriber(&PropertyBagWindow::handleTypeSelected, this));
}

PropertyBagWindow::~PropertyBagWindow()
{
}

void PropertyBagWindow::setType(const std::string& _type)
{
  window->getChild(object->getProductName() + "Type")->setText(_type.c_str());
}
void PropertyBagWindow::setParent(PropertyBagInterface* _parent)
{
  if (_parent)
    window->getChild(object->getProductName() + "Parent")->setText(_parent->getProductName().c_str());
  else
    window->getChild(object->getProductName() + "Parent")->setText("no parent");
}


void PropertyBagWindow::setProperty(const std::string& _type, const std::string& _name, const std::string& _value)
{
  assert(dynamic_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties")));
  MultiColumnList* properties = static_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties"));
  if (properties->getRowCount() != 0)
    {
      ListboxItem* property = properties->findColumnItemWithText(_name, 1, nullptr);
      if (property)
	{
	  uint row = properties->getItemRowIndex(property);
	  properties->removeRow(row);
	}
    }
  unsigned int row = properties->addRow();
  ListboxTextItem* type = new ListboxTextItem(_type.c_str(),0);
  properties->setItem(type, 0, row);
  ListboxTextItem* name = new ListboxTextItem(_name.c_str(),0);
  properties->setItem(name, 1, row);
  ListboxTextItem* value = new ListboxTextItem(_value.c_str(),0);
  properties->setItem(value, 2, row);
}
void PropertyBagWindow::setEvent(const std::string& _name)
{
  Listbox* events = dynamic_cast<Listbox*>(window->getChild(object->getProductName() + "Events"));
  assert(events);
  auto item = new ListboxTextItem(_name);
  //item->setUserData(child);
  events->addItem(item);
  //events->subscribeEvent(Listbox::EventSelectionChanged, Event::Subscriber(&PropertyBagWindow::handleChildSelected, this));
}

bool PropertyBagWindow::handlePropertySelectionChanged(const EventArgs& _event)
{
  assert(dynamic_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties")));
  MultiColumnList* properties = static_cast<MultiColumnList*>(window->getChild(object->getProductName() + "Properties"));
  ListboxItem* selected = properties->getFirstSelectedItem();
  MCLGridRef gridRef = properties->getItemGridReference(selected);
  if (gridRef.column == 2)
    {
      new DefaultEditor(object->getProductName(),properties->getItemAtGridReference({gridRef.row,0})->getText().c_str(), properties->getItemAtGridReference({gridRef.row,1})->getText().c_str(), selected->getText().c_str(), this);
    }
}
#include <iostream>
bool PropertyBagWindow::handleChildSelected(const EventArgs& _event)
{
  Listbox* children = dynamic_cast<Listbox*>(window->getChild(object->getProductName() + "Children"));
  assert(children == static_cast<const WindowEventArgs&>(_event).window);
  ListboxItem* item = children->getFirstSelectedItem();
  std::cout << item->getText() << std::endl;
  PropertyBagInterface* interface = static_cast<PropertyBagInterface*>(item->getUserData());
  assert(interface);
  assert(interface->getProductName() == item->getText());
  InstanceParameters* params = dynamic_cast<InstanceParameters*>(interface);
  assert(params);
  params->createWindow();
}

bool PropertyBagWindow::handleTypeSelected(const EventArgs& _event)
{
  object->getFactoryProperties()->createWindow();
}
