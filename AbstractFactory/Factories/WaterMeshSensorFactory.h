#pragma once

#include <AbstractFactory/AbstractFactory.h>
#include <Physics/BodyParts/BodyPart.h>
#include <Graphics/Skins/Skin.h>
class Entity;
class FactoryLoader;

class WaterMeshSensorFactory : public AbstractFactory<Entity, WaterMeshSensorFactory>
{
    public:
        WaterMeshSensorFactory();
        virtual ~WaterMeshSensorFactory();
        Entity* useFactory(FactoryParameters* _parameters);
        void init(FactoryLoader* loader, AbstractFactories* factories);
        static std::string name()
        {
            return "WaterMeshSensorFactory";
        }
    protected:
        AbstractFactoryBase<Skin>* skinFactory;
        AbstractFactoryBase<BodyPart>* bodyFactory;
    private:
};
