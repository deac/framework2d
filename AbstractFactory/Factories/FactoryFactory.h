#pragma once

template <typename Factory>
class FactoryFactory : public TypedGameComponent<FactoryFactory<Factory>>, public TemplateFactoryInterface<Factory, FactoryFactory<Factory>>
{
public:
  FactoryFactory();
  ~FactoryFactory();
  GameComponent* createObject();
  void initObject(GameComponent* _object, PropertyBagInterface* _params);

  void baseInit(const std::string& _name, PropertyBagInterface* _loader){}
  //GameComponent* privateUseFactory(PropertyBagInterface* _params, GameComponent* _parent);
  void build(PropertyBagInterface* _parameters);
  static void registerActions(GameComponentType* _type);
  static std::string name()
  {
    return Factory::name() + "FactoryFactory";
  }
  /*void renderParameters(CEGUI::Window* _parameters)
  {
    Factory::staticRenderParameters(_parameters);
    }*/
};


template <typename Factory>
FactoryFactory<Factory>::FactoryFactory()
{
  GameComponent::setName(name()+"kill the jews");
}
template <typename Factory>
FactoryFactory<Factory>::~FactoryFactory()
{
}
template <typename Factory>
void FactoryFactory<Factory>::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<FactoryFactory<Factory>, FactoryInterface>();
}

template <typename Factory>
GameComponent* FactoryFactory<Factory>::createObject()
{
  return new Factory;
}
template <typename Factory>
void FactoryFactory<Factory>::initObject(GameComponent* _object, PropertyBagInterface* _params)
{
  assert(dynamic_cast<Factory*>(_object));
  Factory* factory = static_cast<Factory*>(_object);
  factory->init(_params);
}
/*template <typename Factory>
GameComponent* FactoryFactory<Factory>::privateUseFactory(PropertyBagInterface* _parameters, GameComponent* _parent)
{
  Factory* factory = new Factory;
  factory->baseInit(_parameters->getProductName(), _parameters);
  _parent->attachChild(factory);
  return factory;
  }*/
