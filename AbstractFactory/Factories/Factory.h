#pragma once

#include <Objects/GameInterfaces/FactoryInterface.h>

template <typename Product, typename StaticData>
class AutoFactory: public TypedGameComponent<AutoFactory<Product, StaticData>>, public TemplateFactoryInterface<Product, AutoFactory<Product, StaticData>>
{
public:
  AutoFactory();
  ~AutoFactory();
  void init(PropertyBagInterface* _loader);
  void initObject(Product* _object, PropertyBagInterface* _params);
  //GameComponent* privateUseFactory(PropertyBagInterface* _params, GameComponent* _parent);
  static std::string name()
  {
    return StaticData::name() + "AutoFactory";
  }
  static void registerActions(GameComponentType* _type);
  void renderParameters(PropertyBagWindow* _parameters, TypeTable* _table)
  {
    Product::staticRenderParameters(_parameters, _table);
  }
private:
  StaticData data;
};

template <typename Product, typename StaticData>
class Factory: public TypedGameComponent<Factory<Product, StaticData>>, public TemplateFactoryInterface<AutoFactory<Product, StaticData>, Factory<Product, StaticData>>
{
public:
  static Factory<Product, StaticData>* privateFactory();
  void initObject(AutoFactory<Product, StaticData>* _object, PropertyBagInterface* _params);
  //void baseInit(PropertyBagInterface* _loader);
  //GameComponent* privateUseFactory(PropertyBagInterface* _params, GameComponent* _parent);
  static std::string name()
  {
    return StaticData::name() + "AutoFactoryFactory";
  }
  static void registerActions(GameComponentType* _type);
  void renderParameters(PropertyBagWindow* _parameters, TypeTable* _table)
  {
    StaticData::staticRenderParameters(_parameters, _table);
    //Product::staticRenderParameters(_parameters, _table);
  }
private:
  Factory();
  ~Factory();
};

  
  

template <typename Product, typename StaticData>
Factory<Product, StaticData>::Factory()
  :TypedGameComponent<Factory<Product, StaticData>>(StaticData::name()+"Factory")
{
}
template <typename Product, typename StaticData>
Factory<Product, StaticData>::~Factory()
{
}

template <typename Product, typename StaticData>
Factory<Product, StaticData>* Factory<Product, StaticData>::privateFactory()
{
  auto ret = new Factory<Product, StaticData>;
  ret->setName(StaticData::name() + "Factory");
  return ret;
}

template <typename Product, typename StaticData>
void Factory<Product, StaticData>::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<Factory<Product, StaticData>, FactoryInterface>();
}

template <typename Product, typename StaticData>
void Factory<Product, StaticData>::initObject(AutoFactory<Product, StaticData>* _object, PropertyBagInterface* _params)
{
  _object->init(_params);
}

/*template <typename Product, typename StaticData>
GameComponent* Factory<Product, StaticData>::privateUseFactory(PropertyBagInterface* _params, GameComponent* _parent)
{
  AutoFactory<Product, StaticData>* factory = new AutoFactory<Product, StaticData>;
  factory->baseInit(_params->getProductName(), _params);
  return factory;
  }*/

template <typename Product, typename StaticData>
AutoFactory<Product, StaticData>::AutoFactory()
{
}
template <typename Product, typename StaticData>
AutoFactory<Product, StaticData>::~AutoFactory()
{
}
template <typename Product, typename StaticData>
void AutoFactory<Product, StaticData>::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<AutoFactory<Product, StaticData>, FactoryInterface>();
}
template <typename Product, typename StaticData>
void AutoFactory<Product, StaticData>::init(PropertyBagInterface* _loader)
{
  Product::typeInit(&data, _loader);
}
template <typename Product, typename StaticData>
void AutoFactory<Product, StaticData>::initObject(Product* _object, PropertyBagInterface* _params)
{
  _object->instanceInit(&data, _params);
}

/*template <typename Product, typename StaticData>
GameComponent* AutoFactory<Product, StaticData>::privateUseFactory(PropertyBagInterface* _params, GameComponent* _parent)
{
  Product* product = new Product;
  product->setName(_params->getProductName());
  if (_parent != nullptr)
    _parent->attachChild(product);
  product->instanceInit(&data, _params);
  return product;
  }*/
