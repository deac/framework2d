#include "WaterMeshSensorFactory.h"
#include <Entities/WaterMeshSensor.h>
#include <Physics/WaterPhysicsSystem/Boundary.h>

WaterMeshSensorFactory::WaterMeshSensorFactory()
{
}

WaterMeshSensorFactory::~WaterMeshSensorFactory()
{
}

void WaterMeshSensorFactory::init(FactoryLoader* _loader, AbstractFactories* _factories)
{
  skinFactory = _loader->getFactory<Skin>("skin","StaticSkinFactory");
  bodyFactory = _loader->getFactory<BodyPart>("body", "b2FixtureBodyPartFactory");
}

Entity* WaterMeshSensorFactory::useFactory(FactoryParameters* _parameters)
{
  BedBoundary* boundary = _parameters->get<BedBoundary*>("boundary", nullptr);
  assert(boundary);
  WaterMeshSensor* entity = new WaterMeshSensor(boundary);

  entity->baseInit(skinFactory->use(_parameters, entity));

  BodyPart* body = bodyFactory->use(_parameters, entity);
  entity->setRootBody(body);

  return entity;
}
