#pragma once

#include <Objects/GameInterfaces/FactoryInterface.h>

template <typename Product>
class SimpleFactory: public TypedGameComponent<SimpleFactory<Product>>, public TemplateFactoryInterface<Product, SimpleFactory<Product>>
{
public:
  static SimpleFactory<Product>* privateFactory();
  void baseInit(const std::string& _name, PropertyBagInterface* _loader){}
  GameComponent* createObject();
  void initObject(GameComponent* _object, PropertyBagInterface* _params);
  static std::string name()
  {
    return Product::name();
  }
  static void registerActions(GameComponentType* _type);
  void renderParameters(PropertyBagWindow* _parameters, TypeTable* _table)
  {
    Product::staticRenderParameters(_parameters, _table);
  }
private:
  SimpleFactory();
  ~SimpleFactory();
};


template <typename Product>
SimpleFactory<Product>::SimpleFactory()
{
  GameComponent::setName(name());
}
template <typename Product>
SimpleFactory<Product>::~SimpleFactory()
{
}
template <typename Product>
SimpleFactory<Product>* SimpleFactory<Product>::privateFactory()
{
  return new SimpleFactory<Product>;
}

template <typename Product>
GameComponent* SimpleFactory<Product>::createObject()
{
  return new Product;
}
template <typename Product>
void SimpleFactory<Product>::initObject(GameComponent* _object, PropertyBagInterface* _params)
{
  Product* product = static_cast<Product*>(_object);
  product->init(_params);
}
template <typename Product>
void SimpleFactory<Product>::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<SimpleFactory<Product>, FactoryInterface>();
}

