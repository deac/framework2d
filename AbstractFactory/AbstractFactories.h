#ifndef ABSTRACTFACTORIES_H
#define ABSTRACTFACTORIES_H

class AbstractFactoryBase;
#include <Objects/GameComponent.h>
#include <string>
#include <vector>
#include <cassert>
#include <unordered_map>
typedef std::string AbstractFactoryReference;
class AbstractFactoryList;
class FactoryParameters;
class UntypedAbstractFactory;
template <typename DerivedEvent>
class EventsListener;
class FactoryLoader;
class AbstractFactoryListBase;
class PhysicsManager;

class AbstractFactories : public TypedGameComponent<AbstractFactories>
{
public:
  AbstractFactories();
  ~AbstractFactories();
  void init();

  GameComponent* useFactory(AbstractFactoryReference factory, FactoryParameters* _parameters, GameComponent* _parent);

  template <typename Factory>
  static void registerFactoryType();

  AbstractFactoryBase* getFactory(const std::string& name);

  AbstractFactoryBase* addFactory(FactoryLoader* _loader);

  GameComponent* addFactory(const std::string& _product, FactoryLoader* _loader);

  void print();

  void setWorld(PhysicsManager* _world);
  PhysicsManager* getWorld();

  static void registerActions(GameComponentType* _type);

  static std::string name()
  {
    return "AbstractFactories";
  }
protected:
private:
  std::string virtualPrint();
  /*class ProductType
  {
  public:
    ProductType(unsigned int& _productId){productId = _productId++;}
    virtual AbstractFactoryListBase* createFactoryList(AbstractFactories* _factories)=0;
    unsigned int getProductId(){return productId;}
  private:
    unsigned int productId;
  };
  static std::unordered_map<std::string, ProductType*>& productTypeList()
  {
    static std::unordered_map<std::string, ProductType*> list;
    return list;
  }
  template <typename Product>
  class DerivedProductType: public ProductType
  {
  public:
    DerivedProductType(unsigned int& _productId, std::unordered_map<std::string, class ProductType*>* _productTypeList);
    AbstractFactoryListBase* createFactoryList(AbstractFactories* _factories);
    };
  static std::vector<ProductType*> productTypes;
  static unsigned int& newProductIds()
  {
    static unsigned int productId = 0;
    return productId;
  }
  template <typename Product>
  static ProductType* getProductType()
  {
    static DerivedProductType<Product> productType(newProductIds(), &productTypeList());
    return &productType;
    }*/
  /*AbstractFactoryList* getFactoryList()
  {
    ProductType* type = getProductType<Product>();
    AbstractFactoryListBase* factoryList = indexedFactoryLists[type->getProductId()];
    assert(factoryList);
    assert(dynamic_cast<AbstractFactoryList<Product>*>(factoryList));
    return static_cast<AbstractFactoryList<Product>*>(factoryList);
  }
  std::vector<AbstractFactoryListBase*> indexedFactoryLists;
  PhysicsManager* physicsManager;
  CollisionDatabase collisionDatabase;*/
};

/** Implementation
*
*
*/


/*#include <AbstractFactory/AbstractFactoryList.h>
#include <AbstractFactory/TypeTableFactoryType.h>
#include <Types/TypeTable.h>

AbstractFactories::useFactory(AbstractFactoryReference factory, FactoryParameters* _parameters, GameComponent* _parent)
{
    return getFactoryList<Product>()->useFactory(factory,_parameters, _parent);
}
AbstractFactoryBase* AbstractFactories::getFactory(const std::string& name)
{
    return getFactoryList<Product>()->getFactory(name);
}

template <typename Factory>
void AbstractFactories::registerFactoryType()
{
    getProductType<Product>();
    auto creator = new TemplateFactoryCreator<Product, Factory>;
    AbstractFactoryList<Product>::registerFactoryType(Factory::name(), creator);
}

AbstractFactoryBase* AbstractFactories::addFactory(FactoryLoader* _loader)
{
    return getFactoryList()->AbstractFactoryList::addFactory(this, _loader);
}

AbstractFactories::DerivedProductType<Product>::DerivedProductType(unsigned int& _productId, std::unordered_map<std::string, class ProductType*>* _productTypeList)
:ProductType(_productId)
{
    (*_productTypeList)[EvaluateTypeName<Product>()] = this;
    TypeTable::overloadType<AbstractFactoryBase<Product>*>(AbstractFactoryList<Product>::getProductName()+"Factory", new TypeTableFactoryType<Product>);
}

template <typename Product>
AbstractFactoryListBase* AbstractFactories::DerivedProductType<Product>::createFactoryList(AbstractFactories* _factories)
{
    return new AbstractFactoryList<Product>(_factories);
}
*/









#endif // ABSTRACTFACTORIES_H
