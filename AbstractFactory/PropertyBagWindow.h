#pragma once

#include <string>
class PropertyBagInterface;
namespace CEGUI
{
  class Window;
  class EventArgs;
}

class PropertyBagWindow
{
public:
  PropertyBagWindow(PropertyBagInterface* _object);
  ~PropertyBagWindow();
  void setProperty(const std::string& _type, const std::string& _name, const std::string& _value);
  void setEvent(const std::string& _type);
  CEGUI::Window* getWindow(){return window;}
private:
  void setType(const std::string& _type);
  void setParent(PropertyBagInterface* _parent);
  bool handlePropertySelectionChanged(const CEGUI::EventArgs& _event);
  bool handleChildSelected(const CEGUI::EventArgs& _event);
  bool handleTypeSelected(const CEGUI::EventArgs& _event);
  CEGUI::Window* window;
  PropertyBagInterface* object;
};
