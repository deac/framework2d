#pragma once

#include <Objects/Singleton.h>

class DefaultFactories: public Singleton<DefaultFactories>
{
public:
  DefaultFactories();
  template <typename Product>
  void registerSimpleFactory();
  template <typename Product, typename StaticData>
  void addFactory();
  template <typename Factory>
  void registerFactoryType();
  static void singletonRegisterActions(GameComponentType* _type);
  void singletonInit();
  static DefaultFactories* global();
  static std::string name()
  {
    return "DefaultFactories";
  }
private:
  ~DefaultFactories();
};

#include <AbstractFactory/Factories/SimpleFactory.h>
#include <AbstractFactory/Factories/FactoryFactory.h>
#include <AbstractFactory/Factories/Factory.h>

template <typename Product>
void DefaultFactories::registerSimpleFactory()
{
  attachChild(SimpleFactory<Product>::privateFactory());
}
template <typename Product, typename StaticData>
void DefaultFactories::addFactory()
{
  attachChild(Factory<Product, StaticData>::privateFactory());
  /*GameComponent* factory = new Factory<Product, StaticData>();
  factory->setName(StaticData::name()+"Factory");
  attachChild(factory);*/
}

template <typename Factory>
void DefaultFactories::registerFactoryType()
{
  attachChild(new FactoryFactory<Factory>());
}
