#pragma once

#include <Types/Vec2f.h>
#include <Types/TypeTable.h>
#include <Objects/GameComponent.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <unordered_map>
#include <string>
class PropertyBagSerializer;

class InstanceParameters : public TypedGameComponent<InstanceParameters>, public PropertyBagInterface
{
  struct StaticData
  {
  };
public:
  InstanceParameters(GameComponent* _root, const std::string& _name);
  ~InstanceParameters();

  TypeTable* getTypeTable(){return &table;}

  void setType(const std::string& _type);
  const std::string& getType(){return type;}
  virtual std::string getProductType(){return type;}
	
  static std::string name()
  {
    return "InstanceParameters";
  }
  static void typeInit(StaticData* _data, PropertyBagInterface* _loader);
  static void registerActions(GameComponentType* _type);

  std::string getProductName(){return getName();}
  void setProductName(const std::string& _name){setName(_name);}
  FactoryInterface* getFactory();
  PropertyBagInterface* getChildren();
  PropertyBagInterface* getNext();
  PropertyBagWindow* createWindow();
  GameComponent* getComponent(){return this;}
protected:
  void setRoot(GameComponent* _root){root = _root;}
  GameComponent* getRoot(){return root;}
private:
  //std::unordered_map<std::string, float> _parameters;
  GameComponent* root;
  TypeTable table;
  std::string type;
};

/*template <typename Type>
void InstanceParameters::add(const std::string& name, const Type& value)
{
  table.addValue(name, value);
}

template <typename Type>
void InstanceParameters::addArray(const std::string& _name, const std::vector<Type>& _value)
{
  table.addArray(_name, _value);
}

template <typename Type>
Type InstanceParameters::get(const std::string& name, const Type& _default)
{
  return table.getValue(name, _default);
}
template <typename Type>
const std::vector<Type>& InstanceParameters::getArray(const std::string& name, const std::vector<Type>& _default)
{
  return table.getArray(name, _default);
}
*/
