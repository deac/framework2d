#include <AbstractFactory/InstanceParameters.h>
#include <Filesystem/Filesystem.h>
#include <Objects/GameInterfaces/PropertyBagInterface.h>
#include <Objects/GameInterfaces/FactoryInterface.h>
#include <AbstractFactory/PropertyBagWindow.h>

InstanceParameters::InstanceParameters(GameComponent* _root, const std::string& _name)
  :TypedGameComponent<InstanceParameters>(_name),
   table(nullptr, false) /// FIXME TypeTable shouldn't even use _root anymore
{
  root = _root;
}

InstanceParameters::~InstanceParameters()
{
}

PropertyBagWindow* InstanceParameters::createWindow()
{
  PropertyBagWindow* window = new PropertyBagWindow(this);
  getFactory()->renderParameters(window, &table);
  getFactory()->getProductType()->renderEvents(window);
  return window;
}

FactoryInterface* InstanceParameters::getFactory()
{
  GameComponent* factory = getRoot()->getNode(type);
  return factory->getInterface<FactoryInterface>();
  /*try
    {
      GameComponent* factories = getRoot()->getNode("factories");
      factory = factories->getNode(type);
    }
  catch (Log::Error& _error)
    {
      _error.catchError();
      factory = Filesystem::global()->getNode("dev/defaultFactories")->getNode(type);
    }
    return factory->getInterface<FactoryInterface>();*/
}
PropertyBagInterface* InstanceParameters::getChildren()
{
  if (GameComponent::getChildren())
    return GameComponent::getChildren()->getInterface<PropertyBagInterface>();
  else
    return nullptr;
}
PropertyBagInterface* InstanceParameters::getNext()
{
  if (GameComponent::getNext())
    return GameComponent::getNext()->getInterface<PropertyBagInterface>();
  else
    return nullptr;
}

void InstanceParameters::setType(const std::string& _type)
{
  type = _type; 
  /// FIXME might need to set a variable in GameComponent later on, not been written yet though
}
void InstanceParameters::registerActions(GameComponentType* _type)
{
  _type->exposeInterface<InstanceParameters, PropertyBagInterface>();
  //DefaultFactories::global()->addFactory<InstanceParameters, StaticData>("InstanceParameters");
}
