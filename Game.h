#ifndef GAME_H
#define GAME_H

extern class Game
{
    public:
        Game();
        void init();
        virtual ~Game();
        void run();
    protected:
    private:
}g_Game;

#endif // GAME_H
